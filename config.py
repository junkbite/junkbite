import os
from os.path import expanduser
import json

BASEDIR = os.path.abspath(os.path.dirname(__file__))


try:
    with open('/etc/junkbite.json') as config_file:
        config = json.load(config_file)
except FileNotFoundError as e:
    print('Not in production, using .local_config.json')
    with open('.local_config.json') as config_file:
        config = json.load(config_file)


class Config:
    """Holds configurations for the application, this gets passed in on create
    app on server startup"""
    HOME = expanduser("~")
    FLASK_ENV = config.get("FLASK_ENV")
    FLASK_APP = config.get("FLASK_APP")
    # DATABASE_URL
    # To switch databases do so in .local_config.json
    # MYSQL: "mysql://username:password@localhost/database-name"
    # SQLITE:  "instance/app.sqlite"
    DATABASE_URI = (
        config.get('DATABASE_URL') or
        'sqlite:///' + os.path.join(BASEDIR, 'instance/app.sqlite')
    )
    SQLALCHEMY_DATABASE_URI = DATABASE_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DATABASE_HOST = config.get("DATABASE_HOST")
    DATABASE_USER= config.get("DATABASE_USER")
    DATABASE_PASSWORD = config.get("DATABASE_PASSWORD")
    DATABASE_NAME = config.get("DATABASE_NAME")
    SQL_CHAR = "?" if 'sqlite' in DATABASE_URI else "%s"
    STATIC_FOLDER=os.path.join(BASEDIR, 'app/static')
    UPLOAD_FOLDER=os.path.join(BASEDIR, 'app/static/images')
    INVOICE_DIR=os.path.join(BASEDIR, 'app/static/invoices')
    SECRET_KEY=config.get('SECRET_KEY')
    DEBUG=config.get('DEBUG') or False
    ADMINS=config.get('ADMIN_EMAIL_ADDRESS_LIST')
    FLASK_PORT=5000
    APP_NAME = 'junkbite'  # Set this to the name of the application
    # Tor
    FLASK_TOR=config.get('FLASK_TOR') or False
    TOR_FOLDER=os.path.join(BASEDIR, '.tor/keys.txt')
    TOR_PORT=80
    # Lightning
    LIGHTNING_RPC=os.path.join(HOME, '.lightning/bitcoin/lightning-rpc')
    # LND
    LND_MACAROON=config.get("LND_MACAROON")
    LND_TLS_CERT=config.get("LND_TLS_CERT")
    LND_RPC_IP=config.get("LND_RPC_IP")
    # Allowed extensions for file uploads
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', 'webm', 'webp', 'mp4', 'mov'])
    # Twitter keys
    TWITTER_KEY=config.get('TWITTER_API_KEY')
    TWITTER_SECRET=config.get('TWITTER_API_SECRET')
    TWITTER_ACCESS_TOKEN=config.get('TWITTER_ACCESS_TOKEN')
    TWITTER_ACCESS_SECRET=config.get('TWITTER_ACCESS_SECRET')
    TWITTER_BEARER_TOKEN=config.get('TWITTER_BEARER_TOKEN')
    # ERROR MESSAGES - Move this elsewhere later
    FILE_TYPE_UNSUPPORTED = 'that filetype is unsupported.'
    # LOGGING
    LOG_TO_STDOUT = config.get('LOG_TO_STDOUT') or False
    # Twilio
    SENDGRID_SECRET = config.get('SENDGRID_SECRET')
    # Recaptcha
    RECAPTCHA_KEY = config.get("RECAPTCHA_KEY")
    RECAPTCHA_SECRET = config.get("RECAPTCHA_SECRET")
    # Scrolling Defaults
    DEFAULT_LIMIT = 10
    DEFAULT_OFFSET = 10