import requests
from time import sleep

ip = "localhost"
port = "5000"
url = f"http://{ip}:{port}/api/"

'''
api.api_comments                   GET        /api/comments
api.api_create_post                POST       /api/create-post
api.api_posts                      GET        /api/posts
api.api_threads                    GET        /api/threads
api.api_users                      GET        /api/users
api.docs                           GET        /api/docs
'''

endpoints = [
    'comments',
    #'create-post',
    'posts',
    'threads',
    'users',
]

for endpoint in endpoints:
    response = requests.get(url + endpoint)
    print(response.json(), end='\n\n\n')
    sleep(1)