import re
from app.models import (
    User,
    HashTag
)
from flask import current_app
from app import db
import logging

logger = logging.getLogger(__name__)

# Custom filters for jinja templates defined in here
def strftime(value, format=" %-I %p %A %B %-d"):
    try:
        return value.strftime(format)
    except AttributeError:
        return value


def remove_leading_char(value):
    return value[1:]


def strip_code_type(value):
    if 'post_code' in value:
        return value.replace('post_code: ', '')
    if 'bounty_code' in value:
        return value.replace('bounty_code: ', '')
    if 'bounty_response_code' in value:
        return value.replace('bounty_response_code: ', '')
    return value


def _process_user_tags(value):
    """
    This checks text for any users and then styles them with html and link 
    to profile
    """
    user_regex = re.compile("^\@.+$")
    words = value.split()  # split words
    replacement_map = {}
    for word in words:
        match = user_regex.match(word)
        if match:
            replacement_map[word] = word

    # now check if any of the possible usernames are 
    tagged_usernames = [u[1:] for u in list(replacement_map.keys())]
    users = User.query.filter(User.username.in_(tagged_usernames)).all()
    if current_app.config.get("FLASK_ENV") == "development":
        base_replace_str = "<a class='text-info font-weight-bolder user-tag' href='//localhost:5000/u/{}'>@{}</a>"
    else:
        base_replace_str = "<a class='text-info font-weight-bolder user-tag' href='//junkbite.com/u/{}'>@{}</a>"
    for user in users:
        replacement_map[f'@{user.username}'] = base_replace_str.format(user.username, user.username)

    if replacement_map:
        rep = dict((re.escape(k), v) for k, v in replacement_map.items())
        pattern = re.compile("|".join(rep.keys()))
        return pattern.sub(lambda m: rep[re.escape(m.group(0))], value)
    return value


def _process_hashtags(value):
    """
    This checks text for any users and then styles them with html and link 
    to profile
    """
    hashtag_regex = re.compile("^\#.+$")
    words = value.split()  # split words
    replacement_map = {}
    for word in words:
        logger.info(words)
        match = hashtag_regex.match(word)
        if match:
            replacement_map[word] = word

    # now check if any of the possible tags are - lowercase search
    hashtags_tagged = [u[1:] for u in list(replacement_map.keys())]
    logger.info(hashtags_tagged)
    added_tags = 0
    for tag in hashtags_tagged:
        exists = HashTag.query.filter_by(hashtag=tag.lower()).first()
        if not exists:
            logger.info('doesnt exist!')
            added_tags += 1
            new_tag = HashTag(hashtag=tag.lower())
            db.session.add(new_tag)
    if added_tags:
        db.session.commit()

    logger.info(hashtags_tagged)
    if current_app.config.get("FLASK_ENV") == "development":
        base_replace_str = "<a class='text-warning font-weight-lighter' href='//localhost:5000/search?s={}&active=post'>#{}</a>"
    else:
        base_replace_str = "<a class='text-warning font-weight-lighter' href='//junkbite.com/search?s={}&active=post'>#{}</a>"

    for tag in hashtags_tagged:
        replacement_map[f'#{tag}'] = base_replace_str.format(tag, tag)

    logger.info(replacement_map)
    if replacement_map:
        rep = dict((re.escape(k), v) for k, v in replacement_map.items())
        pattern = re.compile("|".join(rep.keys()))
        return pattern.sub(lambda m: rep[re.escape(m.group(0))], value)
    return value


def _find_hyperlinks_in_text(value):
    """
    Pre-processing each post coming through junkbite to add href tags around
    the content that should be clickable link
    -- leaving this here for future but using jinja2 urlize for now
    """
    regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    url = re.findall(regex, value)
    replacement_list = [x[0] for x in url]
    replacement_map = {}
    base_replace_str = "<a href='{}' target='_blank'>{}</a>"
    abs_base_replace_str = "<a href='//{}' target='_blank'>{}</a>"
    matches = ["http://", "https://"]
    for r in replacement_list:
        if any(x in r for x in matches):
            replacement_map[r] = base_replace_str.format(r, r)
        else:
            replacement_map[r] = abs_base_replace_str.format(r, r)

    if replacement_map:
        rep = dict((re.escape(k), v) for k, v in replacement_map.items())
        pattern = re.compile("|".join(rep.keys()))
        return pattern.sub(lambda m: rep[re.escape(m.group(0))], value)
    return value

def process_post_text(value):
    """
    Check text for user tags or hash tags
    """
    value = _find_hyperlinks_in_text(value)
    value = _process_user_tags(value)
    value = _process_hashtags(value)
    return value
