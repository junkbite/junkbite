# To be run in shell for now

def get_user(username):
    return User.query.filter_by(username=username).first()

def get_balance(username):
    user = get_user(username)
    return UserBalance.query.filter_by(user=user).first()


def get_balance_history(username):
    balance = get_balance(username)
    return UserBalanceEntry.query.filter_by(user_balance_id=balance.id).all()

def get_balances(limit=10):
    return (
        UserBalance.query.order_by(UserBalance.balance.desc())
        .limit(limit)
        .all()
    )

def debit_account(balance: UserBalance, amount):
    # used for correcting balances
    balance.balance = balance.balance - amount
    db.session.add(balance)
    db.session.commit()
