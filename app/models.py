import jwt
import json
import shortuuid
from time import time
from datetime import datetime
from flask import current_app, g
from app import db
from werkzeug.security import (
    generate_password_hash,
    check_password_hash
)
from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired
)
from sqlalchemy.sql import func, select
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import case, event
from sqlalchemy.dialects.mysql import LONGTEXT
from sqlalchemy.orm import class_mapper, ColumnProperty


class GenericMixin:
    def columns(self):
        """Return the actual columns of a SQLAlchemy-mapped object"""
        columns = []
        for prop in class_mapper(self.__class__).iterate_properties:
            if isinstance(prop, ColumnProperty):
                columns.append(prop.key)
        return columns


class User(GenericMixin, db.Model):
    """
    User
    """
    id = db.Column(db.Integer, primary_key=True)
    user_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(128))
    email_opt_out = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    original_ip_address = db.Column(db.String(50))
    ip_addresses = db.Column(db.String(3000))
    last_login = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    last_active = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    admin = db.Column(db.Boolean, default=False)
    admin_actions_taken = db.Column(db.Integer)
    punished_level = db.Column(db.Integer)
    email = db.Column(db.String(120))
    email_verified = db.Column(db.Boolean, default=False)
    unique_filename = db.Column(db.String(280))
    banned = db.Column(db.Boolean, default=False)
    link = db.Column(db.String(260))
    bio = db.Column(db.String(140))
    token = db.Column(db.String(200))
    token_expiration = db.Column(db.DateTime, nullable=True)
    private = db.Column(db.Boolean, default=False)
    posts = db.relationship('Post', back_populates='user', lazy='dynamic')
    bounties = db.relationship('Bounty', back_populates='user', lazy='dynamic')
    bounty_responses = db.relationship('BountyResponse', back_populates='user', lazy='dynamic')
    comments = db.relationship('Comment', back_populates='user', lazy='dynamic')
    likes = db.relationship('PostLike', back_populates='user', lazy='dynamic')
    tickets = db.relationship('SupportTicket', back_populates='user', lazy='dynamic')
    feedback = db.relationship('Feedback', back_populates='user', lazy='dynamic')
    withdrawals = db.relationship('Withdrawal', back_populates='user', lazy='dynamic')
    balance = db.relationship('UserBalance', back_populates='user')
    blogs = db.relationship('Blog', back_populates='user', lazy='dynamic')
    sent_payments = db.relationship(
        'Payment',
        backref="sending_user",
        foreign_keys='Payment.sending_user_id',
    )
    received_payments = db.relationship(
        'Payment',
        backref="receiving_user",
        foreign_keys='Payment.receiving_user_id',
    )
    sent_notifications = db.relationship(
        'Notification',
        backref="from_user",
        foreign_keys="Notification.from_user_id",
    )
    received_notifications = db.relationship(
        'Notification',
        backref="to_user",
        foreign_keys="Notification.to_user_id",
    )

    def __repr__(self):
        return '<User {}>'.format(self.username)

    @classmethod
    def created_today(cls):
        return [u.username for u in cls.query.filter(
            func.DATE(cls.created) == datetime.utcnow().date()
        ).all()]

    @classmethod
    def active_today(cls):
        return [u.username for u in cls.query.filter(
            func.DATE(cls.last_active) == datetime.utcnow().date()
        ).all()]

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @property
    def unread_notification_count(self):
        return len([n for n in self.received_notifications if n.opened == 0])

    @property
    def is_followed_by_you(self):
        if not g.user:
            return False
        return Followers.query.filter_by(
            follower_id=g.user.id, followee_id=self.id
        ).first()

    @property
    def is_following_you(self):
        if not g.user:
            return False
        return Followers.query.filter_by(
            follower_id=self.id, followee_id=g.user.id
        ).first()

    @hybrid_property
    def follower_count(self):
        return Followers.query.filter_by(followee_id=self.id).count()

    @follower_count.expression
    def follower_count(cls):
        return (
            select(func.count(Followers.id))
            .where(Followers.followee_id==cls.id)
            .label('follower_count')
        )

    @hybrid_property
    def followee_count(self):
        return Followers.query.filter_by(follower_id=self.id).count()

    @followee_count.expression
    def followee_count(cls):
        return (
            select(func.count(Followers.id))
            .where(Followers.follower_id==cls.id)
            .label('followee_count')
        )

    def serialized(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'post_count': self.posts.count(),
            'unique_filename': self.unique_filename,
            'upload': self.upload,
            'bio': self.bio,
            'private': self.private,
            'created': self.created
        }

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {
                'reset_password': self.id,
                'exp': time() + expires_in
            },
            current_app.config['SECRET_KEY'],
            algorithm='HS256'
        )

    @staticmethod
    def verify_reset_password_token(token):
        # id is what is found in 'reset_password' after decode
        try:
            user_id = jwt.decode(
                token,
                current_app.config['SECRET_KEY'],
                algorithms=['HS256']
            )['reset_password']
        except Exception:
            return
        return User.query.get(user_id)

    def get_email_confirmation_token(self, expires_in=1200):
        return jwt.encode(
            {
                'code': self.user_code,
                'exp': time() + expires_in
            },
            current_app.config['SECRET_KEY'],
            algorithm='HS256'
        )

    @staticmethod
    def verify_email_confirmation_token(token):
        # id is what is found in 'reset_password' after decode
        try:
            user_code = jwt.decode(
                token,
                current_app.config['SECRET_KEY'],
                algorithms=['HS256']
            )['code']
        except Exception:
            return
        return User.query.filter_by(user_code=user_code).first()


    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user


class Post(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    post_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    sticky = db.Column(db.Integer)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    title = db.Column(db.String(500))
    body = db.Column(db.Text(4294000000))
    processed_body = db.Column(db.Text(4294000000))
    upload = db.Column(db.String(280))
    unique_filename = db.Column(db.String(280))
    nsfw = db.Column(db.Boolean, default=False)
    meme = db.Column(db.Boolean, default=True)
    article = db.Column(db.Boolean, default=False)
    reading_time = db.Column(db.Integer)
    draft = db.Column(db.Boolean, default=False)
    tenor_gif = db.Column(db.Boolean, default=False)
    board = db.Column(db.String(100))
    user = db.relationship('User', back_populates='posts')
    likes = db.relationship('PostLike', backref='post', lazy='dynamic')
    comments = db.relationship('Comment', backref='post', lazy='dynamic')
    payments = db.relationship('Payment', backref='post', lazy='dynamic')

    def __repr__(self):
        return '<Post {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'title': self.title,
            'body': self.body,
            'processed_body': self.processed_body,
            'created': self.created,
            'unique_filename': self.unique_filename,
            'upload': self.upload,
            'author_id': self.author_id,
            'author_username': self.user.username,
            'nsfw': self.nsfw,
            'meme': self.meme,
            'sats_boosted': self.sats_boosted,
            'like_count': self.like_count,
            'comment_count': self.comment_count
        }

    @property
    def user_id(self):
        return self.author_id

    @hybrid_property
    def sats_boosted(self):
        payment_amounts = [
            p.amount for p in Payment.query.filter_by(
                post_id=self.id, status="completed"
            )
        ]
        return sum(payment_amounts)

    @sats_boosted.expression
    def sats_boosted(cls):
        return (
            select(func.sum(Payment.amount))
            .where(Payment.post_id==cls.id, Payment.status=="completed")
            .label('sats_boosted')
        )

    @hybrid_property
    def like_count(self):
        return PostLike.query.filter_by(post_id=self.id).count()

    @like_count.expression
    def like_count(cls):
        return (
            select(func.count(PostLike.id))
            .where(PostLike.post_id==cls.id)
            .label('like_count')
        )

    @hybrid_property
    def comment_count(self):
        return Comment.query.filter_by(post_id=self.id).count()

    @comment_count.expression
    def comment_count(cls):
        return (
            select(func.count(Comment.id))
            .where(Comment.post_id==cls.id)
            .label('comment_count')
        )

    @property
    def liked(self):
        if not g.user:
            return False
        return PostLike.query.filter_by(
            post_id=self.id, user_id=g.user.id
        ).first()

    @property
    def zapped(self):
        if not g.user:
            return False
        return Payment.query.filter_by(
            post_id=self.id, sending_user_id=g.user.id, status="completed"
        ).first()

    @property
    def bookmarked(self):
        if not g.user:
            return False
        return True if Bookmark.query.filter_by(
            post_id=self.id, user_id=g.user.id
        ).first() else False

    @property
    def author_img(self):
        return self.user.unique_filename

    @property
    def username(self):
        return self.user.username


class Comment(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='SET NULL'))
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.id', ondelete='SET NULL'))
    body = db.Column(db.String(2000))
    processed_body = db.Column(db.String(2000))
    draft = db.Column(db.Boolean, default=False)
    nsfw = db.Column(db.Boolean, default=False)
    meme = db.Column(db.Boolean, default=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    unique_filename = db.Column(db.String(280))
    upload = db.Column(db.String(280))
    tenor_gif = db.Column(db.Boolean, default=False)
    user = db.relationship('User', back_populates='comments')
    likes = db.relationship('CommentLike', backref='comment', lazy='dynamic')
    payments = db.relationship('Payment', backref='comment', lazy='dynamic')

    def __repr__(self):
        return '<Comment {}>'.format(self.id)

    @property
    def title(self):
        return self.body

    @property
    def author_id(self):
        return self.user_id

    @property
    def author_img(self):
        return self.user.unique_filename

    @property
    def username(self):
        return self.user.username

    @property
    def post_code(self):
        return self.post.post_code

    @property
    def liked(self):
        if not g.user:
            return False
        return CommentLike.query.filter_by(
            comment_id=self.id, user_id=g.user.id
        ).first()

    @hybrid_property
    def like_count(self):
        return CommentLike.query.filter_by(comment_id=self.id).count()

    @like_count.expression
    def like_count(cls):
        return (
            select(func.count(CommentLike.id))
            .where(CommentLike.comment_id==cls.id)
            .label('like_count')
        )

    @hybrid_property
    def sats_boosted(self):
        payment_amounts = [
            p.amount for p in Payment.query.filter_by(
                comment_id=self.id, status="completed"
            )
        ]
        return sum(payment_amounts)

    @sats_boosted.expression
    def sats_boosted(cls):
        return (
            select(func.sum(Payment.amount))
            .where(Payment.comment_id==cls.id, Payment.status=="completed")
            .label('sats_boosted')
        )

    @property
    def zapped(self):
        if not g.user:
            return False
        return Payment.query.filter_by(
            comment_id=self.id, sending_user_id=g.user.id
        ).first()

    @hybrid_property
    def comment_count(self):
        return Comment.query.filter_by(comment_id=self.id).count()

    @comment_count.expression
    def comment_count(cls):
        return (
            select(func.count(Comment.id))
            .where(Comment.comment_id==cls.id)
            .label('comment_count')
        )

    @property
    def comments(self):
        return Comment.query.filter_by(comment_id=self.id).all()

    def serialized(self):
        return {
            'id': self.id,
            'body': self.body,
            'user': User.query.get(self.user_id).serialized(),
            'post': Post.query.get(self.post_id).serialized(),
            'created': self.created,
            'unique_filename': self.unique_filename,
            'upload': self.upload
        }


class HashTag(GenericMixin, db.Model):
    """
    A tag to categorize 
    """
    id = db.Column(db.Integer, primary_key=True)
    hashtag_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    hashtag = db.Column(db.String(300))

    def __repr__(self):
        return f"<HashTag {self.id}>"


class FollowRequest(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    request_to = db.Column(db.Integer, db.ForeignKey('user.id'))
    requestor = db.Column(db.Integer, db.ForeignKey('user.id'))
    status = db.Column(db.Integer, default=2)  # 2 pending, 1 accepted, 0 rejected

    def __repr__(self):
        return '<FollowRequest {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'request_to': User.query.get(self.request_to).serialized(),
            'requestor': User.query.get(self.requestor).serialized(),
            'status': self.status
        }


class Notification(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    notification_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    to_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    from_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    opened = db.Column(db.Integer, default=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    content = db.Column(db.String(300))
    post_id = db.Column(db.Integer)
    bounty_id = db.Column(db.Integer)
    bounty_response_id = db.Column(db.Integer)
    comment_id = db.Column(db.Integer)

    def __repr__(self):
        return '<Notification {}>'.format(self.id)

    @property
    def post_code(self):
        if not self.post_id:
            return None
        return Post.query.get(self.post_id).post_code

    @property
    def content_code(self):
        if self.comment_id:
            return Comment.query.get(self.comment_id).comment_code
        elif self.post_id:
            return Post.query.get(self.post_id).post_code
        elif self.bounty_id:
            return Bounty.query.get(self.bounty_id).bounty_code
        elif self.bounty_response_id:
            bounty_response_code = (
                BountyResponse.query.get(self.bounty_response_id)
                .bounty_response_code
            )
            return bounty_response_code

    def serialized(self):
        return {
            'id': self.id,
            'to_user_id': self.to_user_id,
            'from_user_id': self.from_user_id,
            'to_user': User.query.get(self.to_user_id),
            'from_user': User.query.get(self.from_user_id),
            'opened': self.opened,
            'created': self.created,
            'content': self.content,
            'post_id': self.post_id,
            'bounty_id': self.bounty_id,
            'bounty_response_id': self.bounty_response_id,
            'comment_id': self.comment_id,
            'post_code': self.post_code if self.post_id else None
        }


class Bounty(GenericMixin, db.Model):
    """Bounty"""
    id = db.Column(db.Integer, primary_key=True)
    bounty_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    status = db.Column(db.String(20), default="open")
    winner_response_id = db.Column(db.Integer)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    title = db.Column(db.String(280))
    body = db.Column(db.Text)
    processed_body = db.Column(db.Text)
    private = db.Column(db.Boolean, default=False)
    unique_filename = db.Column(db.String(280))
    upload = db.Column(db.String(280))
    initial_sats = db.Column(db.Integer)
    initial_ln_invoice = db.Column(db.String(500))
    total_sats = db.Column(db.Integer)
    draft = db.Column(db.Boolean, default=False)
    user = db.relationship('User', back_populates='bounties')
    responses = db.relationship('BountyResponse', back_populates='bounty', lazy='dynamic')

    def __repr__(self):
        return '<Bounty {}>'.format(self.id)

    @property
    def response_count(self):
        return self.responses.count()

    @property
    def username(self):
        return self.user.username

    def serialized(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'winner_response_id': self.winner_response_id,
            'created': self.created,
            'title': self.title,
            'body': self.body,
            'processed_body': self.processed_body,
            'private': self.private,
            'status': self.status,
            'initial_sats': self.initial_sats,
            'initial_ln_invoice': self.initial_ln_invoice,
            'unique_filename': self.unique_filename,
            'total_sats': self.total_sats
        }


class BountyResponse(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bounty_response_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    bounty_id = db.Column(db.Integer, db.ForeignKey('bounty.id', ondelete='SET NULL'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    title = db.Column(db.String(280))
    body = db.Column(db.Text)
    processed_body = db.Column(db.Text)
    private = db.Column(db.Boolean, default=False)
    unique_filename = db.Column(db.String(280))
    upload = db.Column(db.String(280))
    upvotes = db.Column(db.Integer, default=0)
    bounty_winner = db.Column(db.Boolean, default=False)
    user = db.relationship('User', back_populates='bounty_responses')
    bounty = db.relationship('Bounty', back_populates='responses')

    def __repr__(self):
        return '<BountyResponse {}>'.format(self.id)

    @property
    def username(self):
        return self.user.username

    @property
    def upvoted(self):
        if not g.user:
            return False
        return BountyResponseLike.query.filter_by(
            bounty_response_id=self.id, user_id=g.user.id
        ).first()

    @hybrid_property
    def sats_boosted(self):
        payment_amounts = [
            p.amount for p in Payment.query.filter_by(
                bounty_response_id=self.id, status="completed"
            )
        ]
        return sum(payment_amounts)

    @sats_boosted.expression
    def sats_boosted(cls):
        return (
            select(func.sum(Payment.amount))
            .where(Payment.bounty_response_id==cls.id, Payment.status=="completed")
            .label('sats_boosted')
        )

    @property
    def zapped(self):
        if not g.user:
            return False
        return Payment.query.filter_by(
            bounty_response_id=self.id,
            sending_user_id=g.user.id,
            status="completed"
        ).first()

    def serialized(self):
        return {
            'id': self.id,
            'bounty_id': self.bounty_id,
            'created': self.created,
            'body': self.body,
            'processed_body': self.processed_body,
            'ln_invoice': self.ln_invoice,
            'private': self.private,
            'status': self.status,
            'unique_filename': self.unique_filename,
            'upvotes': self.upvotes,
            'bounty_winner': self.bounty_winner
        }


class Withdrawal(GenericMixin, db.Model):
    '''when a user sends to an outside address'''
    id = db.Column(db.Integer, primary_key=True)
    withdrawal_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    completed = db.Column(db.DateTime)
    amount = db.Column(db.Integer)
    ln_invoice = db.Column(db.String(400))
    node_uri = db.Column(db.String(400))
    status = db.Column(db.String(250))
    notes = db.Column(db.Text)
    user = db.relationship('User', back_populates='withdrawals')

    def __repr__(self):
        return '<Withdrawal {}>'.format(self.id)

    @property
    def username(self):
        return self.user.username

    def serialized(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'created': self.created,
            'completed': self.completed,
            'amount': self.amount,
            'ln_invoice': self.ln_invoice,
            'node_uri': self.node_uri,
            'status': self.status,
            'initiated': self.created
        }


class ReportedContent(GenericMixin, db.Model):
    '''database table where reported posts get sent'''
    __tablename__ = 'reported_post'
    id = db.Column(db.Integer, primary_key=True)
    report_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='SET NULL'))
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.id', ondelete='SET NULL'))
    bounty_id = db.Column(db.Integer, db.ForeignKey('bounty.id', ondelete='SET NULL'))
    bounty_response_id = db.Column(db.Integer, db.ForeignKey('bounty_response.id', ondelete='SET NULL'))
    post = db.relationship('Post')
    reported_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    reported_user = db.relationship('User', foreign_keys=[reported_user_id])
    reporting_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    reporting_user = db.relationship('User', foreign_keys=[reporting_user_id])
    reason = db.Column(db.String(500))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    closed = db.Column(db.DateTime, default=datetime.utcnow)
    action_taken = db.Column(db.String(500))

    def __repr__(self):
        return '<ReportedContent {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'post_id': self.post_id,
            'post': self.post.serialized(),
            'reported_user': self.reported_user.serialized(),
            'reporting_user': self.reporting_user.serialized(),
            'reason': self.reason,
            'created_timestamp': self.created,
            'closed_timestamp': self.closed,
            'action_taken': self.action_taken
        }


class Followers(GenericMixin, db.Model):
    """
    Follower relationship Table
    """
    id = db.Column(db.Integer, primary_key=True)
    follower_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    followee_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))

    def __repr__(self):
        return f"<Followers: {self.id}>"

    def serialized(self):
        return {
            'id': self.id,
            'follower_id': self.follower_id,
            'followee_id': self.followee_id
        }


class UserNode(GenericMixin, db.Model):
    """
    User Node relationship
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    node_uri = db.Column(db.String(400))

    def __repr__(self):
        return '<UserNode {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'node_uri': self.node_uri,
        }


class PostNode(GenericMixin, db.Model):
    """
    Post Node
    """
    id = db.Column(db.Integer, primary_key=True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='CASCADE'))
    node_uri = db.Column(db.String(400))

    def __repr__(self):
        return '<PostNode {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'post_id': self.post_id,
            'node_uri': self.node_uri,
        }


class UserBalance(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_balance_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    balance = db.Column(db.Integer)
    user = db.relationship('User', back_populates='balance')
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<UserBalance {}>'.format(self.id)

    def __str__(self):
        return f'{self.user.username}: {self.balance}'

    @property
    def username(self):
        return self.user.username

    def serialized(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'balance': self.balance,
        }


class UserBalanceEntry(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_balance_id = db.Column(db.Integer, db.ForeignKey('user_balance.id'))
    before_balance = db.Column(db.Integer)
    action_type = db.Column(db.String(50))
    ln_invoice = db.Column(db.String(400))
    amount = db.Column(db.Integer)
    updating_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    user_balance = db.relationship('UserBalance')
    payment_id = db.Column(db.Integer, db.ForeignKey('payment.id', ondelete='SET NULL'))
    payment = db.relationship('Payment')

    def __repr__(self):
        return '<UserBalanceEntry {}>'.format(self.id)

    def __str__(self):
        return f'{self.action_type} {self.amount} {self.ln_invoice} to {self.username}'

    @property
    def username(self):
        return self.user_balance.user.username

    def serialized(self):
        return {
            'id': self.id,
            'user_balance_id': self.user_balance_id,
            'amount': self.amount,
            'timestamp': self.timestamp
        }


class Payment(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    payment_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='SET NULL'))
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.id', ondelete='SET NULL'))
    bounty_id = db.Column(db.Integer, db.ForeignKey('bounty.id', ondelete='SET NULL'))
    bounty_response_id = db.Column(db.Integer, db.ForeignKey('bounty_response.id', ondelete='SET NULL'))
    receiving_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    sending_user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    junkbite_app = db.Column(db.String(50))
    amount = db.Column(db.Integer)
    ln_invoice = db.Column(db.String(400))
    payment_type = db.Column(db.String(100))
    status = db.Column(db.String(100))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    fee = db.relationship('PaymentProcessingFee', uselist=False, backref='payment')

    def __repr__(self):
        return '<Payment {}>'.format(self.id)

    def __str__(self):
        return f"Payment of {self.amount} from {self.sending_user.username} to {self.receiving_user.username}"

    @classmethod
    def created_today(cls):
        return [p.amount for p in cls.query.filter(
            func.DATE(cls.created) == datetime.utcnow().date()
        ).all()]

    @classmethod
    def paid_to_junkbite_today(cls):
        return [p.amount for p in cls.query.filter(
            func.DATE(cls.created) == datetime.utcnow().date(),
            Payment.receiving_user_id==User.query.filter_by(username='junkbite').first().id
        ).all()]

    @property
    def amount_minus_fee(self):
        return self.amount - self.fee.amount

    @property
    def sending_username(self):
        return self.sending_user.username

    @property
    def receiving_username(self):
        return self.receiving_user.username

    @property
    def post_code(self):
        if not self.post_id:
            return None
        return Post.query.get(self.post_id).post_code

    @property
    def content_code(self):
        if self.post_id:
            post_code = Post.query.get(self.post_id).post_code
            return f"post_code: {post_code}"
        if self.comment_id:
            comment_code = Comment.query.get(self.comment_id).comment_code
            return f"comment_code: {comment_code}"
        if self.bounty_id:
            bounty_code = Bounty.query.get(self.bounty_id).bounty_code
            return f"bounty_code: {bounty_code}"
        if self.bounty_response_id:
            bounty_response_code = (
                BountyResponse.query.get(self.bounty_response_id)
                .bounty_response_code
            )
            return f"bounty_response_code: {bounty_response_code}"
        return self.junkbite_app

    def serialized(self):
        return {
            'id': self.id,
            'post_id': self.post_id,
            'user_id': self.user_id,
            'created': self.created
        }


class PaymentProcessingFee(GenericMixin, db.Model):
    """
    Table to record each fee collected by junkbite from incoming payments
    """
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Integer)
    payment_id = db.Column(db.Integer, db.ForeignKey('payment.id', ondelete='SET NULL'))

    def __repr__(self):
        return f"<PaymentProcessingFee {self.id}>"

    @property
    def content_code(self):
        return self.payment.content_code

    @property
    def username(self):
        return self.payment.sending_username


class PostLike(GenericMixin, db.Model):
    """
    Database Represenation of a 'favorite' or a 'like'
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='CASCADE'))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    user = db.relationship('User', back_populates='likes')

    def __repr__(self):
        return '<PostLike {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'bounty_response_id': self.bounty_response_id,
            'user_id': self.user_id,
            'created': self.created
        }


class BountyResponseLike(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    bounty_response_id = db.Column(db.Integer, db.ForeignKey('bounty_response.id', ondelete='CASCADE'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<BountyResponseLike {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'bounty_response_id': self.bounty_response_id,
            'user_id': self.user_id,
            'created': self.created
        }


class Bookmark(GenericMixin, db.Model):
    """
    Database Represenation of a 'bookmark' or saved post
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    post_id = db.Column(db.Integer, db.ForeignKey('post.id', ondelete='CASCADE'))
    bounty_id = db.Column(db.Integer, db.ForeignKey('bounty.id', ondelete='CASCADE'))
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Bookmark {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'post_id': self.post_id,
            'created': self.created
        }


class CommentLike(GenericMixin, db.Model):
    """
    Database Represenation of a 'comment favorite'
    """
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.id', ondelete='CASCADE'))
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<CommentLike {}>'.format(self.id)

    def serialized(self):
        return {
            'id': self.id,
            'user': self.user.serialized(),
            'comment': self.post.serialized(),
            'timestamp': self.timestamp
        }


class Blog(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    blog_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'))
    sticky = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    draft = db.Column(db.Boolean, default=False)
    slug = db.Column(db.String(300))
    title = db.Column(db.String(280))
    body = db.Column(db.Text)
    processed_body = db.Column(db.Text)
    unique_filename = db.Column(db.String(280))
    upload = db.Column(db.String(280))
    user = db.relationship('User', back_populates='blogs', cascade="all, delete")

    def __repr__(self):
        return f"<Blog {self.id}>"

    @property
    def username(self):
        return self.user.username

    def serialized(self):
        return {
            'id': self.id,
        }


class OnionUrl(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    onion = db.Column(db.String(500))

    def __repr__(self):
        return f"<OnionUrl {self.id}>"

    def serialized(self):
        return {
            'id': self.id,
        }



class Advertisement(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ad_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    """
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    post_id INTEGER,
    price INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    hours_purchased INTEGER NOT NULL,
    expired INTEGER NOT NULL DEFAULT 0,
    """

    def __repr__(self):
        return f"<Advertisement {self.id}>"

    def serialized(self):
        return {
            'id': self.id,
        }


class AdvertisementStats(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ad_stats_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    """
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    advertisement_id INTEGER NOT NULL,
    clicks INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (advertisement_id) REFERENCES advertisement (id)
    """

    def __repr__(self):
        return f"<AdvertisementStats {self.id}>"

    def serialized(self):
        return {
            'id': self.id,
        }


class JunkbiteNode(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    pubkey = db.Column(db.String(500))
    uri = db.Column(db.String(500))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return f"<JunkbiteNode {self.pubkey}>"

    def serialized(self):
        return {
            'id': self.id,
            'title': self.title,
            'pubkey': self.pubkey,
            'uri': self.uri
        }


class SupportTicket(GenericMixin, db.Model):
    """
    Ticketing system to help users
    """
    id = db.Column(db.Integer, primary_key=True)
    ticket_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    description = db.Column(db.String(10000))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    ticket_status = db.Column(db.String(30), default="opened")
    created = db.Column(db.DateTime, default=datetime.utcnow)
    user = db.relationship('User', back_populates='tickets')
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    responses = db.relationship('SupportTicketResponse', back_populates='ticket', lazy='dynamic')

    def __repr__(self):
        return f"<SupportTicket {self.id}>"

    @property
    def username(self):
        return User.query.get(self.user_id).username

    def serialized(self):
        return {
            'id': self.id,
            'ticket_code': self.ticket_code,
            'description': self.description,
            'user_id': self.user_id,
            'ticket_status': self.ticket_status,
            'created': self.created
        }


class SupportTicketResponse(GenericMixin, db.Model):
    """
    Allow for responses on a support ticket so tickets can be acted upon
    and closed
    """
    id = db.Column(db.Integer, primary_key=True)
    support_ticket_id = db.Column(db.Integer, db.ForeignKey('support_ticket.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    response_text = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    ticket = db.relationship('SupportTicket', back_populates="responses")

    def __repr__(self):
        return f"SupportTicketResponse {self.id}>"

    @property
    def user(self):
        return User.query.get(self.user_id)


class Feedback(GenericMixin, db.Model):
    """
    Store each entry of user feedback
    """
    id = db.Column(db.Integer, primary_key=True)
    feedback_code = db.Column(db.String(30), default=shortuuid.uuid, unique=True)
    description = db.Column(db.String(10000))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='SET NULL'))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    user = db.relationship('User', back_populates='feedback')

    def __repr__(self):
        return f"<Feedback {self.id}>"

    @property
    def username(self):
        return User.query.get(self.user_id).username

    def serialized(self):
        return {
            'id': self.id,
            'feedback_code': self.feedback_code,
            'description': self.description,
            'user_id': self.user_id,
            'created': self.created
        }


class AdminView(db.Model):
    """
    Define information here about an admin view
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    route = db.Column(db.String(100))
    function = db.Column(db.String(50))
    field_names = db.Column(db.String(1000))
    field_headers = db.Column(db.String(1000))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return f"<AdminView {self.id}>"

    @property
    def field_names_list(self):
        return [f.strip() for f in self.field_names.split(',')]

    @property
    def field_headers_list(self):
        return [f.strip() for f in self.field_headers.split(',')]


class LightningAppConfiguration(GenericMixin, db.Model):
    """
    Configuration of junkbite lightning apps here to enable dynamic changes
    for example the cost of sending a tweet or seeing a random post
    """
    id = db.Column(db.Integer, primary_key=True)
    application_name = db.Column(db.String(200), unique=True, nullable=False)
    price_for_function = db.Column(db.Integer, nullable=False)
    utility_field = db.Column(db.String(200))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    last_updated = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return f"<LightningAppConfiguration {self.id}>"


class EmailHistory(GenericMixin, db.Model):
    """
    Store email contents
    """
    id = db.Column(db.Integer, primary_key=True)
    recipients = db.Column(db.String(2000))
    sender = db.Column(db.String(250))
    subject = db.Column(db.String(500))
    text_content = db.Column(db.Text)
    html_content = db.Column(db.Text)
    created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return f"<EmailHistory {self.id}>"

    # TODO, in the case where there is more than 1 recipient to the same email
    # however there are currently none that do this
    @property
    def username(self):
        try:
            return [u.username for u in User.query.filter(
                User.email.in_([r.strip() for r in self.recipients.split(',')])
            )][0]
        except:
            return ''


class Settings(GenericMixin, db.Model):
    """
    Store some general settings for junkbite in here
    """
    id = db.Column(db.Integer, primary_key=True)
    payment_percentage = db.Column(db.Integer)
    withdrawals_frozen = db.Column(db.Boolean, default=False)
    withdrawals_frozen_message = db.Column(db.Text)
    maximum_withdrawal_amount = db.Column(db.Integer, default=1000)
    withdrawal_period = db.Column(db.Integer, default=24)

    def __repr__(self):
        return f"<Settings {self.id}>"


class FAQEntry(GenericMixin, db.Model):
    """
    Store frequently asked questions in DB so it can be dynamically updated
    """
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.Text(10000))
    answer = db.Column(db.Text(4294000000))
    visible = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"<FAQEntry {self.id}>"

