from app.auth.utils import login_required
from app.utils.meta_utils import (
    _datetime_to_humantime,
    _sqliterow_to_dict,
)
from flask import (
    current_app,
    g,
    flash,
    request,
    redirect,
    render_template,
    url_for,
)
from werkzeug.exceptions import abort
from app.bounty import bp
from app.email import sendgrid_new_bounty_response
from app.utils.db_utils import (
    _delete_bounty_response_upvote,
    _get_bounty_user_id,
    _get_bounty_detail,
    _get_bounty,
    _get_all_bounties,
    _get_bounty_id_by_response_code,
    _get_closed_bounties,
    _get_open_bounties,
    _get_user_bounties,
    _get_current_user_id,
    _get_bounty_response_user_id,
    _get_bounty_user_id,
    _get_bounties_user_responded_to,
    _get_bounty_winner_info,
    _get_bounty_response_upvote,
    _get_bounty_from_bounty_code,
    _get_max_withdrawal_amount,
    _has_invoice_been_paid,
    _save_bounty_response_upvote,
    _save_bounty,
    _save_bounty_response,
    _set_bounty_winner_and_close,
    _update_user_balance,
    _save_bounty_payout,
    _save_bounty_creation_payment,
    _save_payment,
    _save_notification
)
from lndgrpc import LNDClient


OPEN_BOUNTIES = 'bounty.open_bounties'
BOUNTIES_HTML = 'bounty/bounties.html'
BOUNTY_DETAIL_VIEW = 'bounty.view_bounty'
INJECTABLE_BOUNTIES = 'bounty/injectable_bounties.html'


@bp.route('/award-bounty/<bounty_code>', methods=("POST",))
@login_required
def award_bounty(bounty_code):
    response_winner_id = request.form.get('bountyResponseWinner')
    # set bounty winner, pay winning user and close
    bounty = _get_bounty(bounty_code)
    if bounty.status != "open":
        flash('Bounty already closed')
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty.bounty_code))
    response_user_id = _get_bounty_response_user_id(response_winner_id)
    bounty_id = bounty.id
    if not g.user or g.user.id != bounty.user_id:
        flash("This is not your bounty to award")
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))
    if bounty.user_id == response_user_id:
        flash("You cannot award yourself the bounty")
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))
    _set_bounty_winner_and_close(bounty_id, response_winner_id)
    amount, ln_invoice, user_id = _get_bounty_winner_info(bounty_id)
    bounty_creator_user_id = _get_bounty_user_id(bounty_id)
    # create payment for winning bounty and update user balance
    was_balance_updated = _save_bounty_payout(
        bounty_code=bounty_code,
        amount=amount,
        ln_invoice=ln_invoice,
        bounty_id=bounty_id,
        junkbite_app="bounty",
        payment_type="bounty winner",
        receiving_user_id=user_id,
        sending_user_id=bounty_creator_user_id
    )
    if not was_balance_updated:
        flash('Award already claimed')
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))
    content = f"{g.user.username} has awarded you a bounty"
    _save_notification(
        response_user_id, g.user.id, content, bounty_id=bounty_id
    )
    flash('Bounty Awarded and Closed')
    return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))


@bp.route('/all-bounties')
def all_bounties():
    title = 'All Bounties'
    bounties = _get_all_bounties(limit=20, offset=0)
    return render_template(
        BOUNTIES_HTML,
        title=title,
        bounties=bounties
    )


@bp.route('/open-bounties')
def open_bounties():
    title = 'Open Bounties'
    bounties = _get_open_bounties(limit=20, offset=0)
    return render_template(
        BOUNTIES_HTML,
        title=title,
        bounties=bounties
    )


@bp.route('/closed-bounties')
def closed_bounties():
    title = 'Closed Bounties'
    bounties = _get_closed_bounties(limit=20, offset=0)
    return render_template(
        BOUNTIES_HTML,
        title=title,
        bounties=bounties
    )


@bp.route('/create-bounty', methods=('GET', 'POST'))
@login_required
def create_bounty():
    """
    View to create a new bounty
    """
    title = 'Create Bounty'
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if request.method == 'POST':
        title = request.form.get('title')
        body = request.form.get('editorText')
        invoice = request.form.get("invoiceSubmit")
        if not title:
            flash('Title is required.')
            return redirect(url_for('bounty.create_bounty'))
        if body == "<p><br></p>":
            flash('Body is required.')
            return redirect(url_for('bounty.create_bounty'))
        # using wysiwyg editor now so no markdown processing
        processed_body = body
        # save bounty - pull info from ln invoice
        lnd = LNDClient(
            ip_address=current_app.config["LND_RPC_IP"],
            macaroon_filepath=current_app.config['LND_MACAROON'],
            cert_filepath=current_app.config['LND_TLS_CERT'],
            admin=True
        )
        info = lnd.decode_payment_request(invoice)
        amount = info.num_satoshis
        if amount > maximum_withdrawal_amount:
            flash(f'Max bounty is {maximum_withdrawal_amount} sats')
            return redirect(url_for('bounty.create_bounty'))
        upload = None
        filename = None
        user_id = _get_current_user_id()
        bounty = _save_bounty(
            title,
            body,
            processed_body,
            user_id, 
            upload,
            filename,
            amount,
            invoice
        )
        _save_bounty_creation_payment(
            amount=amount,
            ln_invoice=invoice,
            bounty_id=bounty.id,
            junkbite_app="bounty",
            payment_type="bounty creation",
        )
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty.bounty_code))
    return render_template(
        'bounty/create_bounty.html',
        title=title,
        max_zap=maximum_withdrawal_amount
    )


@bp.route('/u/<username>/user-bounties', methods=("GET",))
@login_required
def user_bounties(username):
    """
    View a user's bounties
    """
    title = "Bounties you created"
    bounties = _get_user_bounties(username)
    return render_template(
        BOUNTIES_HTML,
        title=title,
        bounties=bounties
    )


@bp.route('/get-create-bounty-invoice', methods=["POST"])
@login_required
def get_create_bounty_invoice():
    """
    Get a bounty invoice to finalize the creation of the bounty
    """
    amount = request.form.get("amount")
    if amount is None:
        return {'message': 'failed'}
    amount = int(amount)
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if amount > maximum_withdrawal_amount:
        return {'message': f'Max zap is {maximum_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(amount)
    ln_invoice = response.payment_request
    _save_payment(
        amount=amount,
        ln_invoice=ln_invoice,
        junkbite_app="bounty",
        payment_type="bounty creation"
    )
    return {'message': ln_invoice}


@bp.route('/get-bounty-invoice', methods=("POST",))
def get_bounty_invoice():
    """
    Get a payment invoice to boost a bounty
    """
    bounty_id = request.form.get("bountyId")
    payment_amount = int(request.form.get("amount"))
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if payment_amount > maximum_withdrawal_amount:
        return {'message': f'Max zap is {maximum_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(payment_amount)
    ln_invoice = response.payment_request
    if bounty_id:
        _save_payment(
            amount=payment_amount,
            ln_invoice=ln_invoice,
            bounty_id=bounty_id,
            junkbite_app="bounty",
            payment_type="sat boost on bounty"
        )
    else:
        return {'message': 'failed to determine bounty'}
    return {'message': ln_invoice}


@bp.route('/get-bounty-response-invoice', methods=("POST",))
def get_bounty_response_invoice():
    """
    Get a bounty invoice
    """
    response_id = request.form.get("responseId")
    payment_amount = int(request.form.get("amount"))
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if payment_amount > maximum_withdrawal_amount:
        return {'message': f'Max zap is {maximum_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(payment_amount)
    ln_invoice = response.payment_request
    if response_id:
        _save_payment(
            amount=payment_amount,
            ln_invoice=ln_invoice,
            response_id=response_id,
            junkbite_app="bounty",
            payment_type="sat boost on bounty response"
        )
    else:
        return {'message': 'failed to determine bounty response'}
    return {'message': ln_invoice}


@bp.route('/response-upvote', methods=("POST",))
def response_upvote():
    '''
    upvotes a bounty response
    '''
    response_id = request.form.get('responseId')
    if not g.user:
        return {'message': 'login to upvote'}
    # check if response_id has already been upvoted
    upvote = _get_bounty_response_upvote(response_id, g.user.id)
    if upvote:
        _delete_bounty_response_upvote(upvote)
        return {'message': 'deleted'}
    _save_bounty_response_upvote(response_id, g.user.id)
    return {'message': 'upvoted'}


@bp.route('/u/<username>/user-bounty-responses', methods=("GET",))
@login_required
def user_responded_bounties(username):
    """
    View a user's bounties they responded
    """
    bounties = _get_bounties_user_responded_to(username)
    return render_template(BOUNTIES_HTML, bounties=bounties)


@bp.route('/bounty/<bounty_code>', methods=("GET", "POST"))
def view_bounty(bounty_code):
    """
    View a bounty in detail
    """
    # todo clean this up to just get bounty - its dumb right now
    bounty = _get_bounty_from_bounty_code(bounty_code)
    if not bounty:
        flash("Not Found")
        return redirect(url_for('bounty.open_bounties'))
    if request.method == "POST":
        body = request.form.get("response")
        processed_body = current_app.markdown(body)
        user_id = _get_current_user_id()
        bounty_author_id = bounty.user_id
        if user_id == bounty_author_id:
            flash("You cannot respond to your own bounty")
            return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))
        _save_bounty_response(bounty.id, user_id, body, processed_body)
        sendgrid_new_bounty_response(bounty.id)
        content = f"{g.user.username} responded to your bounty"
        _save_notification(
            bounty_author_id, g.user.id, content, bounty_id=bounty.id
        )
        return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))
    if not bounty:
        flash("Not Found")
        return redirect(url_for('bounty.open_bounties'))
    responses = bounty.responses
    return render_template(
        'bounty/bounty_detail.html',
        bounty=bounty,
        responses=responses,
        max_zap=_get_max_withdrawal_amount()
    )


@bp.route('/bounty_response/<bounty_response_code>', methods=("GET",))
def view_bounty_response(bounty_response_code):
    """
    Finds a bounty based on bounty response
    """
    bounty_code = _get_bounty_id_by_response_code(bounty_response_code)
    return redirect(url_for(BOUNTY_DETAIL_VIEW, bounty_code=bounty_code))


@bp.route('/more-all-bounties', methods=("POST",))
def more_all_bounties():
    offset = request.form.get('offset')
    bounties = _get_all_bounties(limit=offset, offset=offset)
    html = render_template(INJECTABLE_BOUNTIES, bounties=bounties)
    return {'offset': int(offset) + int(offset), 'html': html}


@bp.route('/more-open-bounties', methods=("POST",))
def more_open_bounties():
    offset = request.form.get('offset')
    bounties = _get_open_bounties(limit=offset, offset=offset)
    html = render_template(INJECTABLE_BOUNTIES, bounties=bounties)
    return {'offset': int(offset) + int(offset), 'html': html}


@bp.route('/more-closed-bounties', methods=("POST",))
def more_closed_bounties():
    offset = request.form.get('offset')
    bounties = _get_closed_bounties(limit=offset, offset=offset)
    html = render_template(INJECTABLE_BOUNTIES, bounties=bounties)
    return {'offset': int(offset) + int(offset), 'html': html}


@bp.route('/more-user-bounties', methods=("POST",))
def more_user_bounties():
    offset = request.form.get('offset')
    user_id = _get_current_user_id()
    bounties = _get_user_bounties(user_id, limit=offset, offset=offset)
    html = render_template(INJECTABLE_BOUNTIES, bounties=bounties)
    return {'offset': int(offset) + int(offset), 'html': html}


@bp.route('/more-user-responded-bounties', methods=("POST",))
def more_user_responded_bounties():
    offset = request.form.get('offset')
    user_id = _get_current_user_id()
    bounties = _get_bounties_user_responded_to(user_id, limit=offset, offset=offset)
    html = render_template(INJECTABLE_BOUNTIES, bounties=bounties)
    return {'offset': int(offset) + int(offset), 'html': html}
