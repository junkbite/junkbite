from flask import Blueprint

bp = Blueprint('bounty', __name__)

from app.bounty import routes
