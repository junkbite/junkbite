import csv
import click
from datetime import datetime, timedelta
from flask.cli import with_appcontext
from flask import current_app
from app import db
from werkzeug.security import generate_password_hash
from lndgrpc import LNDClient
from app.models import (
    User,
    Post,
    Comment,
    UserBalance,
    UserBalanceEntry,
    Payment,
    PaymentProcessingFee,
    Withdrawal,
    JunkbiteNode,
    AdminView,
    LightningAppConfiguration,
    Settings
)
from app.utils.db_utils import (
    _create_user,
    _freeze_withdrawals,
    _get_max_withdrawal_amount
)
from app.utils.accounting_utils import (
    _get_payments_last_x_hours,
    _get_balance_entries_associated_to_payments,
    _get_balance_entries_last_x_hours
)
from app.email import (
    sendgrid_send_withdrawal_issue_alert,
    sendgrid_withdrawals_frozen_alert
)


@click.command('add-users')
@click.argument('file')
@with_appcontext
def add_users(file):
    """
    loads fake users from a csv into the database for testing purposes
    # Example:
    username,password,email,admin,private,unique_filename,link,bio
    fakey,fakey-pass,fake@gmail.com,0,default-profile.png,junkbite.com,#bitcoin
    """
    with open(file, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            user = User(
                username=row[0],
                email=row[2],
                admin=row[3],
                unique_filename=row[4],
                link=row[5],
                bio=row[6]
            )
            user.set_password(row[1])
            db.session.add(user)
            print('Adding User: {}'.format(user))
    db.session.commit()


@click.command('list-pending-withdrawals')
@with_appcontext
def list_pending_withdrawals():
    """
    Displays a list of pending withdrawal requests
    """
    pending_withdrawal_query = Withdrawal.query.filter_by(status="requested")
    pending_withdrawals = []
    for w in pending_withdrawal_query:
        pending_withdrawals.append(w.serialized())
    if pending_withdrawals:
        print("Status,Amount,Initiated,Lightning")
    else:
        print("No pending withdrawals")
    for w in pending_withdrawals:
        print("{},{},{},{}".format(
            w['status'], w['amount'], w['initiated'], w['ln_invoice'])
        )


@click.command('accounting')
@with_appcontext
def accounting():
    """
    Checks all user balance updates against the lightning nodes transactions
    to confirm all real transactions
    """
    settings = Settings.query.get(1)
    if not settings.withdrawals_frozen:
        now = datetime.now()
        hours_ago=1
        payments = _get_payments_last_x_hours(now=now, hours_ago=hours_ago)
        balance_entries = _get_balance_entries_associated_to_payments(payments)
        # Ensure each payment lines up with a corresponding balance update
        '''
        UPDATE THIS TO CHECK AGAINST BOUNTY AWARDS TOO
        balance_entries_2 = _get_balance_entries_last_x_hours(now=now, hours_ago=hours_ago)
        if balance_entries != balance_entries_2:
            # ALERT ADMIN - FREEZE WITHDRAWALS
            print("BALANCE ENTRIES NOT EQUAL")
            info = {
                'balance_query_one': balance_entries,
                'balance_query_two': balance_entries_2,
                'message': 'BALANCE ENTRY BY PAYMENT DIFFERS WHEN SEARCHED BY TIME'
            }
            sendgrid_withdrawals_frozen_alert(info)
            _freeze_withdrawals()
        else:
            print('balances okay')
        '''
        # Ensure a lightning invoice is used only once in a payment and once
        # in a user balance update
        ln_invoices = [p.ln_invoice for p in payments]
        ln_invoices_set = set(ln_invoices)
        if len(ln_invoices) != len(ln_invoices_set):
            # ALERT ADMIN - FREEZE WITHDRAWALS
            info = {
                'invoices': ln_invoices,
                'invoices_set': ln_invoices_set,
                'message': 'DUPLICATE INVOICE USE IN PAYMENT'
            }
            sendgrid_withdrawals_frozen_alert(info)
            _freeze_withdrawals()
            print("DUPLICATE INVOICE USE IN PAYMENT")
        else:
            print("payment invoices okay")
        ln_invoice_balance_updates = [e.ln_invoice for e in balance_entries]
        ln_invoice_balance_updates_set = set(ln_invoice_balance_updates)
        if len(ln_invoice_balance_updates) != len(ln_invoice_balance_updates_set):
            # ALERT ADMIN - FREEZE WITHDRAWALS
            print("SAME INVOICE USED IN MULTIPLE BALANCE UPDATES!")
            info = {
                'invoices': ln_invoice_balance_updates,
                'invoices_set': ln_invoice_balance_updates_set,
                'message': 'DUPLICATE INVOICE USE TO UPDATE USER BALANCE'
            }
            sendgrid_withdrawals_frozen_alert(info)
            _freeze_withdrawals()
        else:
            print('balance invoices okay')

        # Now check actual balance amounts and payment amounts and ln invoice amounts
        lnd = LNDClient(
            ip_address=current_app.config["LND_RPC_IP"],
            macaroon_filepath=current_app.config['LND_MACAROON'],
            cert_filepath=current_app.config['LND_TLS_CERT'],
            admin=True
        )
        for payment in payments:
            print(payment.ln_invoice)
            if payment.ln_invoice:  # can be None
                try:
                    dpr = lnd.decode_payment_request(payment.ln_invoice)
                except:
                    dpr = "INVALID INVOICE"
                    info = {
                        'payment_amount': payment.amount,
                        'invoice_amount': dpr,
                        'message': 'FALSIFIED INVOICE FOUND - LND UNABLED TO DECODE INVOICE'
                    }
                    sendgrid_withdrawals_frozen_alert(info)
                    _freeze_withdrawals()
                    print("INVALID INVOICE FOUND")
                    continue
        
                if payment.amount != dpr.num_satoshis:
                    # FREEZE USER AND WITHDRAWALS
                    print("FALSE INVOICE FOUND")
                    info = {
                        'payment_amount': payment.amount,
                        'invoice_amount': dpr.num_satoshis,
                        'message': 'FALSIFIED INVOICE FOUND - LND INVOICE AMOUNT DOES NOT MATCH PAYMENT'
                    }
                    print(info)
                    sendgrid_withdrawals_frozen_alert(info)
                    _freeze_withdrawals()
                else:
                    print(f"Payment okay: {payment.amount} == {dpr.num_satoshis}")

        for entry in balance_entries:
            dpr = lnd.decode_payment_request(entry.ln_invoice)
            # REMEBER JUNKBITE PERCENTAGE
            # TODO TAKE PERCENTAGE FROM PAYMENT FEE
            fee = Payment.query.filter_by(ln_invoice=entry.ln_invoice).first().fee
            invoice_minus_cut = dpr.num_satoshis - fee.amount
            if entry.amount != invoice_minus_cut:
                # FREEZE USER AND WITHDRAWALS
                print(f"FALSE INVOICE FOUND: {entry.amount} == {invoice_minus_cut}")
                info = {
                    'balance_update_amount': payment.amount,
                    'invoice_amount': dpr.num_satoshis,
                    'invoice_minus_cut': invoice_minus_cut,
                    'message': 'FALSIFIED INVOICE FOUND - LND INVOICE AMOUNT DOES NOT MATCH BALANCE UPDATE AMOUNT'
                }
                sendgrid_withdrawals_frozen_alert(info)
                _freeze_withdrawals()
            else:
                print(f"Entry okay: {entry.amount} == {invoice_minus_cut}")


@click.command('pay-pending-withdrawals')
@with_appcontext
def pay_pending_withdrawals():
    """
    Pay all pending lightning invoice withdrawals and update the user's balance
    """
    pending_withdrawal_query = Withdrawal.query.filter_by(status="requested")
    pending_withdrawals = []
    for w in pending_withdrawal_query:
        pending_withdrawals.append(w)
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    now = datetime.now()
    for w in pending_withdrawals:
        user = w.user
        # get any withdraws in past twenty four hours
        now = datetime.now()
        twenty_four_hours = now - timedelta(hours=24)
        recent_previous_withdraws = (
            Withdrawal.query.filter(
                Withdrawal.user==user,
                Withdrawal.status=="completed",
                Withdrawal.completed > twenty_four_hours
            )
            .all()
        )
        if not recent_previous_withdraws:
            try:
                user_balance = (
                    UserBalance.query.filter_by(user_id=w.user_id).first()
                )
                # probably need an alert here for big withdrawals
                maximum_withdrawal_amount = _get_max_withdrawal_amount()
                if w.amount > maximum_withdrawal_amount:
                    w.status = "failed"
                    w.notes = "Max withdrawal per attempt is {maximum_withdrawal_amount} sats"
                    w.completed = now
                elif user_balance.balance - w.amount < 0:
                    w.status = "failed"
                    w.notes = "insufficient balance"
                    w.completed = now
                else:
                    response = lnd.send_payment(w.ln_invoice)
                    if not response.payment_error:
                        w.status = "completed"
                        w.notes = 'payment sent successfully'
                        w.completed = now
                        user_balance.balance -= w.amount
                        # save user balance
                        db.session.add(user_balance)
                    else:
                        w.status = 'failed'
                        w.notes = response.payment_error
                        current_app.logger.info(response.payment_error)
                        sendgrid_send_withdrawal_issue_alert(response.payment_error)
                # save withdraw info
                db.session.add(w)
                db.session.commit()
            except Exception as e:
                print(type(e))
                error_msg = e.details()
                w.status = "failed"
                w.notes = error_msg
                w.completed = now
                db.session.add(w)
                db.session.commit()
                sendgrid_send_withdrawal_issue_alert("Exception on withdraw")
        else:
            w.status = "failed"
            w.notes = "Rewards can only be withdrawn once per day"
            w.completed = now
            db.session.commit()


@click.command('create-user')
@click.argument('username', type=click.STRING)
@click.argument('password', type=click.STRING)
@with_appcontext
def create_user(username, password):
    """
    Creates a regular user -- usage: username password
    """
    click.echo(f'Initializing new user: {username}')
    _create_user(username, password, False)


@click.command('create-admin')
@click.argument('username', type=click.STRING)
@click.argument('password', type=click.STRING)
@with_appcontext
def create_admin(username, password):
    """
    Create a new admin user -- usage: username password
    """
    click.echo('Initializing new administrative user.')
    _create_user(username, password, True)


@click.command('create-anonymous')
@click.argument('password', type=click.STRING)
@with_appcontext
def create_anonymous(password):
    """
    Create anonymous user -- usage: password

    This is used for creating advertisements without having to be a user
    """
    click.echo('Initializing new anonymous user account')
    _create_user('anonymous', password, False)


@click.command('reset-password')
@click.argument('username', type=click.STRING)
@click.argument('password', type=click.STRING)
@with_appcontext
def reset_password(username, password):
    """
    Updates an existing user's password
    """
    user = User.query.filter_by(username=username).first()
    if user:
        user.set_password(password)
        db.session.add(user)
        db.session.commit()
        click.echo("Password updated.")
    else:
        click.echo("User not found.")


@click.command('add-admin')
@click.argument('username', type=click.STRING)
@with_appcontext
def add_admin(username):
    """
    Create a new admin user -- usage: username password
    """
    user = User.query.filter_by(username=username).first()
    if user:
        user.admin = True
        db.session.add(user)
        db.session.commit()
        click.echo("User added as admin")
    else:
        click.echo("User not found.")


@click.command('remove-admin')
@click.argument('username', type=click.STRING)
@with_appcontext
def remove_admin(username):
    """
    Removes a user's admin privileges
    """
    user = User.query.filter_by(username=username).first()
    if user:
        user.admin = False
        db.session.add(user)
        db.session.commit()
        click.echo("User removed from admin")
    else:
        click.echo("User not found.")


@click.command('add-junkbite-node-record')
@click.argument('title')
@click.argument('pubkey')
@click.argument('uri')
@with_appcontext
def add_junkbite_node_record(title, pubkey, uri):
    """
    Stores junkbite's lightning node information into the database for display
    to users trying to connect
    """
    jbn = JunkbiteNode(
        title=title,
        pubkey=pubkey,
        uri=uri
    )
    db.session.add(jbn)
    db.session.commit()


@click.command('generate-admin-views')
@with_appcontext
def generate_admin_views():
    """
    Sort of a place holder function, this is needed for admin model management
    """
    base_model_list = [
        ['User', '/users', 'admin_users', 'id,username,email,created,last_login,admin','ID,Username,Email,Created,Last Login,Is Admin?'],
        ['Post', '/posts', 'admin_posts', 'id,post_code,username,created,title','ID,Code,Author,Created,Title'],
        ['Comment', '/comments', 'admin_comments', 'id,comment_code,username,post_id,body,created','Comment ID,Code,User ID,Post ID,Comment,Created'],
        ['ReportedContent', '/reported-content', 'admin_reported_content', 'id,username,email,created,last_login,admin','Report ID,Username,Email,Created,Last Login,Is Admin?'],
        ['Bounty', '/bounties', 'admin_bounties', 'id,bounty_code,user_id,status,title,total_sats,created', 'Bounty ID,Code,User ID,Status,Title,Total Sats,Created'],
        ['BountyResponse', '/bounty-responses', 'admin_bounty_responses', 'id,bounty_response_code,bounty_id,user_id,body,created', 'Response ID,C0de,Bounty ID,User ID,Body,Created'],
        ['Payment', '/payments', 'admin_payments', 'id,payment_code,sending_user_id,amount,junkbite_app,status,created', 'ID,Code,Sending User ID,Amount,App,Status,Created'],
        ['Notification', '/notifications', 'admin_notifications', 'id,notification_code,to_user_id,from_user_id,created,content','ID,Code,To User ID,From User ID,Created,Content'],
        ['Feedback', '/feedback', 'admin_feedback', 'id,feedback_code,user.username,description,created', 'ID,Code,Username,Description,Created'],
        ['SupportTicket', '/support', 'admin_support', 'id,username,email,created,last_login,admin','id,username,email,created,last_login,admin'],
        ['JunkbiteNode', '/junkbite-node', 'admin_junkbite_node', 'id,username,email,created,last_login,admin', 'id,username,email,created,last_login,admin'],
        ['Blog', '/blogs', 'admin_blogs', 'id,code,author_id,slug,title,body', 'ID,Code,User ID,Slug,Title,Body'],
        ['UserBalance', '/user-balances', 'admin_user_balances', 'id,user_balance_code,user_id,balance', 'ID,Code,User ID,Balance'],
        ['UserBalanceEntry', '/user-balance-entries', 'admin_user_balance_entries', 'id,user_balance_id,before_balance,action_type,amount,timestamp', 'ID,User Balance ID,Before Balance,Action Type,Amount,Timestamp'],
        ['Withdrawal', '/withdrawals', 'admin_withdrawals', 'id,withdrawal_code,user_id,created,completed,status,amount', 'id,withdrawal_code,user_id,created,completed,status,amount'],
    ]
    AdminView.query.delete()
    db.session.commit()
    for item in base_model_list:
        if AdminView.query.filter_by(name=item[0]).first():
            continue
        view = AdminView(
            name=item[0],
            route=item[1],
            function=item[2],
            field_names=item[3],
            field_headers=item[4]
        )
        db.session.add(view)
        db.session.commit()


@click.command("default-config-lightning-apps")
@with_appcontext
def default_config_lightning_apps():
    """
    This just generates the base configuration for lightning apps which can
    be changed later in admin
    """
    lightning_apps = {
        "random": [250, "See a random post"],
        "send": [250, "Send a tweet"],
        "banner": [350, "Update Banner Image"],
        "profile": [350, "Update Profile Image"]
    }
    for name, info in lightning_apps.items():
        app_config = LightningAppConfiguration(
            application_name=name,
            price_for_function=info[0],
            utility_field=info[1]
        )
        db.session.add(app_config)
    db.session.commit()


def init_app(app):
    """
    app.teardown_appcontext() tells Flask to call that function when cleaning
    up after returning the response.

    app.cli.add_command() adds a new command that can be called with the flask
    command.
    """
    app.cli.add_command(create_user)
    app.cli.add_command(create_admin)
    app.cli.add_command(create_anonymous)
    app.cli.add_command(add_admin)
    app.cli.add_command(accounting)
    app.cli.add_command(remove_admin)
    app.cli.add_command(add_junkbite_node_record)
    app.cli.add_command(list_pending_withdrawals)
    app.cli.add_command(pay_pending_withdrawals)
    app.cli.add_command(reset_password)
    app.cli.add_command(generate_admin_views)
    app.cli.add_command(default_config_lightning_apps)
