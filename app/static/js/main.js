$(document).ready(function() {
    // idk how to use this yet
    try {
        document.getElementById('getval').addEventListener('change', readUrl, true);
        document.getElementById('getval-banner-img').addEventListener('change', showTwitterBannerImg, true);
        document.getElementById('getval-profile-img').addEventListener('change', showTwitterProfileImg, true);
    } catch {}

    // Initialize Poppers Tooltip Library everywhere
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
});


window.addEventListener('scroll', _.throttle(loadOnScroll, 800));

function loadOnScroll() {
    let end = $("#baseFooter").offset().top;
    let viewEnd = $(window).scrollTop() + $(window).height();
    let distance = end - viewEnd;
    let distanceFromBottomToTrigger = 300;
    if (distance < distanceFromBottomToTrigger) {
        if ( window.location.pathname.includes("home") || window.location.pathname == "/" ) {
            requestMoreThreads();
        } else if ( window.location.pathname.includes("post") ){
            requestMoreComments();
        } else if ( window.location.pathname.includes("images") ){
            requestMoreImages();
        } else if ( window.location.pathname.includes("explore") ) {
            requestMoreExplore();
        } else if ( window.location.pathname.includes("open-bounties") ) {
            requestMoreOpenBounties();
        } else if ( window.location.pathname.includes("closed-bounties") ) {
            requestMoreClosedBounties();
        } else if ( window.location.pathname.includes("all-bounties") ) {
            requestMoreAllBounties();
        } else if ( window.location.pathname.includes("user-bounties") ) {
            requestMoreUserBounties();
        } else if ( window.location.pathname.includes("user-bounty-responses") ) {
            requestMoreUserRespondedBounties();
        } else if ( window.location.pathname.includes("likes") ) {
            requestMoreUserLikes();
        } else if ( window.location.pathname.includes("bookmarks") ) {
            requestMoreUserBookmarks();
        } else if ( window.location.pathname.includes("zaps") ) {
            requestMoreUserZaps();
        } else if ( window.location.pathname.includes("received-payment-history") ) {
            requestMoreReceivedPayments();
        } else if ( window.location.pathname.includes("sent-payment-history") ) {
            requestMoreSentPayments();
        } else if ( window.location.pathname.includes("/u/") ) {
            requestMoreUserProfilePosts();
        } else if ( window.location.pathname.includes("/notifications") ) {
            requestMoreNotifications();
        }
    }
}


function loadDefaultProfileImg(id) {
    $(`#${id}`).attr("src","/static/images/default-profile.png");
}

function readUrl() {
    var file = document.getElementById("getval").files[0];
    var video_source = document.getElementById('source-me');
    var video = document.getElementById('load-video');
    var reader = new FileReader();
    reader.onloadend = function () {
        // set video source and load if its a webm or mp4 file
        if (file.name.includes('.webm') || file.name.includes('.mp4') || file.name.includes('.mov')) {
            video_source.src = reader.result;
            video.load();
            video.style.display = "block";
        } else {
            var img = document.getElementById('load-img');
            img.style.display = "block";
            img.src = reader.result;
        }
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}


function showTwitterBannerImg() {
    let file = document.getElementById("getval-banner-img").files[0];
    let reader = new FileReader();
    reader.onloadend = function () {
        var img = document.getElementById("load-twitter-banner-img");
        img.style.display = "block";
        img.src = reader.result;
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}


function showTwitterProfileImg() {
    let file = document.getElementById("getval-profile-img").files[0];
    let reader = new FileReader();
    reader.onloadend = function () {
        var img = document.getElementById("load-twitter-profile-img");
        img.style.display = "block";
        img.src = reader.result;
    }
    if (file) {
        reader.readAsDataURL(file);
    }
}

function saveDraftProgress(url) {
    $("#createForm").attr('action', url);
    $("#createForm").submit();
}

function savePostDraft(url) {
    $("#createForm").attr('action', url);
    $("#createForm").submit();
}

function saveArticleDraft(url) {
    // Copy the Quill JS HTML into a text area in the form to submit it
    if ($("#createArticleTitle").val().length === 0) {
        $("#createPostAlert").html("Title is required").show();
    } else {
        $("#hiddenTextArea").val($("#editor .ql-editor").html());
        $("#createArticleForm").submit();
    }
    $("#createArticleForm").attr('action', url);
    $("#createArticleForm").submit();
}

function copyTextToMeme(input) {
    /*
    Displays the text entered into `title` on create form above 
    or below the chosen image
    */
    try {
        let meme_text = document.getElementById('meme-text')
        meme_text.innerHTML = input;
    } catch {}
    try {
        let post_text = document.getElementById('post-text')
        post_text.innerHTML = input;
    } catch {}
}


function copyArticleTitleToPreview(input) {
    /*
    Displays the text entered into `title` on create form above 
    or below the chosen image
    */
    $("#previewHeader").html(input);
}


function copyArticleTitleToPreview1(input) {
    /*
    Displays the text entered into `title` on create form above 
    or below the chosen image
    */
    $("#createArticleTitle").html(input);
}


function toggleLightningInvoice(id) {
    let invoice = document.getElementById(`lightningInvoice${id}`);
    invoice.show();
}

function toggleCommentForm(id) {
    let commentForm = $(`#commentForm${id}`);
    commentForm.toggle();
    let textarea = $(`#inlineComment${id}`);
    textarea[0].focus();
}

function toggleCommentNotLoggedInForm(id) {
    let commentForm = $(`#commentNotLoggedInForm${id}`);
    commentForm.toggle();
}

function toggleCommentFormNested(id) {
    let commentForm = $(`#commentFormNested${id}`);
    commentForm.toggle();
    let textarea = $(`#inlineCommentNested${id}`);
    console.log(textarea);
    textarea[0].focus();
}

function expandComment(id) {
    let div = $(`#commentDiv${id}`);
    if (!div.data("expanded")) {
        $.post('/get-nested-comments', {
            commentCode: id,
        }).done((response) => {
            div.append(response.html);
            div.data("expanded", true)
        }).fail(() => {
            console.log('failed');
        });
    }
}

function toggleCommentNotLoggedInFormNested(id) {
    let commentForm = $(`#commentNotLoggedInFormNested${id}`);
    commentForm.toggle();
}

function toggleIncreaseBountyForm(id) {
    let increaseBountyForm = $(`#bountyZapForm${id}`);
    increaseBountyForm.toggle();
}

function toggleReportComment(id) {
    let reportComment = $(`#reportComment${id}`);
    reportComment.toggle();
}

function toggleReportForm(postCode) {
    let reportForm = $(`#reportForm${postCode}`);
    reportForm.toggle();
}

function toggleDeleteThread() {
    let dangerZone = $("#dangerZone");
    dangerZone.toggle();
}

function toggleDeleteComment(comment_id) {
    let dangerZone = $(`#deleteComment${comment_id}`);
    dangerZone.toggle();
}

function toggleShowPost(postCode) {
    /*
    Shows or Hides a post on the post.html page
    */
    let post = $(`#post-container-${postCode}`);
    post.toggle();
}

function toggleShowComment(comment_id) {
    /*
    Relevant to posts.html

    hides/shows a comment in a thread by clicking the square in corner
    of the header
    */
    let comment = $(`#commentContainer${comment_id}`);
    comment.toggle();
}

function showQRCode(qrElementId, message) {
    new QRCode($(`#${qrElementId}`)[0], {
        text: message,
        width: 400,
        height: 400,
        colorDark: "#000000",
        colorLight: "#ffffff",
        correctLevel: QRCode.CorrectLevel.H,
    });
}

function showInputSatsError(postCode) {
    let span = $(`#zapError${postCode}`);
    span.show();
}

function hideInputSatsError(postCode) {
    let span = $(`#zapError${postCode}`);
    span.hide();
}

function getInvoice(postCode, maxSats) {
    let amount = $(`#input-sats${postCode}`).val();
    let amountInt = parseInt(amount)
    if (amountInt < 1 || amountInt > maxSats) {
        showInputSatsError(postCode);
        return;
    }
    window.amount = amount;
    $.post('/get-invoice', {
        postCode: postCode,
        amount: amount
    }).done((response) => {
        $(`#qrcode${postCode}`).empty();
        toggleThreadZap(postCode);
        // hide previously completed invoice
        showPaymentIncomplete(postCode);
        let invoiceTextElement;
        invoiceTextElement = $(`#invoiceText${postCode}`)[0];
        invoiceTextElement.value = response.message;
        showQRCode(`qrcode${postCode}`, response.message);
        window.invoice = response.message;
        window.postCode = postCode
        $(`#invoice${postCode}`).modal('show');
        checkPaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function getCommentInvoice(commentCode, maxSats) {
    let amount = $(`#zapCommentFormInput${commentCode}`).val();
    console.log(amount);
    let amountInt = parseInt(amount)
    if (amountInt < 1 || amountInt > maxSats) {
        showInputSatsError(commentCode);
        return;
    }
    window.amount = amount;
    $.post('/get-invoice', {
        commentCode: commentCode,
        amount: amount
    }).done((response) => {
        $(`#qrcode${commentCode}`).empty();
        toggleZapCommentForm(commentCode);
        // hide previously completed invoice
        showPaymentIncomplete(commentCode);
        let invoiceTextElement;
        invoiceTextElement = $(`#invoiceText${commentCode}`)[0];
        invoiceTextElement.value = response.message;
        showQRCode(`qrcode${commentCode}`, response.message);
        window.invoice = response.message;
        window.commentCode = commentCode
        $(`#invoice${commentCode}`).modal('show');
        checkCommentPaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function showInputSatsErrorBounty() {
    let span = $("#zapBountyError");
    span.show();
}

function hideInputSatsErrorBounty() {
    let span = $("#zapBountyError");
    span.hide();
}

function getBountyInvoice(bountyId, maxSats) {
    let amount = $(`#increaseBounty${bountyId}`).val();
    let amountInt = parseInt(amount)
    if (amountInt < 1 || amountInt > maxSats) {
        showInputSatsErrorBounty();
        return;
    }
    window.bountyId = bountyId;
    window.amount = amount;
    $.post('/get-bounty-invoice', {
        bountyId: bountyId,
        amount: amount
    }).done((response) => {
        $(`#qrcodeBounty${bountyId}`).empty();
        $(`#invoiceBountyText${bountyId}`).val(response.message);
        showQRCode(`qrcodeBounty${bountyId}`, response.message);
        window.invoice = response.message;
        window.bountyId = bountyId;
        $(`#invoiceBounty${bountyId}`).modal('show');
        checkBountyPaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function showInputSatsErrorBountyResponse(responseId) {
    let span = $(`#zapBountyResponseError${responseId}`);
    span.show();
}

function hideInputSatsErrorBountyResponse(responseId) {
    let span = $(`#zapBountyResponseError${responseId}`);
    span.hide();
}

function getBountyResponseInvoice(responseId, maxSats) {
    let amount = $(`#input-sats${responseId}`).val();
    let amountInt = parseInt(amount)
    if (amountInt < 1 || amountInt > maxSats) {
        showInputSatsErrorBountyResponse(responseId);
        return;
    }
    window.responseId = responseId;
    window.amount = amount;
    $.post('/get-bounty-response-invoice', {
        responseId: responseId,
        amount: amount
    }).done((response) => {
        $(`#qrcode${responseId}`).empty();
        $(`#invoiceText${responseId}`).val(response.message);
        showQRCode(`qrcode${responseId}`, response.message);
        window.invoice = response.message;
        window.responseId = responseId;
        $(`#invoice${responseId}`).modal('show');
        checkBountyResponsePaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function getCreateBountyInvoice(maxSats) {
    let amount = $(`#input-sats`).val()
    let amountInt = parseInt(amount)
    if (amountInt < 1 || amountInt > maxSats) {
        showInputSatsErrorBounty();
        return;
    }
    window.amount = amount;
    $.post('/get-create-bounty-invoice', {
        amount: amount,
    }).done((response) => {
        $(`#qrcode`).empty();
        let invoiceTextElement;
        invoiceTextElement = $(`#invoiceText`)[0];
        invoiceTextElement.value = response.message;
        showQRCode(`qrcode`, response.message);
        try {
            $(`#invoiceSubmit`).val(response.message);
        } catch {}
        $(`#invoiceText`).val(response.message);
        $(`#invoice`).modal('show');
        window.invoice = response.message;
        checkGeneralPaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function getGeneralInvoice() {
    let amount = $(`#input-sats`).val()
    window.amount = amount;
    $.post('/get-general-invoice', {
        amount: amount,
    }).done((response) => {
        $(`#qrcode`).empty();
        let invoiceTextElement;
        invoiceTextElement = $(`#invoiceText`)[0];
        invoiceTextElement.value = response.message;
        showQRCode(`qrcode`, response.message);
        try {
            $(`#invoiceSubmit`).val(response.message);
        } catch {}
        $(`#invoiceText`).val(response.message);
        $(`#invoice`).modal('show');
        window.invoice = response.message;
        checkGeneralPaymentComplete();
    }).fail(() => {
        console.log('failed');
    });
}

function getTwitterInvoice(func) {
    $.post('/get-twitter-invoice', {
        func: func,
    }).done((response) => {
        $(`#qrcode`).empty();
        let invoiceTextElement;
        invoiceTextElement = $(`#invoiceText`)[0];
        invoiceTextElement.value = response.message;
        showQRCode(`qrcode`, response.message);
        try {
            $(`#invoiceSubmit`).val(response.message);
        } catch {}
        $(`#invoiceText`).val(response.message);
        $(`#invoice`).modal('show');
        window.invoice = response.message;
        checkGeneralPaymentComplete(func);
    }).fail(() => {
        console.log('failed');
    });
}

function submitComment(postCode="", commentCode=null) {
    let comment = $(`#inlineComment${postCode}`);
    let gifSearch = $(`#gifSearch${postCode}`).val();
    let gif = $(`#gif${postCode}`).val();
    if (!$.trim(comment.val()) && !gif) {
        $.toast({
            title: "Reply cannot be empty",
            subtitle: null,
            content: null,
            type: 'warning',
            pause_on_hover: true,
            delay: 1500
        });
    } else {
        $.post('/new-comment', {
            postCode: postCode,
            comment: comment.val(),
            gifSearch: gifSearch,
            commentCode: commentCode,
            gif: gif
        }).done((response) => {
            if (response.message === 'success') {
                incrementCommentCount(postCode);
                comment.val('');
                toggleCommentForm(postCode);
                $.toast({
                    title: 'Reply Sent',
                    subtitle: null,
                    content: null,
                    type: 'success',
                    pause_on_hover: true,
                    delay: 1000
                });
            } else {
                comment.val('');
                toggleCommentForm(postCode);
                $.toast({
                    title: response.message,
                    subtitle: null,
                    content: null,
                    type: 'warning',
                    pause_on_hover: true,
                    delay: 1000
                });
            }
        }).fail(() => {
            console.log('failed');
            $.toast({
                title: 'Message Failed, Try again later',
                subtitle: null,
                content: null,
                type: 'error',
                pause_on_hover: true,
                delay: 1500
            });
        });
    }
}

function submitCommentNested(postCode, commentCode) {
    let comment = $(`#inlineCommentNested${commentCode}`);
    let gifSearch = $(`#gifSearch${commentCode}`).val();
    let gif = $(`#gif${commentCode}`).val();
    console.log(comment);
    if (!$.trim(comment.val())) {
        $.toast({
            title: "Reply cannot be empty",
            subtitle: null,
            content: null,
            type: 'warning',
            pause_on_hover: true,
            delay: 1500
        });
    } else {
        $.post('/new-comment', {
            postCode: postCode,
            commentCode: commentCode,
            comment: comment.val(),
            gifSearch: gifSearch,
            gif: gif
        }).done((response) => {
            if (response.message === 'success') {
                incrementCommentCount(postCode);
                comment.val('');
                toggleCommentFormNested(commentCode);
                $.toast({
                    title: 'Reply Sent',
                    subtitle: null,
                    content: null,
                    type: 'success',
                    pause_on_hover: true,
                    delay: 1500
                });
            } else {
                comment.val('');
                toggleCommentFormNested(commentCode);
                $.toast({
                    title: response.message,
                    subtitle: null,
                    content: null,
                    type: 'warning',
                    pause_on_hover: true,
                    delay: 1500
                });
            }
        }).fail(() => {
            console.log('failed');
            $.toast({
                title: 'Message Failed, Try again later',
                subtitle: null,
                content: null,
                type: 'error',
                pause_on_hover: true,
                delay: 1500
            });
        });
    }
}

function randomPaymentComplete() {
    let qr = $(`#qrcode`);
    let qrComplete = $(`#qrcode-completed`);
    let lightningInvoiceText = $(`#lightningInvoiceText`);
    qr.hide();
    qrComplete.show();
    lightningInvoiceText.hide();
}

function checkGeneralPaymentComplete(func){
    let poll = function() {
        let invoice = window.invoice;
        $.post('/check-payment', {
            lnInvoice: invoice,
        }).done((response) => {
            if (response.message) {
                if ( window.location.pathname.includes("create-bounty") ) {
                    submitBountyForm();
                } else if (window.location.pathname.includes("lightning") ) {
                    if (func === "send") {
                        $("#newTweetForm").submit();
                    } else if (func === "banner") {
                        $("#tweet-update-banner-form").submit();
                    } else if (func === "profile") {
                        $("#tweet-update-profile-form").submit();
                    } else {
                        $("#random-post-invoice-form").submit();
                    }
                } else {
                    showGeneralPaymentComplete();
                }
                clearInterval(pollInterval)
            }
        }).fail(() => {
            console.log('failed');
        });
    }
    let timeElapsed = 0;
    let intervalTime = 2000;
    let totalTime = 60000;
    let pollInterval = setInterval(function() {
        timeElapsed += intervalTime;
        // stop polling after maxTime -- based on 1 minute lightning invoice
        if (timeElapsed === totalTime) {
            clearInterval(pollInterval);
        }
        // if user closes modal stop polling
        if ( !($(`#invoice`).data('bs.modal') || {})._isShown ) {
            clearInterval(pollInterval);
        }
        poll();
    }, intervalTime);
}

function showGeneralPaymentComplete() {
    $("#invoice-form").submit();
}

function validateFormThenGetGetInvoice(maxSats) {
    let continueToInvoice = true;
    let titleError = false;
    let bountyError = false;
    let errorMessage = "";
    $("#hiddenTextArea").val($("#editor .ql-editor").html());
    $('#invoice-form input:text, textarea').each(function() {
        if ($(this).attr('id') === "title") {
            if($(this).val() === undefined || $(this).val() == "") {
                continueToInvoice = false;
                titleError = true;
            }
        }
        if ($(this).attr('id') === "hiddenTextArea") {
            if ($(this).val() === undefined || $(this).val() == "<p><br></p>") {
                continueToInvoice = false;
                bountyError = true;
            }
        }
    });
    if (continueToInvoice) {
        getCreateBountyInvoice(maxSats);
    } else {
        if ( titleError && bountyError ) {
            errorMessage = "Title and Bounty Information are required";
            $("#createPostAlert").html(errorMessage).show();
        } else if ( titleError ) {
            errorMessage = "Title is required";
            $("#createPostAlert").html(errorMessage).show();
        } else if ( bountyError ) {
            errorMessage = "Bounty Information is required";
            $("#createPostAlert").html(errorMessage).show();
        }
    }
}

function submitBountyForm() {
    // Copy the Quill JS HTML into a text area in the form to submit it
    if ($("#title").val().length === 0) {
        $("#createPostAlert").html("Title is required").show();
    } else {
        $("#hiddenTextArea").val($("#editor .ql-editor").html());
        $("#invoice-form").submit();
    }
}

function submitArticleForm() {
    // Copy the Quill JS HTML into a text area in the form to submit it
    if ($("#createArticleTitle").val().length === 0) {
        $("#createPostAlert").html("Title is required").show();
    } else {
        $("#hiddenTextArea").val($("#editor .ql-editor").html());
        $("#createArticleForm").submit();
    }
}

function submitCreateForm() {
    // There was other stuff here but now its simplified.
    // leaving this for more potential processing later on
    $("#createForm").submit();
}

function saveEditForm() {
    // There was other stuff here but now its simplified.
    // leaving this for more potential processing later on
    $("#createForm").submit();
}

function checkBountyPaymentComplete() {
    let poll = function() {
        $.post('/check-payment', {
            lnInvoice: window.invoice,
            bountyId: window.bountyId
        }).done((response) => {
            if (response.message) {
                showBountyPaymentComplete(bountyId);
                clearInterval(pollInterval);
            }
        }).fail(() => {
            console.log('failed');
        });
    }
    let timeElapsed = 0;
    let intervalTime = 2000;
    let totalTime = 60000;
    let pollInterval = setInterval(function() {
        timeElapsed += intervalTime;
        // stop polling after maxTime -- based on 1 minute lightning invoice
        if (timeElapsed === totalTime) {
            clearInterval(pollInterval);
        }
        // if user closes modal stop polling
        if ( !($(`#invoiceBounty${window.bountyId}`).data('bs.modal') || {})._isShown ) {
            clearInterval(pollInterval);
        }
        poll();
    }, intervalTime);
}

function checkBountyResponsePaymentComplete(){
    let poll = function() {
        $.post('/check-payment', {
            lnInvoice: window.invoice,
            responseId: window.responseId
        }).done((response) => {
            if (response.message) {
                showPaymentComplete(responseId);
                clearInterval(pollInterval);
            }
        }).fail(() => {
            console.log('failed');
        });
    }
    let timeElapsed = 0;
    let intervalTime = 2000;
    let totalTime = 60000;
    let pollInterval = setInterval(function() {
        timeElapsed += intervalTime;
        // stop polling after maxTime -- based on 1 minute lightning invoice
        if (timeElapsed === totalTime) {
            clearInterval(pollInterval);
        }
        // if user closes modal stop polling
        if ( !($(`#invoice${window.responseId}`).data('bs.modal') || {})._isShown ) {
            clearInterval(pollInterval);
        }
        poll();
    }, intervalTime);
}


function checkPaymentComplete(){
    let poll = function() {
        $.post('/check-payment', {
            lnInvoice: window.invoice
        }).done((response) => {
            if (response.message) {
                showPaymentComplete(window.postCode);
                showPaymentComplete(window.commentCode);
                clearInterval(pollInterval);
            }
        });
    }
    let timeElapsed = 0;
    let intervalTime = 2000;
    let totalTime = 60000;
    let pollInterval = setInterval(function() {
        timeElapsed += intervalTime;
        // stop polling after maxTime -- based on 1 minute lightning invoice
        if (timeElapsed === totalTime) {
            clearInterval(pollInterval);
        }
        // if user closes modal stop polling
        if ( !($(`#invoice${window.postCode}`).data('bs.modal') || {})._isShown ) {
            clearInterval(pollInterval);
        }
        poll();
    }, intervalTime);
}

function checkCommentPaymentComplete(){
    let poll = function() {
        $.post('/check-payment', {
            lnInvoice: window.invoice
        }).done((response) => {
            if (response.message) {
                showPaymentComplete(window.postCode);
                showPaymentComplete(window.commentCode);
                clearInterval(pollInterval);
            }
        });
    }
    let timeElapsed = 0;
    let intervalTime = 2000;
    let totalTime = 60000;
    let pollInterval = setInterval(function() {
        timeElapsed += intervalTime;
        // stop polling after maxTime -- based on 1 minute lightning invoice
        if (timeElapsed === totalTime) {
            clearInterval(pollInterval);
        }
        // if user closes modal stop polling
        if ( !($(`#invoice${window.commentCode}`).data('bs.modal') || {})._isShown ) {
            clearInterval(pollInterval);
        }
        poll();
    }, intervalTime);
}

function updateCardWithPaymentAmount(postCode) {
    let showElement = $(`#lightning-filled${postCode}`);
    let hideElement = $(`#lightning-unfilled${postCode}`);
    showElement.show();
    hideElement.hide();
    let satsAmount = $(`#satsAmount${postCode}`);
    let value = satsAmount.html()
    let valueInt = parseInt(value) + parseInt(window.amount);
    if ( isNaN(valueInt) ){
        valueInt = parseInt(window.amount);
    }
    if (valueInt === 1) {
        satsAmount.html(`${valueInt} Sat`);
    } else {
        satsAmount.html(`${valueInt} Sats`);
    }
}

function incrementCommentCount(postCode) {
    let likeCount = $(`#commentCount${postCode}`);
    let value = likeCount.html()
    value = parseInt(value) + 1;
    likeCount.html(value);
}

function incrementLikeCount(postCode) {
    let likeCount = $(`#likeCount${postCode}`);
    let value = likeCount.html()
    let valueInt = parseInt(value) + 1;
    if ( isNaN(valueInt) ){
        valueInt = 1;
    }
    if (valueInt === 1) {
        likeCount.html(`${valueInt} Like`);
    } else {
        likeCount.html(`${valueInt} Likes`);
    }
}

function decrementLikeCount(postCode) {
    let likeCount = $(`#likeCount${postCode}`);
    let value = likeCount.html()
    let valueInt = parseInt(value) - 1;
    console.log(valueInt);
    if ( isNaN(valueInt) || valueInt === 0){
        likeCount.html("");
    } else if (valueInt === 1) {
        likeCount.html(`${valueInt} Like`);
    } else {
        likeCount.html(`${valueInt} Likes`);
    }
}

function incrementCommentLikeCount(commentCode) {
    let likeCount = $(`#commentLikeCount${commentCode}`);
    let value = likeCount.html()
    let valueInt = parseInt(value) + 1;
    if ( isNaN(valueInt) ){
        valueInt = 1;
    }
    if ( isNaN(valueInt) || valueInt === 0){
        likeCount.html("");
    } else {
        likeCount.html(`${valueInt}`);
    }
}

function decrementCommentLikeCount(commentCode) {
    let likeCount = $(`#commentLikeCount${commentCode}`);
    let value = likeCount.html()
    let valueInt = parseInt(value) - 1;
    console.log(valueInt);
    if ( isNaN(valueInt) || valueInt === 0){
        likeCount.html("");
    } else {
        likeCount.html(`${valueInt}`);
    }
}

function showBountyPaymentComplete(bountyId) {
    let qrDiv;
    let qrComplete;
    let lightningInvoiceText;
    qrDiv = $(`#qrcodeBounty${bountyId}`);
    qrComplete = $(`#qrcodeBountyCompleted${bountyId}`);
    lightningInvoiceText = $(`#lightningInvoiceText${bountyId}`);
    qrDiv.hide();
    qrComplete.show();
    lightningInvoiceText.hide();
    updateCardWithPaymentAmount(bountyId);
}

function showPaymentIncomplete(postCode) {
    let qrDiv;
    let qrComplete;
    let lightningInvoiceText;
    if (postCode !== null) {
        qrDiv = $(`#qrcode${postCode}`);
        qrComplete = $(`#qrcode${postCode}-completed`);
        lightningInvoiceText = $(`#lightningInvoiceText${postCode}`);
    } else {
        qrDiv = $("#qrcode");
        qrComplete = $(`#qrcode-completed`);
        lightningInvoiceText = $(`#lightningInvoiceText`);
    }
    qrDiv.show();
    qrComplete.hide();
    lightningInvoiceText.show();
}

function showPaymentComplete(postCode) {
    let qrDiv;
    let qrComplete;
    let lightningInvoiceText;
    if (postCode !== null) {
        qrDiv = $(`#qrcode${postCode}`);
        qrComplete = $(`#qrcode${postCode}-completed`);
        lightningInvoiceText = $(`#lightningInvoiceText${postCode}`);
    } else {
        qrDiv = $("#qrcode");
        qrComplete = $(`#qrcode-completed`);
        lightningInvoiceText = $(`#lightningInvoiceText`);
    }
    qrDiv.hide();
    qrComplete.show();
    lightningInvoiceText.hide();
    try {
        hideInputSatsErrorBountyResponse(postCode);
    } catch {}
    try {
        hideInputSatsError(postCode);
    } catch {}
    try {
        hideInputSatsErrorBounty();
    } catch {}
    updateCardWithPaymentAmount(postCode);
}

function swapVisibleElements(showElementId, hideElementId) {
    /*
        Makes the `showElementId` visibile and
        `hideElementId invisible
    */
   let showElement = $(`#${showElementId}`);
   let hideElement = $(`#${hideElementId}`);
   showElement.toggle();
   hideElement.toggle();
}

function bookmarkPost(postCode) {
    let showElementId = `bookmarked${postCode}`;
    let hideElementId = `notBookmarked${postCode}`;
    swapVisibleElements(showElementId, hideElementId);
    $.post('/save-bookmark', {
        postCode: postCode,
    }).fail(() => {
        console.log('failed');
        $.toast({
            title: 'Bookmark Failed, Try again later',
            subtitle: null,
            content: null,
            type: 'error',
            pause_on_hover: true,
            delay: 1500
        });
    });
}

/* Toggle Showing/Hiding Functions -- Can probably make concisely one function for all of these */
function toggleThreadZap(id) {
    let boostForm = $(`#thread-boost-form${id}`);
    boostForm.toggle();
}

function toggleZapCommentForm(commentCode) {
    let boostForm = $(`#zapCommentForm${commentCode}`);
    boostForm.toggle();
}

function likePost(postCode) {
    let showElementId = `heart-filled${postCode}`;
    let hideElementId = `heart-unfilled${postCode}`;
    swapVisibleElements(showElementId, hideElementId);
    $.post('/post-like', {
        postCode: postCode,
    }).done((response) => {
        if (response.message === 'liked') {
            incrementLikeCount(postCode);
        }
        if (response.message === 'unliked') {
            decrementLikeCount(postCode);
        }
    }).fail(() => {
        console.log('failed');
        $.toast({
            title: 'Like Failed, Try again later',
            subtitle: null,
            content: null,
            type: 'error',
            pause_on_hover: true,
            delay: 1500
        });
    });
}

function likeComment(commentCode) {
    console.log(commentCode);
    let showElementId = `commentHeart${commentCode}`;
    let hideElementId = `commentNoHeart${commentCode}`;
    swapVisibleElements(showElementId, hideElementId);
    $.post('/comment-like', {
        commentCode: commentCode,
    }).done((response) => {
        console.log(response);
        if (response.message === 'liked') {
            incrementCommentLikeCount(commentCode);
        }
        if (response.message === 'unliked') {
            decrementCommentLikeCount(commentCode);
        }
    });
}

function likeResponse(responseId, userId) {
    let showElementId = `heart-filled${responseId}`;
    let hideElementId = `heart-unfilled${responseId}`;
    swapVisibleElements(showElementId, hideElementId);
    $.post('/response-upvote', {
        responseId: responseId,
        userId: userId
    }).done((response) => {
        if (response.message === 'upvoted') {
            incrementLikeCount(responseId);
        }
        if (response.message === 'deleted') {
            decrementLikeCount(responseId);
        }
    }).fail(() => {
        console.log('failed');
    });
}

function followUserFromPost(postCode, username) {
    let follow = document.getElementById(`follow-${postCode}-${username}`);
    if (follow.innerHTML === 'Follow') {
        follow.innerHTML = 'Following';
    } else {
        follow.innerHTML = 'Follow';
    }
    $(`#modal${postCode}`).modal('hide');
    $.post('/follow', {
        username: username
    }).done((response) => {
        $.toast({
            title: response.message,
            subtitle: null,
            content: null,
            type: 'success',
            pause_on_hover: true,
            delay: 1000
        });
    }).fail(() => {
        console.log('failed');
    });
}

function followUserFromUserList(username) {
    let follow = $(`#followUserButton${username}`);
    if (follow.html() === 'Follow') {
        follow.html('Following');
        follow.css('background-color', '#f39c12')
    } else {
        follow.html('Follow');
        follow.css('background-color', '#2c3e50')
    }
    $.post('/follow', {
        username: username
    }).done((response) => {
        $.toast({
            title: response.message,
            subtitle: null,
            content: null,
            type: 'success',
            pause_on_hover: true,
            delay: 1000
        });
    }).fail(() => {
        console.log('failed');
    });
}

function swapPreview() {
    let bottomText = $("#bottom-text-preview");
    let topText = $("#top-text-preview");
    bottomText.toggle();
    topText.toggle();
}

function copyPubkey(elementId) {
    let copyText = document.getElementById(`${elementId}`);
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    $.toast({
        title: "Node URI Copied",
        position: 'top-left',
        subtitle: null,
        content: null,
        type: 'info',
        pause_on_hover: false,
        delay: 1500
    });
}

function copyLink(elementId, postCode) {
    let copyText = document.getElementById(elementId);
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    $(`#modal${postCode}`).modal('hide');
    $.toast({
        title: "Link Copied",
        position: 'top-left',
        subtitle: null,
        content: null,
        type: 'info',
        pause_on_hover: false,
        delay: 1500
    });
}

function copyBountyLink() {
    let copyText = document.getElementById("copyBountyURL");
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    $(`#shareModal`).modal('hide');
    $.toast({
        title: "Link Copied",
        position: 'top-left',
        subtitle: null,
        content: null,
        type: 'info',
        pause_on_hover: false,
        delay: 1500
    });
}

function copyInvoice(postCode=null) {
    let copyText;
    if (postCode === null) {
        copyText = document.getElementById(`invoiceText`);
    } else {
        copyText = document.getElementById(`invoiceText${postCode}`);
    }
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    $.toast({
        title: "Invoice Copied",
        position: 'top-left',
        subtitle: null,
        content: null,
        type: 'info',
        pause_on_hover: false,
        delay: 1500
    });
}

function copyBountyInvoice(bountyId) {
    let copyText = document.getElementById(`invoiceBountyText${bountyId}`);
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
}

function requestMoreThreads() {
    if (!window.hasOwnProperty('threads_offset')) {
        window.threads_offset = 10;
    }
    console.log('more-threads')
    console.log(window.threads_offset);
    $.post('/more-threads', {
        offset: window.threads_offset
    }).done((response) => {
        window.threads_offset = response.offset;
        $(`#homeContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreComments() {
    if (!window.hasOwnProperty('comments_offset')) {
        window.comments_offset = 10;
    }
    console.log(window.comments_offset);
    $.post('/more-comments', {
        postCode: window.location.pathname.split("/").pop(),
        offset: window.comments_offset
    }).done((response) => {
        window.comments_offset = response.offset;
        $(`#comments`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreImages() {
    if (!window.hasOwnProperty('images_offset')) {
        window.images_offset = 10;
    }
    $.post('/more-images', {
        offset: window.images_offset
    }).done((response) => {
        window.images_offset = response.offset;
        $(`#imagesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreExplore() {
    if (!window.hasOwnProperty('explore_offset')) {
        window.explore_offset = 10;
    }
    let filter = $("#filterSelector").val();
    let scope = $("#scopeSelector").val();
    console.log(window.explore_offset);
    $.post('/more-explore', {
        offset: window.explore_offset,
        filter: filter,
        scope: scope
    }).done((response) => {
        window.explore_offset = response.offset;
        $(`#exploreContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserLikes() {
    if (!window.hasOwnProperty('likes_offset')) {
        window.likes_offset = 10;
    }
    $.post('/more-likes', {
        offset: window.likes_offset
    }).done((response) => {
        window.likes_offset = response.offset;
        $(`#exploreContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserBookmarks() {
    if (!window.hasOwnProperty('bookmarks_offset')) {
        window.bookmarks_offset = 10;
    }
    $.post('/more-bookmarks', {
        offset: window.bookmarks_offset
    }).done((response) => {
        window.bookmarks_offset = response.offset;
        $(`#exploreContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserZaps() {
    if (!window.hasOwnProperty('zaps_offset')) {
        window.zaps_offset = 10;
    }
    $.post('/more-zaps', {
        offset: window.zaps_offset
    }).done((response) => {
        window.zaps_offset = response.offset;
        $(`#exploreContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreOpenBounties() {
    if (!window.hasOwnProperty('open_bounties_offset')) {
        window.open_bounties_offset = 10;
    }
    $.post('/more-open-bounties', {
        offset: window.open_bounties_offset
    }).done((response) => {
        window.open_bounties_offset = response.offset;
        $(`#bountiesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreReceivedPayments() {
    if (!window.hasOwnProperty('received_payments_offset')) {
        window.received_payments_offset = 10;
    }
    $.post('/more-received-payments', {
        offset: window.received_payments_offset
    }).done((response) => {
        window.received_payments_offset = response.offset;
        $(`#paymentsContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreSentPayments() {
    if (!window.hasOwnProperty('sent_payments_offset')) {
        window.sent_payments_offset = 10;
    }
    $.post('/more-sent-payments', {
        offset: window.sent_payments_offset
    }).done((response) => {
        window.sent_payments_offset = response.offset;
        $(`#paymentsContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreClosedBounties() {
    if (!window.hasOwnProperty('closed_bounties_offset')) {
        window.closed_bounties_offset = 10;
    }
    $.post('/more-closed-bounties', {
        offset: window.closed_bounties_offset
    }).done((response) => {
        window.closed_bounties_offset = response.offset;
        $(`#bountiesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreAllBounties() {
    if (!window.hasOwnProperty('all_bounties_offset')) {
        window.all_bounties_offset = 10;
    }
    $.post('/more-all-bounties', {
        offset: window.all_bounties_offset
    }).done((response) => {
        window.all_bounties_offset = response.offset;
        $(`#bountiesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserBounties() {
    if (!window.hasOwnProperty('user_bounties_offset')) {
        window.user_bounties_offset = 10;
    }
    $.post('/more-user-bounties', {
        offset: window.user_bounties_offset
    }).done((response) => {
        window.user_bounties_offset = response.offset;
        $(`#bountiesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserRespondedBounties() {
    if (!window.hasOwnProperty('user_responsed_bounties_offset')) {
        window.user_responsed_bounties_offset = 10;
    }
    $.post('/more-user-responded-bounties', {
        offset: window.user_responsed_bounties_offset
    }).done((response) => {
        window.user_responsed_bounties_offset = response.offset;
        $(`#bountiesContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserProfilePosts() {
    if (!window.hasOwnProperty('profile_posts_offset')) {
        window.profile_posts_offset = 10;
    }
    $.post('/more-user-profile-posts', {
        offset: window.profile_posts_offset,
        username: window.location.pathname.split('/').splice(-1)[0]
    }).done((response) => {
        window.profile_posts_offset = response.offset;
        $(`#userPostsContainer`).append(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function requestMoreUserProfilePosts() {
    if (!window.hasOwnProperty('notifications_offset')) {
        window.notifications_offset = 10;
    }
    $.post('/more-notifications', {
        offset: window.notifications_offset,
    }).done((response) => {
        window.notifications_offset = response.offset;
        $(`#notificationsContainer`).append(response.html);
    });
}


function awardBountyPrompt(divId) {
    $(`#${divId}`).toggle();
}

function awardBountySubmit(responseId) {
    $("#bountyResponseWinner").val(responseId);
    $("#bountyResponseWinnerForm").submit();
}

function forceLower(strInput) {
    strInput.value = strInput.value.toLowerCase();
}

function showGifSearch(postCode="") {
    $(`#gifSearchDiv${postCode}`).toggle();
    $(`#tenorGifResponse${postCode}`).toggle();
}

function getGifs(postCode="") {
    let searchTerm = $(`#gifSearch${postCode}`);
    console.log(postCode);
    console.log(searchTerm.val());
    $.post('/gif-search', {
        searchTerm: searchTerm.val(),
        postCode: postCode
    }).done((response) => {
        let gifPreview = $(`#tenorGifResponse${postCode}`)
        gifPreview.html(response.html);
    }).fail(() => {
        console.log('failed');
    });
}

function showGifInPreview(img, postCode="") {
    let src = img.src;
    let preview = $(`#load-img${postCode}`);
    $(`#gif${postCode}`).val(src);
    preview.attr("src", src);
    preview.show();
}
