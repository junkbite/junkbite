from flask import Blueprint

bp = Blueprint('advertise', __name__, url_prefix='/advertise')

from app.advertise import routes
