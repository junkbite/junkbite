from app.auth.utils import admin_login_required
from app.utils.meta_utils import (
    _datetime_to_humantime,
    _process_request_file,
    _sqliterow_to_dict,
)
from werkzeug.exceptions import abort
from app.utils.db_utils import (
    _get_anon_user_id,
    _get_current_user_id,
    _create_advertisement,
    _update_advertisement,
    _get_max_withdrawal_amount
)
from flask import (
    current_app,
    g,
    flash,
    request,
    redirect,
    render_template,
    url_for,
)
from app.advertise import bp


@bp.route('/buy_sticky_post', methods=('GET', 'POST'))
def buy_sticky_post():
    """
    Create an advertisement post by paying an invoice
    """
    if request.method == 'POST':
        price = float(request.form.get('purchase-price')) * 100  # base unit penny
        # TODO, confirm payment here
        hours_purchased = price * 4 / 100 # .25 cents per hour
        ad_id = _create_advertisement(price, hours_purchased)
        flash(f'Sticky post purchased for {hours_purchased} hours')
        return redirect(url_for('advertise.create_sticky_post', ad_id=ad_id))
    max_withdrawal_amount = __get_max_withdrawal_amount()
    return render_template(
        'advertise/buy_sticky_post.html',
        max_zap=max_withdrawal_amount
    )
