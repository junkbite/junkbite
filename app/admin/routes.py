from flask import (
    redirect,
    render_template,
    request,
    url_for,
    flash,
    current_app,
    g
)
from app.auth.utils import admin_login_required
from app.admin import bp
from app.models import (
    Comment,
    Feedback,
    Post,
    ReportedContent,
    SupportTicket,
    User,
    AdminView,
    UserBalance,
    UserBalanceEntry,
    Blog,
    Withdrawal,
    Payment,
    Notification,
    JunkbiteNode,
    Bounty,
    BountyResponse,
    LightningAppConfiguration,
    EmailHistory,
    PaymentProcessingFee,
    Settings,
    FAQEntry,
    FollowRequest,
    Followers,
    UserNode,
    PostNode,
    PostLike,
    CommentLike,
    BountyResponseLike,
    Bookmark,
    OnionUrl,
    Advertisement,
    AdvertisementStats,
    Notification,
)
from app import db
from lndgrpc import LNDClient
from app.utils.db_utils import (
    _get_user_balance,
    _save_withdrawal,
    _junkbite_daily_stats,
)
from app.email import sendgrid_send_email


@bp.route('/<model>/<instance_id>', methods=("GET",))
@admin_login_required
def model_detail_view(model, instance_id):
    """
    Render template for any model to view it
    """
    model = model.lower()
    mapping = {
        'user': User,
        'post': Post,
        'comment': Comment,
        'userbalance': UserBalance,
        'userbalanceentry': UserBalanceEntry,
        'followrequest': FollowRequest,
        'notification': Notification,
        'bounty': Bounty,
        'bountyresponse': BountyResponse,
        'withdrawal': Withdrawal,
        'reportedcontent': ReportedContent,
        'followers': Followers,
        'usernode': UserNode,
        'postnode': PostNode,
        'payment': Payment,
        'postlike': PostLike,
        'bountyresponselike': BountyResponseLike,
        'bookmark': Bookmark,
        'commentlike': CommentLike,
        'blog': Blog,
        'onionurl': OnionUrl,
        'advertisement': Advertisement,
        'advertisementstats': AdvertisementStats,
        'junkbitenode': JunkbiteNode,
        'settings': Settings,
        'paymentprocessingfee': PaymentProcessingFee
    }
    mapped_model = mapping.get(model)
    instance = mapped_model.query.get(instance_id)
    if instance:
        return render_template(
            'admin/_admin_generic_detail.html',
            model_name=model,
            model=mapped_model,
            instance=instance
        )
    flash("Instance not found")
    return redirect(url_for('admin.admin'))


@bp.route('/', methods=("GET",))
@bp.route('/admin', methods=("GET",))
@admin_login_required
def admin():
    user_count = User.query.count()
    post_count = Post.query.count()
    comment_count = Comment.query.count()
    bounty_count = Bounty.query.count()
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    info = lnd.get_info()
    balance = lnd.wallet_balance()
    total_balance = balance.total_balance
    confirmed_balance = balance.confirmed_balance
    pubkey = info.identity_pubkey
    alias = info.alias
    active_channels = info.num_active_channels
    peers = info.num_peers
    block_height = info.block_height
    version = info.version
    return render_template(
        'admin/index.html',
        user_count=user_count,
        post_count=post_count,
        comment_count=comment_count,
        bounty_count=bounty_count,
        pubkey=pubkey,
        alias=alias,
        active_channels=active_channels,
        peers=peers,
        block_height=block_height,
        version=version,
        total_balance=total_balance,
        confirmed_balance=confirmed_balance
    )


@bp.route('/daily-stats', methods=("GET",))
@admin_login_required
def admin_daily_stats():
    stats = _junkbite_daily_stats()
    new_users_today, active_users_today, payments, jb_payments = stats
    return render_template(
        'admin/daily_stats.html',
        new_users_today=new_users_today,
        active_users_today=active_users_today,
        payments=payments,
        jb_payments=jb_payments
    )


@bp.route('/payment-fees', methods=("GET", "POST"))
@admin_login_required
def admin_payment_fees():
    fees = PaymentProcessingFee.query.order_by(PaymentProcessingFee.id).all()
    m = AdminView.query.filter_by(name="PaymentProcessingFee").first()
    return render_template('admin/fees.html', m=m, query=fees)


@bp.route('/deposit', methods=("GET", "POST"))
@admin_login_required
def admin_deposit():
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    addr = lnd.new_address()
    deposit_address = addr.address
    return render_template('admin/deposit.html', deposit_address=deposit_address)


@bp.route('/send-email', methods=("POST",))
@admin_login_required
def admin_send_email():
    username = request.form.get("username")
    subject = request.form.get("subject")
    body = request.form.get("body")
    password = request.form.get("password")
    if not g.user.check_password(password):
        flash("Authentication Failed")
        return redirect(url_for("admin.admin"))
    # get user and their email
    user = User.query.filter_by(username=username).first()
    if not user:
        flash("User not found")
        return redirect(url_for('admin.admin'))
    if not user.email:
        flash("User does not have an email address with junkbite")
        return redirect(url_for('admin.admin'))
    if not subject:
        flash("Subject Required")
        return redirect(url_for('admin.admin'))
    if not body:
        flash("Body Required")
        return redirect(url_for('admin.admin'))
    sendgrid_send_email(
        subject=subject,
        sender=current_app.config['ADMINS'][0],
        recipients=user.email,
        text_body=body,
        html_body=body
    )
    flash("Email Successfully sent")
    return redirect(url_for("admin.admin"))


@bp.route('/send-payment', methods=("GET", "POST",))
@admin_login_required
def admin_send_payment():
    """
    Give admins the ability to manually complete lightning invoices
    - require password on entry?
    - force username entry and check balance before completing
    """
    if request.method == "POST":
        username = request.form.get("username")
        ticket_code = request.form.get("supportTicket")
        invoice = request.form.get("invoice")
        user = User.query.filter_by(username=username).first()
        if not user:
            flash("User not found")
            return redirect(url_for('admin.admin_send_payment'))
        ticket = SupportTicket.query.filter_by(ticket_code=ticket_code).first()
        if not ticket:
            flash("Support Ticket not found")
            return redirect(url_for('admin.admin_send_payment'))
        if ticket.user_id != user.id:
            flash("Support Ticket and User mismatch")
            return redirect(url_for('admin.admin_send_payment'))
        # good to go, check user balance vs invoice amount. if succeed send
        # then close ticket and update user balance
        lnd = LNDClient(
            ip_address=current_app.config["LND_RPC_IP"],
            macaroon_filepath=current_app.config['LND_MACAROON'],
            cert_filepath=current_app.config['LND_TLS_CERT'],
            admin=True
        )
        info = lnd.decode_payment_request(invoice)
        node_uri = info.destination
        amount = info.num_satoshis
        # check balance here and add withdrawal attempt
        w = _save_withdrawal(user.id, invoice, node_uri, amount)
        balance = _get_user_balance(user.id)
        if balance - amount >= 0:
            try:
                lnd.send_payment(invoice)
                w.status = "completed"
                ticket.ticket_status = "closed"
                db.session.add(w)
                # possibly add a ticket response here showing it was paid manually
                db.session.add(ticket)
                db.session.commit()
                flash("Payment successfully sent. Withdrawal Record added to user")
            except Exception as e:
                flash("Payment failed. Due to an exception")
                current_app.logger.info(e)
                w.status = "failed"
                db.session.add(w)
                db.session.commit()
        else:
            flash("User balance does not support this payout")
        return redirect(url_for('admin.admin_send_payment'))
    return render_template('admin/send_payment.html')



@bp.route('/admin-view', methods=("GET", "POST"))
@admin_login_required
def admin_view():
    if request.method == "POST":
        model_name = request.form.get("modelName")
        route = request.form.get("flaskRoute")
        function = request.form.get("flaskFunction")
        field_names = request.form.get("fieldNames")
        field_headers = request.form.get("fieldHeaders")
        # save new model
        model = AdminView(
            name=model_name,
            route=route,
            function=function,
            field_names=field_names,
            field_headers=field_headers
        )
        db.session.add(model)
        db.session.commit()
    models = AdminView.query.all()
    return render_template('admin/admin_view.html', models=models)


@bp.route('/admin-view/<app_id>/edit', methods=("GET", "POST"))
@admin_login_required
def admin_view_edit(app_id):
    model = AdminView.query.get(app_id)
    if request.method == "POST":
        model_name = request.form.get("modelName")
        route = request.form.get("flaskRoute")
        function = request.form.get("flaskFunction")
        field_names = request.form.get("fieldNames")
        field_headers = request.form.get("fieldHeaders")
        if model_name:
            model.name = model_name
        if route:
            model.route = route
        if function:
            model.function = function
        if field_names:
            model.field_names = field_names
        if field_headers:
            model.field_headers = field_headers
        db.session.add(model)
        db.session.commit()
        flash('Updated')
        return redirect(url_for('admin.admin_view_edit', app_id=model.id))
    return render_template('admin/admin_view_edit.html', model=model)



@bp.route('/lightning-app-config', methods=("GET", "POST"))
@admin_login_required
def admin_lightning_app_config():
    """
    List Lightning App Configurations
    """
    apps = LightningAppConfiguration.query.all()
    return render_template("admin/lightning_config.html", apps=apps)


@bp.route('/lightning-app-config/<int:app_id>/edit', methods=("GET", "POST"))
@admin_login_required
def admin_edit_lightning_app_config(app_id):
    """
    Edit configuration of a given lightning application
    """
    app = LightningAppConfiguration.query.get(app_id)
    if request.method == "POST":
        utility_field = request.form.get("utilityField")
        price = request.form.get("price")
        name = request.form.get("name")
        if name:
            app.application_name = name
        if utility_field:
            app.utility_field = utility_field
        if price:
            app.price_for_function = price
        db.session.add(app)
        db.session.commit()
        flash("Configuration Successfully Updated")
        return redirect(url_for('admin.admin_edit_lightning_app_config', app_id=app.id))
    return render_template("admin/lightning_config_edit.html", app=app)



@bp.route('/support', methods=("GET", "POST",))
@admin_login_required
def admin_support():
    if request.method == "POST":
        search = request.form.get("search")
        return redirect(url_for("admin.admin_support", s=search))
    search = request.args.get("s")
    if search:
        tickets = SupportTicket.query.filter(
            (SupportTicket.description.ilike(f"%{search}%")) |
            (SupportTicket.ticket_code.ilike(f"%{search}%"))
        )
    else:
        tickets = SupportTicket.query.all()
    m = AdminView.query.filter_by(name="SupportTicket").first()
    return render_template('admin/support.html', m=m, query=tickets)


@bp.route('/support/<ticket_code>/close', methods=("GET", "POST",))
@admin_login_required
def admin_close_support_ticket(ticket_code):
    """
    Close a support ticket
    """
    ticket = SupportTicket.query.filter_by(ticket_code=ticket_code).first()
    if ticket:
        ticket.ticket_status = "closed"
        db.session.add(ticket)
        db.session.commit()
        flash("Ticket Closed")
        return redirect(url_for('admin.admin_support'))
    flash("Ticket not found")
    return redirect(url_for('admin.admin_support'))


@bp.route('/feedback', methods=("GET", "POST",))
@admin_login_required
def admin_feedback():
    if request.method == "POST":
        search = request.form.get("search")
        return redirect(url_for("admin.admin_feedback", s=search))
    search = request.args.get("s")
    if search:
        feedback = Feedback.query.filter(
            (Feedback.description.ilike(f"%{search}%")) |
            (Feedback.ticket_code.ilike(f"%{search}%"))
        )
    else:
        feedback = Feedback.query.all()
    m = AdminView.query.filter_by(name="Feedback").first()
    return render_template('admin/feedback.html', m=m, query=feedback)


@bp.route('/users', methods=("GET", "POST",))
@admin_login_required
def admin_users():
    if request.method == "POST":
        username = request.form.get("search")
        return redirect(url_for("admin.admin_users", u=username))
    username = request.args.get("u")
    if username:
        users = User.query.filter(
            (User.username.ilike(f"%{username}%")) | (User.email.ilike(f"%{username}%"))
        )
    else:
        users = User.query.order_by(User.id.desc()).all()
    m = AdminView.query.filter_by(name="User").first()
    return render_template('admin/users.html', m=m, query=users)


@bp.route('/bounties', methods=("GET", "POST",))
@admin_login_required
def admin_bounties():
    title = request.args.get("t")
    if title:
        bounties = Bounty.query.filter(Bounty.title.ilike(f"%{title}%"))
    else:
        bounties = Bounty.query.order_by(Bounty.id.desc()).all()
    m = AdminView.query.filter_by(name="Bounty").first()
    return render_template('admin/bounties.html', m=m, query=bounties)


@bp.route('/emails', methods=("GET", "POST",))
@admin_login_required
def admin_emails():
    title = request.args.get("t")
    if title:
        histories = EmailHistory.query.filter(EmailHistory.subject.ilike(f"%{title}%"))
    else:
        histories = EmailHistory.query.order_by(EmailHistory.id.desc()).all()
    m = AdminView.query.filter_by(name="EmailHistory").first()
    return render_template('admin/emails.html', m=m, query=histories)


@bp.route('/bounty-responses', methods=("GET", "POST",))
@admin_login_required
def admin_bounty_responses():
    title = request.args.get("t")
    if title:
        responses = BountyResponse.query.filter(BountyResponse.title.ilike(f"%{title}%"))
    else:
        responses = BountyResponse.query.order_by(BountyResponse.id.desc()).all()
    m = AdminView.query.filter_by(name="BountyResponse").first()
    return render_template('admin/bounties.html', m=m, query=responses)


@bp.route('/payments', methods=("GET", "POST",))
@admin_login_required
def admin_payments():
    # To Do - Paginate
    payments = Payment.query.order_by(Payment.id.desc()).limit(200)
    m = AdminView.query.filter_by(name="Payment").first()
    return render_template('admin/bounties.html', m=m, query=payments)


@bp.route('/withdrawal', methods=("GET", "POST",))
@admin_login_required
def admin_withdrawals():
    withdawals = Withdrawal.query.order_by(Withdrawal.id.desc()).all()
    m = AdminView.query.filter_by(name="Withdrawal").first()
    return render_template('admin/bounties.html', m=m, query=withdawals)


@bp.route('/notifications', methods=("GET", "POST",))
@admin_login_required
def admin_notifications():
    notifications = Notification.query.order_by(Notification.id.desc()).all()
    m = AdminView.query.filter_by(name="Notification").first()
    return render_template('admin/bounties.html', m=m, query=notifications)


@bp.route('/user-balances', methods=("GET", "POST",))
@admin_login_required
def admin_user_balances():
    user_balances = UserBalance.query.order_by(UserBalance.id.desc()).all()
    m = AdminView.query.filter_by(name="UserBalance").first()
    return render_template('admin/bounties.html', m=m, query=user_balances)


@bp.route('/user-balance-entries', methods=("GET", "POST",))
@admin_login_required
def admin_user_balance_entries():
    user_balance_entries = UserBalanceEntry.query.order_by(UserBalanceEntry.id.desc()).all()
    m = AdminView.query.filter_by(name="UserBalanceEntry").first()
    return render_template('admin/bounties.html', m=m, query=user_balance_entries)


@bp.route('/settings', methods=("GET", "POST"))
@admin_login_required
def admin_settings():
    settings = Settings.query.get(1)
    if request.method == "POST":
        payment_percentage = request.form.get("payment_percentage")
        withdrawals_frozen = request.form.get("withdrawals_frozen")
        withdrawals_frozen_msg = request.form.get("withdrawals_frozen_message")
        maximum_withdrawal_amount = request.form.get("maximum_withdrawal_amount")
        withdrawal_period = request.form.get("withdrawal_period")
        settings.payment_percentage = payment_percentage
        settings.withdrawals_frozen = True if withdrawals_frozen in ["True", "true", "t", "T", "TRUE"] else False
        settings.withdrawals_frozen_message = withdrawals_frozen_msg
        settings.maximum_withdrawal_amount = maximum_withdrawal_amount
        settings.withdrawal_period = withdrawal_period
        db.session.add(settings)
        db.session.commit()
    return render_template('admin/settings.html', settings=settings)


@bp.route('/faqs', methods=("GET", "POST"))
@admin_login_required
def admin_faqs():
    faqs = FAQEntry.query.all()
    if request.method == "POST":
        question = request.form.get("question")
        answer = request.form.get("answer")
        visible = request.form.get("visible")
        faq = FAQEntry(
            question=question,
            answer=answer,
            visible=True if visible in ["True", "true", "t", "T", "TRUE"] else False
        )
        db.session.add(faq)
        db.session.commit()
    return render_template('admin/faqs.html', faqs=faqs)


@bp.route('/new-faq', methods=("GET", "POST"))
@admin_login_required
def admin_new_faq():
    faq = FAQEntry.query.get(1)
    if request.method == "POST":
        question = request.form.get("question")
        answer = request.form.get("answer")
        visible = request.form.get("visible")
        faq = FAQEntry(
            question=question,
            answer=answer,
            visible=True if visible in ["True", "true", "t", "T", "TRUE"] else False
        )
        db.session.add(faq)
        db.session.commit()
    return render_template('admin/new_faq.html', faq=faq)


@bp.route('/blogs', methods=("GET", "POST",))
@admin_login_required
def admin_blogs():
    blogs = Blog.query.order_by(Blog.id.desc()).all()
    m = AdminView.query.filter_by(name="Blog").first()
    return render_template('admin/bounties.html', m=m, query=blogs)


@bp.route('/posts', methods=("GET", "POST"))
@admin_login_required
def admin_posts():
    if request.method == "POST":
        title = request.form.get("title")
        return redirect(url_for("admin.admin_posts", t=title))
    # TODO paginate these
    title = request.args.get("t")
    if title:
        posts = Post.query.filter(Post.title.ilike(f"%{title}%"))
    else:
        posts = Post.query.order_by(Post.id.desc()).all()
    m = AdminView.query.filter_by(name="Post").first()
    return render_template('admin/posts.html', m=m, query=posts)


@bp.route('/comments', methods=("GET", "POST"))
@admin_login_required
def admin_comments():
    if request.method == "POST":
        comment = request.form.get("comment")
        return redirect(url_for("admin.admin_comments", c=comment))
    comment = request.args.get("c")
    if comment:
        comments = Comment.query.filter(Comment.body.ilike(f"%{comment}%"))
    else:
        comments = Comment.query.order_by(Comment.id.desc()).all()
    m = AdminView.query.filter_by(name="Comment").first()
    return render_template('admin/comments.html', m=m, query=comments)


@bp.route('/reported-content', methods=("GET",))
@admin_login_required
def admin_reported_content():
    reported_posts = ReportedContent.query.all()
    m = AdminView.query.filter_by(name="ReportedContent").first()
    return render_template(
        'admin/reported_content.html',
        m=m,
        query=reported_posts
    )


@bp.route('/reported-content/<int:report_id>/review', methods=("GET",))
@admin_login_required
def admin_reported_content_review(report_id):
    """
    Allows an admin to take an action on reported content
    """
    reported = ReportedContent.query.get(report_id)
    user = User.query.get(reported.reported_user_id)
    if reported.post_id:
        post = reported.post
        return render_template(
            'admin/review.html',
            reported=reported,
            post=post,
            user=user
        )
    else:
        comment = Comment.query.get(reported.comment_id)
        return render_template(
            'admin/review.html',
            reported=reported,
            post=comment,
            user=user
        )


@bp.route('/reported-content/<int:report_id>/judgement', methods=('POST',))
@admin_login_required
def admin_reported_content_judgement(report_id):
    """
    Pass judgement on reported content after reviewing it

    tables potentially involved:
        - user (warning, mute, ban, permaban)
        - post/comment (replace all content with a message "POST WAS BANNED,
          USER WAS BANNED etc")
        - reported_content (update table with written text of the action taken)
    """
    reported = ReportedContent.query.get(report_id)
    user = User.query.get(reported.reported_user_id)
    msg = ""
    try:
        user_action = request.form['user-action']
        user_action = True
    except Exception:
        user_action = False
    try:
        delete_content = request.form['delete-content']
        delete_content = True
    except Exception:
        delete_content = False
    action_taken = request.form['reason']
    if user_action:
        # increment punishment level by one
        user.punished_level += 1
        db.session.add(user)
        msg += "Increase user's punishment level"
    if delete_content:
        # delete content and replace with ban info
        title = "ADMIN ACTION TAKEN"
        body = "This post's content has been removed."
        upload = "<deleted>"
        unique_filename = "<deleted>"
        post = Post.query.get(reported.post_id)
        post.title = title
        post.body = body
        post.upload = upload
        post.unique_filename = unique_filename
        post.processed_body = body
        db.session.add(post)
        msg += "| Deleted content"
    reported.action_taken = action_taken
    db.session.add(reported)
    db.session.commit()
    if msg:
        flash(msg)
    return redirect(url_for('admin.admin'))
