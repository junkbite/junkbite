from flask import Blueprint

bp = Blueprint('admin', __name__, url_prefix='/administration')

from app.admin import routes
