from slugify import slugify
from app import db
from app.auth.utils import admin_login_required
from app.utils.meta_utils import (
    _datetime_to_humantime,
    _process_request_file,
    _sqliterow_to_dict,
)
from werkzeug.exceptions import abort
from flask import (
    current_app,
    g,
    flash,
    request,
    redirect,
    render_template,
    url_for,
)
from app.blog import bp
from app.utils.db_utils import (
    _get_blog,
    _delete_blog,
    _get_blogs_for_view,
    _save_blog,
)


BLOG_ROUTE = 'blog.blog'


@bp.route('/create_blog', methods=('GET', 'POST'))
@admin_login_required
def create_blog():
    """
    Creates a new blog entry. - image title textbox.
    """
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        if not title:
            flash('Title is required')
            return redirect(url_for('blog.create_blog'))
        if not body:
            flash('Body is required')
            return redirect(url_for('blog.create_blog'))
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('blog.create_blog'))
        processed_body = current_app.markdown(body)
        slug = _save_blog(
            title,
            body,
            processed_body,
            g.user.id,
            0,
            upload,
            unique_filename
        )
        return redirect(url_for(BLOG_ROUTE, slug=slug))
    return render_template('blog/create.html')


@bp.route('/blogs/<slug>/edit', methods=('GET', 'POST'))
@admin_login_required
def edit_blog(slug):
    """
    Edit an an existing blog entry. - image title textbox.
    """
    if request.method == 'POST':
        title = request.form.get('title')
        body = request.form.get('body')
        if not title:
            flash('Title is required')
            return redirect(url_for('blog.edit_blog', slug=slug))
        if not body:
            flash('Body is required')
            return redirect(url_for('blog.edit_blog', slug=slug))
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('blog.edit_blog', slug=slug))
        processed_body = current_app.markdown(body)
        blog = _get_blog(slug)
        blog.title = title
        blog.slug = slugify(title)
        blog.body = body
        blog.processed_body = processed_body
        blog.upload = upload
        blog.unique_filename
        db.session.add(blog)
        db.session.commit()
        return redirect(url_for(BLOG_ROUTE, slug=blog.slug))
    blog = _get_blog(slug)
    return render_template('blog/edit.html', blog=blog)


@bp.route('/blogs')
def blogs():
    blogs = _get_blogs_for_view()
    return render_template('blog/blogs.html', posts=blogs)


@bp.route('/blogs/<slug>', methods=('GET', 'POST'))
def blog(slug):
    """
    Loads a blog post based on id -- blog posts are created by admin
    in create_blog
    """
    blog = _get_blog(slug)
    if blog is None:
        flash("Blog not found")
        return redirect(url_for('blog.blogs'))
    return render_template(
        'blog/blog.html',
        post=blog,
    )


@bp.route('/blogs/delete-blog/<int:blog_id>', methods=('POST',))
@admin_login_required
def delete_blog(blog_id):
    _delete_blog(blog_id)
    return redirect(url_for('blog.blogs'))
