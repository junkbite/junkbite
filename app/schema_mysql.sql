CREATE TABLE if not exists user (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) UNIQUE NOT NULL,
    password TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ip_address TEXT,
    last_login TEXT,
    admin INTEGER NOT NULL DEFAULT 0,
    admin_actions_taken INTEGER NOT NULL DEFAULT 0,
    punished_level INTEGER NOT NULL DEFAULT 0,
    email VARCHAR(255) UNIQUE,
    unique_filename TEXT,
    link VARCHAR(255),
    bio TEXT,
    token TEXT,
    token_expiration TEXT
);

CREATE TABLE if not exists followers (
    follower_id INTEGER NOT NULL,
    followee_id INTEGER NOT NULL,
    FOREIGN KEY (follower_id) REFERENCES user (id),
    FOREIGN KEY (followee_id) REFERENCES user (id)
);

CREATE TABLE if not exists user_node (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    node_uri TEXT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists post (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    author_id INTEGER NOT NULL,
    sticky INTEGER NOT NULL DEFAULT 0,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title TEXT NOT NULL,
    body TEXT NOT NULL,
    processed_body TEXT NOT NULL,
    upload TEXT,
    unique_filename TEXT,
    nsfw INTEGER NOT NULL,
    meme INTEGER NOT NULL,
    board TEXT,
    FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE if not exists post_node (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    post_id INTEGER NOT NULL,
    node_uri TEXT NOT NULL,
    FOREIGN KEY (post_id) REFERENCES post (id)
);

CREATE TABLE if not exists withdrawal (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    initiated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    amount INTEGER NOT NULL,
    ln_invoice VARCHAR(300) UNIQUE NOT NULL,
    node_uri TEXT NOT NULL,
    status TEXT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists user_balance (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    balance INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists user_balance_entries (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_balance_id INTEGER NOT NULL,
    before_balance INTEGER NOT NULL,
    action_type TEXT NOT NULL,
    ln_invoice TEXT NOT NULL,
    amount INTEGER NOT NULL,
    updating_user_id INTEGER NOT NULL,
    entry_timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_balance_id) REFERENCES user_balance (id)
);

CREATE TABLE if not exists payment (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    post_id INTEGER,
    bounty_id INTEGER,
    bounty_response_id INTEGER,
    receiving_user_id INTEGER,
    junkbite_app TEXT,
    amount INTEGER NOT NULL,
    invoice_id INTEGER NOT NULL,
    sending_user_id INTEGER,
    payment_type TEXT NOT NULL,
    status VARCHAR(20) NOT NULL DEFAULT 'incomplete',
    FOREIGN KEY (post_id) REFERENCES post (id)
);

CREATE TABLE if not exists bounty (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    status VARCHAR(20) NOT NULL DEFAULT "open",
    winner_response_id INTEGER,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title TEXT NOT NULL,
    body TEXT NOT NULL,
    processed_body TEXT NOT NULL,
    upload TEXT,
    unique_filename TEXT,
    initial_sats INTEGER NOT NULL,
    initial_ln_invoice TEXT NOT NULL,
    total_sats INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE if not exists bounty_response (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    bounty_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    body TEXT NOT NULL,
    processed_body TEXT NOT NULL,
    upload TEXT,
    unique_filename TEXT,
    upvotes INTEGER NOT NULL DEFAULT 0,
    bounty_winner INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (bounty_id) REFERENCES bounty (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
);
ALTER TABLE bounty ADD FOREIGN KEY (winner_response_id) REFERENCES bounty_response (id) ON DELETE CASCADE;

CREATE TABLE if not exists bounty_response_upvote (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    bounty_response_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (bounty_response_id) REFERENCES bounty_response (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists junkbite_node (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    alias TEXT,
    pubkey TEXT NOT NULL,
    uri TEXT NOT NULL
);


CREATE TABLE if not exists post_like (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    post_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists bookmark (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    user_id INTEGER NOT NULL,
    post_id INTEGER,
    bounty_id INTEGER,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE,
    FOREIGN KEY (bounty_id) REFERENCES bounty (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists comment (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    author_id INTEGER NOT NULL,
    post_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    body TEXT NOT NULL,
    processed_body TEXT NOT NULL,
    upload TEXT,
    unique_filename TEXT,
    FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE CASCADE,
    FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE
);

CREATE TABLE if not exists comment_like (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    comment_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE if not exists reported_content (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    post_id INTEGER NOT NULL,
    comment_id INTEGER NOT NULL DEFAULT 0,
    post_or_comment TEXT NOT NULL,
    reported_id INTEGER NOT NULL,
    reporter_id INTEGER NOT NULL,
    reason TEXT NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    count INTEGER NOT NULL DEFAULT 1,
    action_taken TEXT,
    FOREIGN KEY (post_id) REFERENCES post (id),
    FOREIGN KEY (comment_id) REFERENCES comment (id),
    FOREIGN KEY (reporter_id) REFERENCES user (id),
    FOREIGN KEY (reported_id) REFERENCES user (id)
);

CREATE TABLE if not exists blog (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    author_id INTEGER NOT NULL,
    sticky INTEGER NOT NULL DEFAULT 0,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title TEXT NOT NULL,
    body TEXT NOT NULL,
    processed_body TEXT NOT NULL,
    upload TEXT,
    unique_filename TEXT,
    nsfw INTEGER NOT NULL,
    board TEXT,
    FOREIGN KEY (author_id) REFERENCES user (id) ON DELETE CASCADE
);

CREATE TABLE if not exists onion_url (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    onion TEXT NOT NULL
);

CREATE TABLE if not exists advertisement (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    post_id INTEGER,
    price INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    hours_purchased INTEGER NOT NULL,
    expired INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (post_id) REFERENCES post (id)
);

CREATE TABLE if not exists advertisement_stats (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    advertisement_id INTEGER NOT NULL,
    clicks INTEGER NOT NULL DEFAULT 0,
    FOREIGN KEY (advertisement_id) REFERENCES advertisement (id)
);

CREATE TABLE if not exists notification (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    to_user_id INTEGER NOT NULL,
    from_user_id INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    opened INTEGER NOT NULL DEFAULT 0,
    content TEXT NOT NULL,
    post_id INTEGER,
    bounty_id INTEGER,
    bounty_response_id INTEGER,
    comment_id INTEGER,
    FOREIGN KEY (to_user_id) REFERENCES user (id),
    FOREIGN KEY (from_user_id) REFERENCES user (id)
);
