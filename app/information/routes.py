from app.information import bp
from flask import (
    render_template,
    send_from_directory,
    current_app,
    request,
    redirect,
    flash,
    url_for,
    g
)
from app.utils.db_utils import (
    _get_junkbite_node,
    _save_feedback,
    _save_support_ticket
)
from app.models import (
    SupportTicket,
    SupportTicketResponse,
    FAQEntry
)
from app import db
from app.email import (
    sendgrid_new_support_ticket
)

@bp.route('/info')
@bp.route('/information')
def info():
    return render_template('information/information.html')


@bp.route('/about')
def about():
    return render_template('information/about.html')


@bp.route('/help')
def help():
    return render_template('information/help.html')


@bp.route('/rules')
def rules():
    return render_template('information/rules.html')


@bp.route('/privacy-policy')
def privacy_policy():
    return render_template('information/privacy_policy.html')


@bp.route('/terms-of-service')
def terms_of_service():
    return render_template('information/terms_of_service.html')



@bp.route('/bitcoin.pdf')
def bitcoin_pdf():
    """
    Returns the sacred text
    """
    return send_from_directory(current_app.config['STATIC_FOLDER'], 'bitcoin.pdf')


@bp.route('/node-information')
def lightning_info():
    """
    Returns a page describing how to connect to junkbite lightning node
    """
    node = _get_junkbite_node()
    return render_template('information/lightning_node.html', node=node)


@bp.route("/frequently-asked-questions")
def faq():
    """
    Returns a document with answers to common questions on junkbite
    """
    faqs = FAQEntry.query.filter_by(visible=True).all()
    return render_template("information/faq.html", faqs=faqs)


@bp.route("/feedback", methods=["GET", "POST"])
def feedback():
    """
    A form for users to give feedback
    """
    if request.method == "POST":
        description = request.form.get("description")
        flash("Thanks")
        _save_feedback(description)
        return redirect(url_for('information.feedback'))
    return render_template('information/feedback.html')


@bp.route("/support", methods=["GET", "POST"])
def support():
    """
    A form for users to open support tickets
    """
    if request.method == "POST":
        description = request.form.get("description")
        ticket_code = _save_support_ticket(description)
        flash(f"Support ticket, opened. Ticket Code: {ticket_code}")
        sendgrid_new_support_ticket(description, ticket_code)
        return redirect(url_for('information.support_detail', ticket_code=ticket_code))
    if g.user:
        tickets = (
            SupportTicket.query.filter_by(user_id=g.user.id)
            .order_by(SupportTicket.created.desc())
        )
        return render_template('information/support.html', tickets=tickets)
    return render_template('information/support.html', tickets=None)


@bp.route("/support/<ticket_code>/detail", methods=["GET", "POST"])
def support_detail(ticket_code):
    """
    A form for users to open support tickets
    """
    ticket = SupportTicket.query.filter_by(ticket_code=ticket_code).first()
    if not g.user:
        flash("Unauthorized")
        return redirect(url_for("information.support"))
    if ticket.user_id != g.user.id and g.user.admin == False:
        flash("Unauthorized")
        return redirect(url_for("information.support"))
    if request.method == "POST":
        response = request.form.get("response")
        ticket_response = SupportTicketResponse(
            support_ticket_id=ticket.id,
            user_id=g.user.id,
            response_text=response,
        )
        db.session.add(ticket_response)
        db.session.commit()
        flash(f"Response added to ticket")
        return redirect(url_for('information.support_detail', ticket_code=ticket.ticket_code))
    return render_template(
        'information/support_detail.html',
        ticket=ticket
    )
