from flask import Blueprint

bp = Blueprint('information', __name__, 'information')

from app.information import routes
