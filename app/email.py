import os
from flask import (
    render_template,
    current_app
)
from sendgrid.helpers.mail import Mail
from app import db
from app.models import EmailHistory
from app.models import (
    User,
    Post,
    Bounty,
    Comment
)


def sendgrid_send_email(subject, sender, recipients, text_body, html_body,
                        attachments=None, sync=False):
    """
    All emails flow out through this function, this function sends the email

    Makes an Post API call to send_grid to send email.
    send grid client is stored in app.sg on app creation

    recipients can be a string containing a single email or a list containing
    multiple email strings also a tuple or list of tuples (sendgrid api)

    Need to verify a single sender id or something or this doesnt work

    # TODO this currently only supports 1 email at a time otherwise
    it shows all the other emails that it also sent it to... to fix this
    do personalizations in sendgrid but i dont have time right now
    """
    message = Mail(
        from_email=sender,
        to_emails=recipients,
        subject=subject,
        plain_text_content=text_body,
        html_content=html_body,
    )
    try:
        response = current_app.sg.send(message)
        if str(response.status_code)[0] == '2':  # any 200 status is fine
            # save email history
            eh = EmailHistory(
                recipients=recipients,
                sender=sender,
                subject=subject,
                text_content=text_body,
                html_content=html_body
            )
            db.session.add(eh)
            db.session.commit()
            return True
    except Exception as e:
        print(e)
        return False
    return False


def sendgrid_reset_password_email(user):
    token = user.get_reset_password_token()
    payload = {
        'user': user,
        'token': token
    }
    subject = 'Junkbite - Password Reset Request'
    sender=current_app.config['ADMINS'][0]
    recipients=[user.email]
    text_body=render_template('email/reset_password.txt', **payload)
    html_body=render_template('email/reset_password.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_password_updated(user):
    payload = {'user': user}
    subject = 'Junkbite - Password Successfully Updated'
    sender=current_app.config['ADMINS'][0]
    recipients=[user.email]
    text_body=render_template('email/password_updated.txt', **payload)
    html_body=render_template('email/password_updated.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_zap_on_post(payment):
    post = Post.query.get(payment.post_id)
    if not payment.receiving_user.email or payment.receiving_user.email_opt_out:  # or if they are opted out TODO
        return
    payload = {'payment': payment, 'post': post}
    subject = f'Junkbite - {payment.sending_user.username} zapped you sats!'
    sender=current_app.config['ADMINS'][0]
    recipients=[payment.receiving_user.email]
    text_body=render_template('email/new_zap.txt', **payload)
    html_body=render_template('email/new_zap.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_alert_tagged_users(post, users):
    # only opt-in emails
    recipient_emails = [u.email for u in users if u.email and not u.email_opt_out]
    if not recipient_emails:
        return
    payload = {'post': post}
    subject = f'Junkbite - {post.user.username} tagged you in a post'
    sender=current_app.config['ADMINS'][0]
    text_body=render_template('email/tagged_users.txt', **payload)
    html_body=render_template('email/tagged_users.html', **payload)
    for recipient in recipient_emails:
        success = sendgrid_send_email(
            subject=subject,
            sender=sender,
            recipients=recipient,
            text_body=text_body,
            html_body=html_body,
        )
        if not success:
            sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_follower(followee_user, new_follower_user):
    if not followee_user.email or followee_user.email_opt_out:
        return
    payload = {
        'followee_user': followee_user,
        'new_follower_user': new_follower_user
    }
    subject = f'Junkbite - {new_follower_user.username} followed you!'
    sender=current_app.config['ADMINS'][0]
    recipients=[followee_user.email]
    text_body=render_template('email/new_follower.txt', **payload)
    html_body=render_template('email/new_follower.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_bounty_response(bounty_id):
    bounty = Bounty.query.get(bounty_id)
    if not bounty.user.email or bounty.user.email_opt_out:  # or if they are opted out TODO
        return
    payload = {'bounty': bounty}
    subject = f'Junkbite - new response on your bounty'
    sender=current_app.config['ADMINS'][0]
    recipients=[bounty.user.email]
    text_body=render_template('email/new_bounty_response.txt', **payload)
    html_body=render_template('email/new_bounty_response.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_comment_on_post(user, post_id):
    post = Post.query.get(post_id)
    if not post.user:
        return
    if not post.user.email or post.user.email_opt_out:  # or if they are opted out TODO
        return
    payload = {'user': user, 'post': post}
    subject = f'Junkbite - {user.username} commented on your post!'
    sender=current_app.config['ADMINS'][0]
    recipients=[post.user.email]
    text_body=render_template('email/new_comment.txt', **payload)
    html_body=render_template('email/new_comment.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_comment_on_comment(user, parent_comment):
    
    if not parent_comment.user.email or parent_comment.user.email_opt_out:  # or if they are opted out TODO
        return
    payload = {'user': user, 'comment': parent_comment}
    subject = f'Junkbite - {user.username} replied to your comment'
    sender=current_app.config['ADMINS'][0]
    recipients=[parent_comment.user.email]
    text_body=render_template('email/comment_reply.txt', **payload)
    html_body=render_template('email/comment_reply.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_like_on_post(user, post_id):
    post = Post.query.get(post_id)
    if not post.user:
        return
    if post.user.id == user.id:
        return  # don't notify when self liking
    if not post.user.email or post.user.email_opt_out:  # or if they are opted out TODO
        return
    payload = {'user': user, 'post': post}
    subject = f'Junkbite - {user.username} liked your post!'
    sender=current_app.config['ADMINS'][0]
    recipients=[post.user.email]
    text_body=render_template('email/new_like.txt', **payload)
    html_body=render_template('email/new_like.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_notify_admin_new_user(user):
    """
    Just for now while its just me maintaing this chaos
    """
    subject = 'New user alert'
    payload = {
        'username': user.username,
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/new_user_alert.txt', **payload)
    html_body=render_template('email/new_user_alert.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_notify_admin_new_post(post):
    """
    Just for now while its just me maintaing this chaos
    """
    subject = 'New post alert'
    payload = {
        'post': post,
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/new_post_alert.txt', **payload)
    html_body=render_template('email/new_post_alert.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_withdrawals_frozen_alert(info=None):
    """
    Sends an alert to admin if internal accounting system picks up on 
    mismatching payments / entries or invoices
    """
    subject = 'Withdrawals Have Been Frozen By Accounting System'
    payload = {
        'info': info,
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/withdrawals_frozen.txt', **payload)
    html_body=render_template('email/withdrawals_frozen.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')



def sendgrid_notify_admin_new_withdrawal(withdrawal):
    """
    Just for now while its just me maintaing this chaos
    """
    subject = 'New withdrawal alert'
    payload = {
        'withdrawal': withdrawal,
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/new_withdrawal_alert.txt', **payload)
    html_body=render_template('email/new_withdrawal_alert.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_support_ticket(description, ticket_code):
    """
    Just for now while its just me maintaing this chaos
    """
    subject = 'New support ticket'
    payload = {
        'description': description,
        'ticket_code': ticket_code
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/new_support_ticket.txt', **payload)
    html_body=render_template('email/new_support_ticket.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_user_welcome_email(user):
    subject = 'Junkbite - Confirm Email Address'
    token = user.get_email_confirmation_token()
    payload = {
        'username': user.username,
        'token': token
    }
    sender=current_app.config['ADMINS'][0]
    recipients=[user.email]
    text_body=render_template('email/welcome.txt', **payload)
    html_body=render_template('email/welcome.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_new_user_has_confirmed_email(user):
    subject = 'New user has confirmed email - a real user'
    payload = {
        'user': user,
    }
    sender=current_app.config['ADMINS'][0]
    recipients=['psqnt@pm.me']
    text_body=render_template('email/new_confirmed_email.txt', **payload)
    html_body=render_template('email/new_confirmed_email.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')


def sendgrid_confirm_email(user):
    subject = 'Confirm Email Address'
    token = user.get_email_confirmation_token()
    payload = {
        'username': user.username,
        'token': token
    }
    sender=current_app.config['ADMINS'][0]
    recipients=[user.email]
    text_body=render_template('email/confirm_email.txt', **payload)
    html_body=render_template('email/confirm_email.html', **payload)
    success = sendgrid_send_email(
        subject=subject,
        sender=sender,
        recipients=recipients,
        text_body=text_body,
        html_body=html_body,
    )
    if not success:
        sendgrid_send_error_email_to_admin('sendgrid email failed to send')



def sendgrid_send_error_email_to_admin(error):
    """
    Sends an error message to an admin, helps with ops monitoring
    """
    sendgrid_send_email(
        '[junkbite] An Exception Occured',
        sender=current_app.config['ADMINS'][0],
        recipients=['psqnt@pm.me'],
        text_body=render_template(
            'email/error_notify_admin.txt', error=error
        ),
        html_body=render_template(
            'email/error_notify_admin.txt', error=error
        )
    )


def sendgrid_send_weird_user_balance_update(error=None):
    """
    Sends an error message to an admin, helps with ops monitoring
    """
    sendgrid_send_email(
        '[junkbite] Weird user balance update',
        sender=current_app.config['ADMINS'][0],
        recipients=['psqnt@pm.me'],
        text_body=render_template(
            'email/balance_update_weird.txt', error=error
        ),
        html_body=render_template(
            'email/balance_update_weird.html', error=error
        )
    )


def sendgrid_send_withdrawal_issue_alert(error):
    """
    Sends an error message to an admin, helps with ops monitoring
    """
    sendgrid_send_email(
        '[junkbite] - An issue with withdrawals just occurred',
        sender=current_app.config['ADMINS'][0],
        recipients=['psqnt@pm.me'],
        text_body=render_template(
            'email/error_notify_admin.txt', error=error
        ),
        html_body=render_template(
            'email/error_notify_admin.txt', error=error
        )
    )


def send_email_withdrawal_sent(user, withdrawal):
    sendgrid_send_email(
        '[junkbite] Withdrawal Sent',
        sender=current_app.config['ADMINS'][0],
        recipients=user['email'],
        text_body=render_template(
            'email/withdrawal_sent.txt', user=user, withdrawal=withdrawal
        ),
        html_body=render_template(
            'email/withdrawal_sent.html', user=user, withdrawal=withdrawal
        )
    )


def send_email_new_reported_post(reported_post):
    sendgrid_send_email(
        '[junkbite] Post Reported',
        sender=current_app.config['ADMINS'][0],
        recipients='ledgendairy1@gmail.com',
        text_body=render_template(
            'email/new_reported_post.txt', reported_post=reported_post
        ),
        html_body=render_template(
            'email/new_reported_post.html', reported_post=reported_post
        )
    )
