import functools
from flask import redirect, url_for, session, flash, g, request, current_app
from app.auth import bp
from app.models import User


AUTH_LOGIN = 'auth.login'


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    if user_id is None:
        g.user = None
    else:
        try:
            user = User.query.get(user_id)
            g.user = user if not user.banned else None
        except Exception:
            g.user = None


def login_required(view):
    """
    Some actions require users to be logged in. The decorator allows us to check
    this for each view it is applied to.
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:  # if the user isnt logged in, redirect to login
            flash('Login for this function')
            return redirect(url_for(AUTH_LOGIN, wantsurl=request.path))
        if g.user.banned is True:  # if the user isnt logged in, redirect to login
            return redirect(url_for(AUTH_LOGIN, wantsurl=request.path))
        return view(**kwargs)
    return wrapped_view


def admin_login_required(view):
    """
    Some actions requires a user to be an admin. The decorator checks this for
    each view it is applied to.
    """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:  # if the user isnt logged in, redirect to login
            flash('Unauthorized')
            return redirect(url_for(AUTH_LOGIN, wantsurl=request.path))
        if g.user.admin is False or g.user.banned:
            flash('You cannot go there')
            return redirect(url_for('index'))
        return view(**kwargs)
    return wrapped_view