import re
import json
import requests
from datetime import datetime
from flask import (
    current_app,
    flash,
    g,
    redirect,
    render_template,
    request,
    session,
    url_for
)
from werkzeug.security import (
    check_password_hash,
    generate_password_hash
)
from app import db
from app.auth import bp
from app.auth.utils import login_required
from app.utils.db_utils import (
    _create_user,
    _get_user_by_username,
    _update_user_last_login,
    _get_user_by_email,
    _update_user_password
)
from app.email import (
    sendgrid_reset_password_email,
    sendgrid_new_user_welcome_email,
    sendgrid_notify_admin_new_user,
    sendgrid_confirm_email,
    sendgrid_new_user_has_confirmed_email
)
from app.models import User


AUTH_LOGIN = 'auth.login'
IP_BANS = [
    "213.227.129.35",
    "112.30.45.53",
    "91.218.177.124",
    "41.60.123.81",
    "121.180.182.158",
    "117.208.255.250",
    "197.237.243.237",
    "41.104.49.114",
    "41.105.229.151",
    "41.105.127.120",
    "211.141.155.131",
    "197.58.144.234",
    "197.58.127.37",
    "110.247.142.241",
    "213.120.145.223",
    "41.104.49.114",
    "41.105.28.74",
    "41.105.127.120",
    "41.105.229.151",
    "41.105.121.80"
]



def _is_human(recaptcha_response):
    """
    Verify the recaptcha in the request
    """
    secret = current_app.config.get("RECAPTCHA_SECRET")
    payload = {
        "response": recaptcha_response,
        "secret": secret
    }
    url = "https://google.com/recaptcha/api/siteverify"
    response = requests.post(url, payload)
    return json.loads(response.text)['success']


@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        if g.user:
            return redirect(url_for('bombadillo.threads'))
        username = request.form.get('username')
        if username:
            username = username.lower()
        password = request.form.get('password')
        error = None
        if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
            ip_address = request.environ.get('REMOTE_ADDR')
        else:
            ip_address = request.environ.get('HTTP_X_FORWARDED_FOR')
        if ip_address in IP_BANS:
            # dont allow these ips
            return redirect(url_for('auth.login'))
        user = _get_user_by_username(username)
        if user is None:
            error = 'Username not registered'
            current_app.logger.info(
                f"Failed login attempt for user: {username}. "
                "Reason: username not registered"
            )
        elif not user.check_password(password):
            error = 'Incorrect password'
            current_app.logger.info(
                f"Failed login attempt for username: {username}. "
                "Reason: Incorrect password attempt."
            )
        if error is None:
            user.ip_addresses = user.ip_addresses + f",{ip_address}"
            user.last_login = datetime.now()
            db.session.add(user)
            db.session.commit()
            session.clear()
            session['user_id'] = user.id
            session['admin_id'] = user.id if user.admin == True else None
            session.permanent = True
            current_app.logger.info(f"Successful login for user: {username}")
            if session['admin_id'] is not None:
                current_app.logger.info(
                    f"Successful admin access granted for: {username}"
                )
            if "wantsurl" in request.url:
                return redirect(request.args.get('wantsurl'))
            else:
                return redirect(url_for('index'))
        flash(error)
    if g.user:
        return redirect(url_for('bombadillo.threads'))
    recaptcha_site_key = current_app.config.get("RECAPTCHA_KEY")
    return render_template(
        'auth/login.html',
        recaptcha_site_key=recaptcha_site_key
    )


@bp.route('/logout')
def logout():
    if g.user:
        current_app.logger.info(f"Successful manual logout for user: {g.user.username}")
    session.clear()
    return redirect(url_for('auth.login'))


@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        if not _is_human(request.form.get("g-recaptcha-response")):
            flash("Nice try, robot")
            return redirect(url_for('auth.register'))
        username = request.form.get('username')
        if username:
            username = username.lower()
        error = None
        if not re.match(r"^[a-z0-9_]{3,30}$", username):  # No need to compile
            error = "Username must be lowercase letters or numbers, no whitespace, more than 2 characters and less than 30 in length"
        password = request.form.get('password')
        if password and len(password) < 8:
            error = "Please make a password longer than 8 characters"
        email = request.form.get('email')
        if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
            ip_address = request.environ.get('REMOTE_ADDR')
        else:
            ip_address = request.environ.get('HTTP_X_FORWARDED_FOR')
        if ip_address in IP_BANS:
            # dont allow these ips
            return redirect(url_for('auth.login'))
        # check for errors
        if not username:
            error = 'Username is required'
            current_app.logger.info("Failed registration attempt. Reason: No username entered")
        elif not password:
            error = 'Password is required'
            current_app.logger.info("Failed registration attempt. Reason: No password entered")
        elif User.query.filter_by(username=username).first() is not None:
            error = f'{username} is already registered'
            current_app.logger.info("Failed registration attempt. Reason: Username already in use")
        elif email and User.query.filter_by(email=email).first() is not None:
            error = f'{email} already has an account registered. Use Forgot Password link to recover account'
            current_app.logger.info("Failed registration attempt. Reason: Email already in use")
        # if no errors are found, save user to database
        # redirect them to login page
        if error is None:
            user = _create_user(username, password, False, ip_address, email)
            session.clear()
            session['user_id'] = user.id
            session.permanent = True
            current_app.logger.info(f"Successful registration for User: {username}")
            sendgrid_notify_admin_new_user(user)
            if email:
                user = _get_user_by_username(username)
                sendgrid_new_user_welcome_email(user)
                return redirect(url_for('bombadillo.user_confirm_email', username=user.username))
            flash('Successfully registered')
            return redirect(url_for('bombadillo.view_user', username=user.username))
        flash(error)
    recaptcha_site_key = current_app.config.get("RECAPTCHA_KEY")
    return render_template(
        'auth/register.html',
        recaptcha_site_key=recaptcha_site_key
    )


@bp.route('/reset-password-request', methods=("GET", "POST"))
def reset_password_request():
    if request.method == "POST":
        email = request.form.get("email")
        user = _get_user_by_email(email)
        if user:
            # send password reset email
            current_app.logger.info(f"Successful password reset request for user: {email}")
            sendgrid_reset_password_email(user)
            flash("Check your inbox for a reset password link")
        return redirect(url_for(AUTH_LOGIN))
    return render_template("auth/reset_password_request.html")


@bp.route('/reset-password/<token>', methods=("GET", "POST"))
def reset_password(token):
    user = User.verify_reset_password_token(token)
    if not user:
        current_app.logger.info(f"Failed password reset attempt with invalid token: {token}")
        flash('Invalid Reset Token')
        return redirect(url_for(AUTH_LOGIN))
    if request.method == "POST":
        new_password = request.form.get("password")
        _update_user_password(user.id, new_password)
        current_app.logger.info(f"Successful password reset for user: {user.username}")
        flash('Your password has been reset.')
        return redirect(url_for(AUTH_LOGIN))
    return render_template('auth/reset_password.html')


@bp.route('/resend-confirm-email', methods=("GET", "POST"))
@login_required
def resend_confirm_email():
    if request.method == "POST":
        # send password reset email
        current_app.logger.info(f"Successful confirm email sent: {g.user.username}")
        sendgrid_confirm_email(g.user)
        flash("Check your inbox for a confirmation email")
        return redirect(url_for('bombadillo.view_user', username=g.user.username))
    if not g.user.email:
        flash("Please enter an email in edit profile then click confirm email")
        return redirect(url_for('bombadillo.view_user', username=g.user.username))
    return render_template("auth/resend_confirm_email.html")


@bp.route('/confirm-email/<token>', methods=("GET", "POST"))
def confirm_email(token):
    user = User.verify_email_confirmation_token(token)
    if not user:
        current_app.logger.info(f"Failed to confirm email with token: {token}")
        flash('Invalid Confirmation Token')
        return redirect(url_for(AUTH_LOGIN))
    sendgrid_new_user_has_confirmed_email(user)
    user.email_verified = True
    db.session.add(user)
    db.session.commit()
    flash("Your email has been verified. Thank you!")
    return redirect(url_for('bombadillo.view_user', username=user.username))
