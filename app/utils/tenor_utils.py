import TenGiphPy
from flask import (
    current_app
)


def search_tenor_gifs(search_term, limit=8, safe_search=False):
    """
    Search for gives by term, limit default is 8
    """
    t = TenGiphPy.Tenor(token=current_app.config.get("TENOR_KEY"))
    results = t.search(tag=search_term, safesearch=safe_search, limit=limit)
    urls = []
    for r in results['results']:
        urls.append(r["media"][0]["gif"]["url"])
    return urls
