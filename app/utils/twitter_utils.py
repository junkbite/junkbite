import os
import tweepy
from pathlib import Path
from jinja2 import Template
from flask import current_app


def compose_tweet(data: dict, template_filename):
    """
    renders a jinja2 template with the given data for the given template,
    dict keys must match template
    :param tweet_data: a dictionary with keys matching a template
    :return: a rendered template
    """
    placeholder = (
        Path(__file__).parent.absolute() / Path(template_filename)
    ).read_text()
    template = Template(placeholder)
    return template.render(**data)


def authorize():
    """
    Authorizes this app to interact with the @junkbite_tweets twitter
    :return: an api instance -- api.update_status
    """
    consumer_key = current_app.config['TWITTER_KEY']
    consumer_secret = current_app.config['TWITTER_SECRET']
    twitter_key = current_app.config['TWITTER_ACCESS_TOKEN']
    twitter_secret = current_app.config['TWITTER_ACCESS_SECRET']
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(twitter_key, twitter_secret)
    return tweepy.API(auth)


def send_tweet(content):
    """
    Sends a tweet using the given api instance with `content`
    :param api: what is returned from `authorize`
    :param content: string of text meant for a tweet
    """
    try:
        api = authorize()
        response = api.update_status(content)
        return "Success"
    except tweepy.error.TweepError as error:
        current_app.logger.info(f"Twitter send tweet error: {error}")
        return str(error)


def send_media_tweet(filename, content):
    """
    Sends a tweet with an image in it given the api instance 
    """
    try:
        api = authorize()
        full_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
        response = api.update_with_media(full_path, content)
        return "Success"
    except tweepy.error.TweepError as error:
        current_app.logger.info(f"Twitter send media tweet error: {error}")
        return str(error)


def update_twitter_image(filename):
    """
    Updates profile picture of the account connected to api
    """
    try:
        api = authorize()
        full_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
        api.update_profile_image(full_path)
        return None
    except tweepy.error.TweepError as error:
        current_app.logger.info(f"Twitter update profile image error: {error}")
        return str(error)


def update_twitter_background_image(filename):
    """
    Updates profile picture of the account connected to api
    """
    try:
        api = authorize()
        full_path = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
        api.update_profile_banner(full_path)
        return None
    except tweepy.error.TweepError as error:
        current_app.logger.info(f"Twitter update banner image error: {error}")
        return str(error)
