import operator
import random
import jwt
import re
import shortuuid
from slugify import slugify
from datetime import datetime, timedelta
from time import time
from flask import g, current_app
from werkzeug.exceptions import abort
from app.utils.meta_utils import (
    _bitcoin_to_sats,
    _datetime_to_humantime,
    _sats_to_bitcoin,
    _sqliterow_to_dict
)
from app import db
from app.models import (
    User,
    Post,
    Comment,
    Payment,
    UserBalance,
    UserBalanceEntry,
    FollowRequest,
    Notification,
    Bounty,
    BountyResponse,
    Withdrawal,
    ReportedContent,
    Followers,
    UserNode,
    PostNode,
    Payment,
    PostLike,
    BountyResponseLike,
    Bookmark,
    CommentLike,
    Blog,
    OnionUrl,
    Advertisement,
    AdvertisementStats,
    JunkbiteNode,
    Feedback,
    SupportTicket,
    LightningAppConfiguration,
    PaymentProcessingFee,
    Settings
)
from app.email import (
    sendgrid_alert_tagged_users,
    sendgrid_send_weird_user_balance_update
)


DELETED = '<deleted>'


def _create_user(username, password, admin, ip_address=None, email=None):
    user = User(
        user_code=shortuuid.uuid(),
        username=username,
        admin=admin,
        original_ip_address=ip_address if ip_address else '127.0.0.1',
        ip_addresses=""
    )
    user.set_password(password)
    if email:
        user.email = email
    db.session.add(user)
    db.session.commit()
    user_balance = UserBalance(
        user_id=user.id,
        balance=0
    )
    db.session.add(user_balance)
    db.session.commit()
    first_entry = UserBalanceEntry(
        user_balance_id=user_balance.id,
        before_balance=0,
        action_type="Account Creation"
    )
    db.session.add(first_entry)
    db.session.commit()
    return user


def _api_create_post(post):
    new_post = Post(
        author_id=post['user_id'],
        title=post['title'],
        body=post['body'],
        processed_body=post['processed_body'],
        upload=post['upload'],
        unique_filename=post['unique_filename'],
        nsfw=0,
        meme=1
    )
    db.session.add(new_post)
    db.session.commit()
    return new_post.id


def _api_get_users():
    # Todo limit this and paginate
    user_query = User.query.all()
    users = []
    for user in user_query:
        users.append(user.serialized())
    return users


def _api_get_posts():
    post_query = Post.query.filter_by(draft=False)
    posts = []
    for post in post_query:
        posts.append(post.serialized())
    return posts


def _api_get_post_comment_mapping():
    comment_query = Comment.query.all()
    post_comment_mapping = {}
    for comment in comment_query:
        if comment.post_id not in post_comment_mapping:
            post_comment_mapping[comment['post_id']] = []
        post_comment_mapping[comment['post_id']].append(comment)
    return post_comment_mapping


def _api_get_comments():
    comment_query = Comment.query.all()
    comments = []
    for comment in comment_query:
        comments.append(comment.serialized())
    return comment


def _api_get_threads():
    posts = _api_get_posts()
    post_comment_mapping = _api_get_post_comment_mapping()
    data = []
    for post in posts:
        post = _sqliterow_to_dict(post)
        post['comments'] = []
        db_comments = post_comment_mapping.get(post['id'], [])
        for comment in db_comments:
            post['comments'].append(_sqliterow_to_dict(comment, comment=True))
        data.append(post)
    return data


def _create_advertisement(price, hours_purchased):
    """
    Writes a record of an advertisement purchase in the database, update later
    with which post was purchased
    """
    ad = Advertisement()
    db.session.add(ad)
    db.session.comit()
    # advertisement (price, hours_purchased)
    return ad.id


def _delete_advertisement(ad_id):
    """
    Delete a specific advertisement from database

    :param id: id of the advertisement to be deleted
    """
    ad = Advertisement.query.get(ad_id)
    db.session.delete(ad)
    db.session.commit()


def _delete_comment(comment_id):
    """
    Delete a specific comment from database

    :param id: id of the comment to be deleted.
    """
    comment = Comment.query.get(comment_id)
    db.session.delete(comment)
    db.commit()


def _delete_expired_ads():
    """
    Delete all expired ads based on hours purchased and created timestamp
    """
    ads = Advertisement.query.all()
    expired_ads = []
    for ad in ads:
        try:
            expiry = ad.created + timedelta(hours=int(ad.hours_purchased)) - timedelta(hours=5)
            now = datetime.now()
            if now > expiry:
                expired_ads.append(ad.id)
                _delete_advertisement(ad.id)
                _delete_post(ad.post_id)
        except ValueError as e:
            print(e)
            expired_ads.append(ad.id)
            _delete_advertisement(ad.id)
            _delete_post(ad.post_id)
        except Exception as e:
            print(e)
    return len(expired_ads)


def _delete_user(user_id):
    """
    Delete a specific user from database

    :param id: id of the user to be deleted.
    """
    user = User.query.get(user_id)
    db.session.delete(user)
    db.session.commit()


def _delete_blog(blog_id):
    """
    Delete the blog post by id
    """
    blog = Blog.query.get(blog_id)
    db.session.delete(blog)
    db.session.commit()


def _delete_post_like(like_id):
    """
    Deletes a user's like of a post to database by ID
    """
    like = PostLike.query.get(like_id)
    db.session.delete(like)
    db.session.commit()


def _delete_bookmark(bookmark_id):
    """
    Deletes a bookmark by ID
    """
    bookmark = Bookmark.query.get(bookmark_id)
    db.session.delete(bookmark)
    db.session.commit()


def _delete_comment_like(like_id):
    """
    Deletes a user's like of a post to database by ID
    """
    like = CommentLike.query.get(like_id)
    db.session.delete(like)
    db.session.commit()


def _delete_post(post_id):
    """
    Deletes a post
    """
    post = Post.query.get(post_id)
    db.session.delete(post)
    db.session.commit()
    

def _delete_comment(comment_id):
    """
    Sets the contents of a comment to <deleted>
    """
    comment = Comment.query.get(comment_id)
    db.session.delete(comment)
    db.session.commit()


def _get_settings():
    """
    return junkbite settings
    """
    return Settings.query.get(1)


def _freeze_withdrawals(message=None):
    """
    Changes the settings to freeze withdrawals incase of attack or error
    """
    s = Settings.query.get(1)
    s.withdrawals_frozen = True
    if message:
        s.withdrawals_frozen_message = message
    db.session.add(s)
    db.session.commit()


def _get_advertisement_post_ids():
    """
    Return latest advertisement
    """
    advertisement_query = Advertisement.query.filter_by(expired=False).all()
    advertisements = []
    for ad in advertisement_query:
        advertisements.append(ad)
    return advertisements


def _get_advertisement_posts(post_ids):
    """
    Returns the posts in from the post table that are live ads
    """
    advertisement_query = Advertisement.query.filter_by(expired=False).all()
    advertisements = []
    for ad in advertisement_query:
        advertisements.append(ad)
    return advertisements
    '''
    liked_posts = _get_user_likes()
    post_payment_sums = _sum_post_payments()
    # Make sqlite rows into dicts so can be edited
    posts = []
    for p in db_posts:
        posts.append(_sqliterow_to_dict(p))
    for post in posts:
        post['created'] = _datetime_to_humantime(post['created'])
        # hit the database for comment count.
        post['sats_boosted'] = int(post_payment_sums.get(post['id'], 0))
        post['liked'] = False
        if post['id'] in liked_posts:
            post['liked'] = True
    # sort posts by number of sats and likes
    posts.sort(key=operator.itemgetter('sats_boosted'), reverse=True)
    '''
    return advertisements


def _junkbite_daily_stats():
    """
    Several queries relating to daily statistics in an admin view
    """
    new_users_today = User.created_today()
    active_users_today = User.active_today()
    payments = Payment.created_today()
    jb_payments = Payment.paid_to_junkbite_today()
    return new_users_today, active_users_today, payments, jb_payments


def _get_anon_user_id():
    """
    Given there will always be an `anonymous` user allowed to buy and create
    posts, this function returns that user as each post needs a user so this
    user is a system user but used in place of an actual user
    """
    return User.query.filter_by(username='anonymous').first().id


def _get_anon_user():
    """
    Given there will always be an `anonymous` user allowed to buy and create
    posts, this function returns that user as each post needs a user so this
    user is a system user but used in place of an actual user
    """
    return User.query.filter_by(username='anonymous').first()



def _get_current_user_id():
    try:
        return g.user.id
    except Exception:
        return _get_anon_user_id()


def _get_blog(slug):
    """
    Get a blog by title
    """
    blog = Blog.query.filter_by(slug=slug).first()
    if blog:
        return blog
    return None


def _get_blogs_for_view():
    """
    Returns list of blogs for the blogs view function
    """
    blog_query = Blog.query.order_by(Blog.created.desc()).all()
    blogs = []
    for blog in blog_query:
        blogs.append(blog)
    return blogs


def _get_comment_by_id(comment_id):
    """
    Returns a comment from database
    """
    comment = Comment.query.get(comment_id)
    return comment


def _get_liked_posts(limit=20, offset=0):
    """
    Returns the set of posts that a user has liked
    """
    liked_query = (
        PostLike.query.filter_by(user_id=g.user.id)
        .order_by(PostLike.created.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )
    post_ids = [
        b.post_id for b in liked_query
    ]
    liked_posts = (
        Post.query.filter(Post.id.in_(post_ids))
        .order_by(Post.created.desc())
        .all()
    )
    return liked_posts


def _get_bookmarked_posts(limit=20, offset=0):
    """
    Returns the set of posts that a user has bookmarked
    """
    bookmarked_query = (
        Bookmark.query.filter_by(user_id=g.user.id)
        .order_by(Bookmark.created.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )
    post_ids = [
        b.post_id for b in bookmarked_query
    ]
    bookmarked_posts = (
        Post.query.filter(Post.id.in_(post_ids))
        .order_by(Post.created.desc())
        .all()
    )
    return bookmarked_posts


def _get_zapped_posts(limit=20, offset=0):
    """
    Returns posts that a user has zapped
    """
    zaps = (
        Payment.query.filter_by(sending_user_id=g.user.id, status="completed")
        .order_by(Payment.created.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )
    post_ids = [
        z.post_id for z in zaps if z.post_id
    ]
    zapped_posts = (
        Post.query.filter(Post.id.in_(post_ids))
        .order_by(Post.created.desc())
        .all()
    )
    return zapped_posts


def _get_random_post():
    """
    Returns a post at random
    """
    post_query = Post.query.filter_by(draft=False)
    count = int(post_query.count())
    random_post = post_query.offset(int(count*random.random())).first()
    return random_post.post_code


def _get_explore_posts(filter_by="sats", scope="today", limit=20, offset=0):
    """
    Returns the set of posts for the explore page
    """
    day_map = {
        "today": 1,
        "week": 7,
        "month": 28,
        "all": (datetime.today() - datetime(2021, 1, 1)).days
    }
    days = day_map.get(scope, 1)  # default to today just in case
    greater_than_timestamp = datetime.today() - timedelta(days=days)
    post_query = Post.query.filter_by(draft=False)
    if filter_by == "sats":
        explore_posts = (
            post_query.filter(Post.created > greater_than_timestamp)
            .order_by(Post.sats_boosted.desc())
            .limit(limit)
            .offset(offset)
            .all()
        )
    elif filter_by == "likes":
        explore_posts = (
            post_query.filter(Post.created > greater_than_timestamp)
            .order_by(Post.like_count.desc())
            .limit(limit)
            .offset(offset)
            .all()
        )
    elif filter_by == "comments":
        explore_posts = (
            post_query.filter(Post.created > greater_than_timestamp)
            .order_by(Post.comment_count.desc())
            .limit(limit)
            .offset(offset)
            .all()
        )
    else:
        explore_posts = (
            post_query.filter(Post.created > greater_than_timestamp)
            .order_by(Post.created.desc())
            .limit(limit)
            .offset(offset)
            .all()
        )
    return explore_posts


def _get_post_like(post_id, user_id):
    """
    Retrieves a like
    """
    post_like = (
        PostLike.query.filter_by(user_id=user_id, post_id=post_id).first()
    )
    if post_like is None:
        return False
    return post_like.id


def _get_comment_like(comment_id, user_id):
    """
    Retrieves a comment like
    """
    comment_like = (
        CommentLike.query.filter_by(user_id=user_id, comment_id=comment_id)
        .first()
    )
    if comment_like is None:
        return False
    return comment_like.id


def _get_bookmark(user_id, post_id=None, bounty_id=None):
    """
    Retrieves a Bookmark
    """
    if post_id:
        bookmark = Bookmark.query.filter_by(
            post_id=post_id, user_id=user_id
        ).first()
    if bounty_id:
        bookmark = Bookmark.query.filter_by(
            bounty_id=bounty_id, user_id=user_id
        ).first()
    if bookmark:
        return bookmark.id
    return False


def _get_junkbite_node():
    """
    Return the latest entry in the junkbite node table
    """
    # TODO check number of entries... should there only be 1? Email admin
    # if more than 1? idk
    node = JunkbiteNode.query.order_by(JunkbiteNode.id.desc()).first()
    return node


def _get_post_comments(post_id, limit=10, offset=0):
    """
    Returns a list of comments on a given post, ignore nested comments
    TODO: if a nested comment is popular it should be shown or from a 
    followed user
    """
    comments = (
        Comment.query.filter_by(post_id=post_id, comment_id=None)
        .limit(limit)
        .offset(offset)
        .all()
    )
    return comments


def _get_comment_comments(comment_id, limit=10):
    """
    Returns a list of comments on a given comment
    """
    comments = (
        Comment.query.filter_by(comment_id=comment_id)
        .limit(limit)
        .all()
    )
    return comments


def _get_post_author(post_id):
    """
    Returns the user id of a the user who created the post
    """
    post = Post.query.get(post_id)
    if post:
        return post.author_id
    return None


def _get_post(post_code):
    """
    Get a post by code or id
    """
    if type(post_code) == str:
        post = Post.query.filter_by(post_code=post_code).first()
    else:
        post = Post.query.get(post_code)
    return post


def _get_comment(comment_code):
    """
    Get a comment by comment_code
    """
    return Comment.query.filter_by(comment_code=comment_code).first()


def _get_images(limit=20, offset=0):
    """
    Return a list of posts and comments that contain images.
    This function first searches the 'images folder' to get filenames of
    all images currently in the system.
        'images folder' = current_app.config['UPLOAD_FOLDER']

    Discard any image files that have been 'deleted' (images may still
    be on the system after post has been deleted) from the list being returned.

    :return <list>: images (list of posts/comments which contain an image)
    """
    post_query = (
        Post.query.filter(
            Post.draft!=True,
            (Post.unique_filename!=None) |
            (Post.unique_filename!='<deleted>')
        )
        .limit(limit)
        .offset(offset)
        .all()
    )
    images = []
    for post in post_query:
        images.append(post)

    comment_query = (
        Comment.query.filter(
            (Comment.unique_filename!=None) | (Comment.unique_filename!='<deleted>')
        )
        .limit(limit)
        .offset(offset)
        .all()
    )
    for comment in comment_query:
        images.append(comment)
    return images


def _get_withdrawals(user_id):
    """
    Return all withdrawals
    ```
    initiated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    amount INTEGER NOT NULL,
    ln_invoice TEXT NOT NULL,
    node_uri TEXT NOT NULL,
    status TEXT NOT NULL,
    ```
    """
    # TODO paginated
    withdrawal_query = (
        Withdrawal.query.filter_by(user_id=user_id)
        .order_by(Withdrawal.created.desc())
    )
    withdrawals = []
    for w in withdrawal_query:
        withdrawals.append(w)
    return withdrawals


def _get_post_from_image(image):
    """
    Get the id of post or 'id and post_id' of comment associated with an image

    :param image: filename of image from 'UPLOAD_FOLDER'
    :return <sqlite.row>: post
    """
    post = Post.query.filter_by(draft=False, unique_filename=image).first()
    if post is None:  # image is from a comment
        post = Comment.query.filter_by(unique_filename=image).first()
    return post


def _get_user_balance(user_id):
    """
    Retrieve user balance
    """
    user_balance = UserBalance.query.filter_by(user_id=user_id).first()
    if user_balance:
        return user_balance.balance
    return 0


def _update_user_last_login(user_id):
    """
    Update users last login time
    """
    now = datetime.now()
    user = User.query.get(user_id)
    user.last_login = now
    db.session.add(user)
    db.session.commit()


def _get_user_by_username(username):
    return User.query.filter_by(username=username).first()


def _get_user_by_email(email):
    return User.query.filter_by(email=email).first()


def _get_user_by_id(user_id):
    return User.query.get(user_id)


def _get_user_posts(user_id, limit=20, offset=0):
    """
    Retreives a user's posts for view in profile
    :param: user - returned from _get_user
    """
    return (
        Post.query.filter_by(draft=False, author_id=user_id)
        .order_by(Post.id.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )


def _get_user_following(username):
    user = _get_user_by_username(username)
    # the follower is username, so take the other side of that and you get
    # users that (username) is following
    follower_ids = [
        i.followee_id for i in Followers.query.filter_by(follower_id=user.id)
    ]
    return user, User.query.filter(User.id.in_(follower_ids))


def _get_user_followers(username):
    user = _get_user_by_username(username)
    # the followee is username, so take the other side of that and you get
    # the user that is following (username)
    user_ids = [
        i.follower_id for i in Followers.query.filter_by(followee_id=user.id)
    ]
    return user, User.query.filter(User.id.in_(user_ids))


def _get_user_drafts(user_id, limit=20, offset=0):
    """
    Retreives a user's posts that are draft=True for view in profile
    :param: user - returned from _get_user
    """
    return (
        Post.query.filter_by(draft=True, author_id=user_id)
        .limit(limit)
        .offset(offset)
        .all()
    )


def _get_user(username):
    """
    Retrieve a user's data.
    Data from user table (user_id, username, created)
    Data from post table (*) - all posts
    user balance and node uri
    :return <list>: content created by this user
    """
    if type(username) == str:
        user = User.query.filter_by(username=username).first()
    else:
        user = User.query.get(username)
    if user:
        balance = UserBalance.query.filter_by(user_id=user.id).first()
        return user, balance.balance
    return abort(404)


def _get_user_upvotes():
    """
    Return the logged in user's upvoted bounty responses
    """
    user_id = _get_current_user_id()
    upvotes = BountyResponseLike.query.filter_by(user_id=user_id).all()
    return [upvote.id for upvote in upvotes]


def _get_user_bookmarked_posts():
    """
    Return the logged in user's bookmarks
    """
    user_id = _get_current_user_id()
    bookmarks = Bookmark.query.filter_by(user_id=user_id).all()
    return [bookmark.id for bookmark in bookmarks]


def _get_user_likes():
    """
    Return the logged in user's likes
    """
    user_id = _get_current_user_id()
    likes = PostLike.query.filter_by(user_id=user_id).all()
    return [like.id for like in likes]


def _get_post_likes(post_code):
    """
    Returns a list of users who have liked a given post
    """
    post = Post.query.filter_by(post_code=post_code).first()
    if not post:
        return []
    likes = PostLike.query.filter_by(post_id=post.id).limit(50).all()
    return post, [like.user for like in likes]


def _get_post_zaps(post_code):
    """
    Returns a list of users who have zapped a given post
    """
    post = Post.query.filter_by(post_code=post_code).first()
    if not post:
        return []
    zaps = (
        Payment.query.filter_by(post_id=post.id, status="completed")
        .limit(50)
        .all()
    )
    return post, [zap.sending_user for zap in zaps]


def _get_user_comment_likes(post_id):
    """
    Returns which comments a user has liked on a given post
    """
    user_id = _get_current_user_id()
    likes = CommentLike.query.filter_by(user_id=user_id).all()
    return [like.id for like in likes]


def _sum_post_payments():
    """
    Queries for all payments paid to posts and sums them up. Returns a dict
    of {post_id: summed tips} so one can lookup sum by post id
    """
    payment_query = Payment.query.filter(
        Payment.status=="completed", Payment.post_id!=None
    )
    sum_lookup = {}
    for payment in payment_query:
        if payment.post_id not in sum_lookup:
            sum_lookup[payment.post_id] = 0
        sum_lookup[payment.post_id] += payment.amount
    return sum_lookup


def _sum_bounty_response_payments():
    """
    Queries for all payments paid to posts and sums them up. Returns a dict
    of {post_id: summed tips} so one can lookup sum by post id
    """
    payment_query = Payment.query.filter(
        Payment.status=="completed",
        Payment.bounty_response_id!=None,
        Payment.junkbite_app=="bounty"
    )
    sum_lookup = {}
    for payment in payment_query:
        if payment.bounty_response_id not in sum_lookup:
            sum_lookup[payment.bounty_response_id] = 0
        sum_lookup[payment.bounty_response_id] += payment.amount
    return sum_lookup


def _sum_post_likes():
    """
    Queries for all payments paid to posts and sums them up. Returns a dict
    of {post_id: summed tips} so one can lookup sum by post id
    """
    likes = PostLike.query.all()
    total_likes_lookup = {}
    for like in likes:
        if like.post_id not in total_likes_lookup:
            total_likes_lookup[like.post_id] = 0
        total_likes_lookup[like.post_id] += 1
    return total_likes_lookup


def _get_user_profile_picture(user_id):
    """
    Returns the unique_filename for a given user_id
    """
    user = User.query.get(user_id)
    if user:
        return user.unique_filename
    return None  # maybe a default picture?


def _get_user_bounties(username, limit=20, offset=0):
    """
    Retrieves the bounties a user has created
    """
    bounties = (
        Bounty.query.filter_by(username=username)
        .limit(limit)
        .offset(offset)
        .all()
    )
    return bounties


def _get_bounties_user_responded_to(username, limit=20, offset=0):
    """
    Retrieves the bounties a user has created
    """
    bounty_responses = (
        BountyResponse.query.filter_by(username=username)
        .limit(limit)
        .offset(offset)
        .all()
    )
    return bounty_responses


def _get_bounty_detail(bounty_id):
    """
    Retrieves database information on a given bounty id
    """
    bounty = Bounty.query.get(bounty_id)
    print(bounty)
    if not bounty:
        return None, []
    return bounty, bounty.responses  # TODO


def _get_all_bounties(limit=20, offset=0, order_by="highest"):
    """
    Returns all bounties regardless of status
    """
    bounties = (
        Bounty.query.filter_by()
        .limit(limit)
        .offset(offset)
        .all()
    )
    return bounties


def _get_closed_bounties(limit=20, offset=0, order_by="highest"):
    """
    Returns a list of closed bounties for view, ordered_by param
    """
    bounties = (
        Bounty.query.filter_by(status="closed")
        .limit(limit)
        .offset(offset)
        .all()
    )
    return bounties


def _get_open_bounties(limit=20, offset=0, order_by="highest"):
    """
    Returns a list of open bounties for view, ordered_by param
    """
    bounties = (
        Bounty.query.filter_by(status="open")
        .limit(limit)
        .offset(offset)
        .all()
    )
    return bounties


def _get_bounty_response_upvote(response_id, user_id):
    """
    Retrieves a bounty response upvote
    """
    return BountyResponseLike.query.filter_by(
            bounty_response_id=response_id,
            user_id=user_id
        ).first()


def _delete_bounty_response_upvote(upvote):
    """
    Deletes a user's upvote of a bounty response to database by id
    """
    response_id = upvote.bounty_response_id
    db.session.delete(upvote)
    response = BountyResponse.query.get(response_id)
    response.upvotes -= 1
    db.session.add(response)
    db.session.commit()


def _save_bounty_response_upvote(response_id, user_id):
    """
    Saves a user's upvote of a response to database
    """
    upvote = BountyResponseLike(
        bounty_response_id=response_id,
        user_id=user_id
    )
    db.session.add(upvote)
    response = BountyResponse.query.get(response_id)
    response.upvotes += 1
    db.session.add(response)
    db.session.commit()


def _save_feedback(description):
    feedback = Feedback(
        feedback_code=shortuuid.uuid(),
        description=description,
        user_id=_get_current_user_id()
    )
    db.session.add(feedback)
    db.session.commit()


def _save_support_ticket(description):
    ticket = SupportTicket(
        ticket_code=shortuuid.uuid(),
        description=description,
        user_id=g.user.id
    )
    db.session.add(ticket)
    db.session.commit()
    return ticket.ticket_code


def _get_threads(limit=10, offset=0):
    """
    Returns the current threads in the database
    """
    followee_ids = _get_followed_users(_get_current_user_id())
    posts = (
        Post.query.filter(
            Post.author_id.in_(followee_ids),
            Post.draft!=True
        )
        .order_by(Post.created.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )
    return posts, followee_ids


def _search_users(search_str, limit=20, offset=0):
    search_query = (
        User.query.filter(User.username.ilike(f"%{search_str}%"))
        .limit(limit)
        .offset(offset)
    )
    final_results = []
    for user in search_query:
        final_results.append(user)
    return final_results


def _search_posts(search_str, limit=20, offset=0):
    search_query = (
        Post.query.filter(
            Post.title.ilike(f"%{search_str}%"),
            Post.draft!=True
        )
        .limit(limit)
        .offset(offset)
    )
    followee_ids = _get_followed_users(_get_current_user_id())
    final_results = []
    for post in search_query:
        final_results.append(post)
    return final_results, followee_ids


def _search_bounties(search_str, limit=25, offset=0):
    search_query = (
        Bounty.query.filter(Bounty.title.ilike(f"%{search_str}%"))
        .limit(limit)
        .offset(offset)
    )
    final_results = []
    for bounty in search_query:
        final_results.append(bounty)
    return final_results


def _get_received_user_payment_history(user_id=None, limit=20, offset=0):
    if not user_id:
        user_id = _get_current_user_id()
    payment_query = (
        Payment.query.filter_by(receiving_user_id=user_id)
        .order_by(Payment.created.desc())
        .limit(limit)
        .offset(offset)
    )
    received_payments = []
    for payment in payment_query:
        received_payments.append(payment)
    return received_payments


def _get_sent_user_payment_history(user_id=None, limit=20, offset=0):
    if not user_id:
        user_id = _get_current_user_id()
    payment_query = (
        Payment.query.filter_by(sending_user_id=user_id)
        .order_by(Payment.created.desc())
        .limit(limit)
        .offset(offset)
    )
    sent_payments = []
    for payment in payment_query:
        sent_payments.append(payment)
    return sent_payments


def _get_users(limit=15, offset=0):
    user_query = User.query.limit(limit).all()
    users = []
    for user in user_query:
        users.append(user)
    return users


def _get_notifications(user_id, limit=20, offset=0):
    notifications = (
        Notification.query.filter_by(to_user_id=user_id)
        .order_by(Notification.created.desc())
        .limit(limit)
        .offset(offset)
        .all()
    )
    return notifications


def _get_max_withdrawal_amount():
    """
    Returns the max withdrawal amount defined in settings
    """
    try:
        return Settings.query.first().maximum_withdrawal_amount
    except Exception:
        return 1000


def _save_blog(title, body, processed_body, user_id, sticky, upload, filename):
    """
    Writes a blog to the database
    """
    slug = slugify(title)
    blog = Blog(
        blog_code=shortuuid.uuid(),
        title=title,
        body=body,
        processed_body=processed_body,
        author_id=user_id,
        sticky=False,
        upload=upload,
        unique_filename=filename,
        slug=slug
    )
    db.session.add(blog)
    db.session.commit()
    return blog.slug


def _save_comment(user_id, post_id, comment_id, body, upload, filename, tenor_gif=False):
    """
    Writes a comment to the database
    """
    comment = Comment(
        comment_code=shortuuid.uuid(),
        user_id=user_id,
        post_id=post_id,
        comment_id=comment_id,
        body=body,
        upload=upload,
        unique_filename=filename,
        tenor_gif=tenor_gif
    )
    db.session.add(comment)
    db.session.commit()
    return comment


def _tag_user_in_post(value):
    """
    Check text for user tags or hash tags
    """
    user_regex = re.compile("^\@.+$")
    words = value.split()  # split words
    replacement_map = {}
    for word in words:
        match = user_regex.match(word)
        if match:
            replacement_map[word[1:]] = word

    # now check if any of the possible usernames are
    tagged_usernames = list(replacement_map.keys())
    users = User.query.filter(User.username.in_(tagged_usernames)).all()
    for user in users:
        replacement_map[user.username] = base_replace_str.format(user.username)

    rep = dict((re.escape(k), v) for k, v in replacement_map.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], value)


def _notify_user_they_have_been_tagged_in_a_post(value, post):
    """
    This checks text for any users and then styles them with html and link 
    to profile
    """
    user_regex = re.compile("^\@.+$")
    words = value.split()  # split words
    replacement_map = {}
    for word in words:
        match = user_regex.match(word)
        if match:
            replacement_map[word] = word

    # now check if any of the possible usernames are 
    tagged_usernames = [u[1:] for u in list(replacement_map.keys())]
    users = User.query.filter(User.username.in_(tagged_usernames)).all()
    sendgrid_alert_tagged_users(post, users)
    content = f"{post.user.username} tagged you in a post"
    for user in users:
        _save_notification(
            user.id,
            post.author_id,
            content,
            post_id=post.id
        )


def _save_post(title, body, processed_body, user_id, upload, filename, tenor_gif=False, nsfw=False, draft=False):
    """
    Writes a meme to the database
    :return: the inserted post
    """
    post = Post(
        title=title,
        post_code=shortuuid.uuid(),
        body=body,
        processed_body=processed_body,
        author_id=user_id,
        upload=upload,
        unique_filename=filename,
        nsfw=nsfw,
        meme=True,
        article=False,
        draft=draft,
        tenor_gif=tenor_gif
    )
    db.session.add(post)
    _notify_user_they_have_been_tagged_in_a_post(title, post)
    db.session.commit()
    return post


def _determine_read_time(word_count):
    """
    Returns a `reading time` for a given word count based on average 
    reading speed of an adult - roughly 265 WPM but slightly lower cuz I read
    slow (250 WPM)
    """
    words_per_minute = 250
    reading_time = int(word_count / words_per_minute)
    if reading_time < 1:
        reading_time = 1
    return reading_time


def _save_article(title, body, processed_body, user_id, draft=False):
    """
    Writes a article to the database
    :return: the inserted post
    """
    try:
        word_count = len(body.split())
    except Exception:  # just in case
        word_count = 0
    reading_time = _determine_read_time(word_count)
    post = Post(
        title=title,
        post_code=shortuuid.uuid(),
        body=body,
        processed_body=processed_body,
        author_id=user_id,
        nsfw=False,
        meme=False,
        article=True,
        reading_time=reading_time,
        draft=draft
    )
    db.session.add(post)
    db.session.commit()
    return post


def _save_node_uri(post_id, node_uri):
    """
    Writes a node uri related to a post to database
    """
    post_node = PostNode(post_id=post_id, node_uri=node_uri)
    db.session.add(post_node)
    db.session.commit()


def _save_post_like(post_id, user_id):
    """
    Saves a user's like of a post to database
    """
    like = PostLike(post_id=post_id, user_id=user_id)
    db.session.add(like)
    db.session.commit()


def _save_bookmark(user_id, post_id=None, bounty_id=None):
    """
    Saves a user's bookmark
    """
    bookmark = Bookmark(user_id=user_id, post_id=post_id, bounty_id=bounty_id)
    db.session.add(bookmark)
    db.session.commit()


def _save_comment_like(comment_id, user_id):
    """
    Saves a user's like of a post to database
    """
    like = CommentLike(comment_id=comment_id, user_id=user_id)
    db.session.add(like)
    db.session.commit()


def _get_followed_users(user_id):
    """
    Retrieves all of the user_ids that the given user is following
    """
    query = Followers.query.filter_by(follower_id=user_id)
    followers = []
    for row in query:
        followers.append(row.followee_id)
    return followers


def _get_follow(follower_id, followee_id):
    """
    Get a follow relationship
    """
    follow = Followers.query.filter_by(
        follower_id=follower_id, followee_id=followee_id
    ).first()
    if follow:
        return True
    return False


def _delete_follow(follower_id, followee_id):
    """
    Destroy a follow relationship
    """
    follow = Followers.query.filter_by(
        follower_id=follower_id, followee_id=followee_id
    ).first()
    if follow:
        db.session.delete(follow)
        db.session.commit()



def _save_follow(follower_id, followee_id):
    """
    Save a follow relationship
    """
    follow = Followers(
        follower_id=follower_id,
        followee_id=followee_id
    )
    db.session.add(follow)
    db.session.commit()


def _update_user_password(user_id, password):
    """
    Updates a user's password
    """
    user = User.query.get(user_id)
    user.set_password(password)
    db.session.add(user)
    db.session.commit()


def _update_user_balance(amount, ln_invoice, receiving_user_id):
    """
    Updates a user balance
    """
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if amount > maximum_withdrawal_amount:
        sendgrid_send_weird_user_balance_update('Large Withdrawal Attempted')
    user_balance = UserBalance.query.filter_by(
        user_id=receiving_user_id
    ).first()
    ln_invoice_already_used = (
        UserBalanceEntry.query.filter_by(ln_invoice=ln_invoice).first()
    )
    if ln_invoice_already_used:
        return False
    if user_balance:
        action = 'DEBIT' if amount > 0 else 'CREDIT'
        new_entry = UserBalanceEntry(
            user_balance_id=user_balance.id,
            before_balance=user_balance.balance,
            action_type=action,
            ln_invoice=ln_invoice,
            amount=amount,
            updating_user_id=_get_current_user_id()
        )
        user_balance.balance += amount
        db.session.add(new_entry)
        db.session.add(user_balance)
        db.session.commit()
        return True
    return False

def _has_invoice_been_paid(ln_invoice):
    """
    Returns True if an invoice has been completed
    """
    payment = Payment.query.filter_by(ln_invoice=ln_invoice).first()
    if payment and payment.status == "completed":
        return True
    return False


def _get_payment_info_from_invoice(ln_invoice):
    """
    Returns the author of the post tipped by an invoice
    """
    payment = Payment.query.filter_by(ln_invoice=ln_invoice).first()
    return payment


def _payment_complete(ln_invoice):
    """
    Updates the payment to complete
    """
    payment = Payment.query.filter_by(ln_invoice=ln_invoice).first()
    if payment:
        payment.status = "completed"
        db.session.add(payment)
        db.session.commit()


def _get_lightning_app_price(name):
    """
    Find a lightning app configuration by name and return the configured price
    """
    app = LightningAppConfiguration.query.filter_by(
        application_name=name
    ).first()
    if app:
        return app.price_for_function
    return 250  # default - won't be reached if properly configured


def _get_post_user_id(post_id):
    """
    Return the author's user_id of a post
    """
    return Post.query.get(post_id).author_id


def _get_comment_user_id(comment_id):
    """
    Return the author's user_id of a post
    """
    return Comment.query.get(comment_id).user_id


def _get_post_id_from_post_code(post_code):
    try:
        return Post.query.filter_by(post_code=post_code).first().id
    except AttributeError:
        return None


def _get_comment_id_from_comment_code(comment_code):
    try:
        return Comment.query.filter_by(comment_code=comment_code).first().id
    except AttributeError:
        return None


def _get_bounty_from_bounty_code(bounty_code):
    return Bounty.query.filter_by(bounty_code=bounty_code).first()

def _get_bounty(bounty_code):
    return Bounty.query.filter_by(bounty_code=bounty_code).first()


def _get_bounty_user_id(bounty_id):
    """
    Return the author's user_id of a bounty
    """
    return Bounty.query.get(bounty_id).user_id


def _get_bounty_response_user_id(response_id):
    """
    Return the author's user_id of a bounty response
    """
    return BountyResponse.query.get(response_id).user_id


def _get_bounty_id_by_response_code(response_code):
    response = BountyResponse.query.filter_by(
        bounty_response_code=response_code
    ).first()
    return Bounty.query.get(response.bounty_id).bounty_code


def _get_junkbite_user_id():
    """
    Return junkbite user - company
    """
    user = User.query.filter_by(username="junkbite").first()
    return user.id


def _increase_bounty_total_sats(bounty_id, amount):
    bounty = Bounty.query.get(bounty_id)
    bounty.total_sats = bounty.total_sats + amount
    db.session.add(bounty)
    db.session.commit()


def _preprocess_transaction(payment: Payment):
    """
    Take a fee from the transaction before crediting the user
    Target a 5% fee on every TX but round it to avoid decimals
    - under 10 sats: No Fee
    - 10 to 39 sats: 1 sat fee
    - 40 tp 59 sats: 2 sats fee
    - 60 - 79 sats: 3 sats fee
    - 80 - 99 sats: 4 sats fee
    - 100 - 119 sats: 5 sats fee
    """
    fee = 0.05
    junkbite_cut = int(payment.amount * fee)
    # create payment to junkbite
    new_fee = PaymentProcessingFee(
        amount=junkbite_cut,
        payment_id=payment.id
    )
    payment.fee = new_fee
    db.session.add_all([new_fee, payment])


def _save_bounty_payout(bounty_code, amount, ln_invoice, bounty_id, junkbite_app, payment_type, receiving_user_id, sending_user_id):
    """
    Creates an entry in the payment table to record a user winning a bounty,
    this user 'receives' the payment from the bounty creator although the
    actual sats are allocated to them by junkbite

    So on bounty create a payment is sent by bounty creator to junkbite to create
    on bounty award a payment is sent by junkbite to bounty winner.

    However, this will show the payment being received by the winner from the 
    creator for simplicity's sake and for the users to understand where the 
    sats came from.

    _save_bounty_payout(
        amount=amount,
        ln_invoice=ln_invoice,
        bounty_id=bounty_id,
        junkbite_app="bounty",
        payment_type="bounty winner",
        receiving_user_id=user_id,
        sending_user_id=bounty_creator_user_id
    )
    """
    # Grab fee from original bounty payment
    bounty = Bounty.query.filter_by(bounty_code=bounty_code).first()
    original_payment = Payment.query.filter_by(bounty_id=bounty.id).first()
    original_fee = original_payment.fee.amount
    amount = amount - original_fee
    was_balance_updated = _update_user_balance(
        amount, ln_invoice, receiving_user_id=receiving_user_id
    )
    if was_balance_updated:
        # remove ln invoice as its an internal credit
        payment = Payment(
            payment_code=shortuuid.uuid(),
            amount=amount,
            bounty_id=bounty_id,
            junkbite_app=junkbite_app,
            payment_type=payment_type,
            receiving_user_id=receiving_user_id,
            sending_user_id=sending_user_id,
            status="completed"
        )
        db.session.add(payment)
        db.session.commit()
    return was_balance_updated


def _save_bounty_creation_payment(amount, ln_invoice, bounty_id, junkbite_app, payment_type):
    """
    Saves the bounty creation payment - > user sends sats to junkbite. Junkbite 
    creates bounty. later on payment, junkbite sends sats to winning user,
    but the transaction looks like it went from creator to winner (ux better i guess)
    See: _save_bounty_payout function description for more
    """
    payment = Payment.query.filter_by(
        amount=amount,
        ln_invoice=ln_invoice,
        payment_type="bounty creation",
        junkbite_app="bounty",
    ).first()
    payment.bounty_id = bounty_id
    payment.status = "completed"
    db.session.add(payment)
    _preprocess_transaction(payment)
    db.session.commit()


def _save_payment(amount, ln_invoice, junkbite_app, post_id=None, bounty_id=None, response_id=None, comment_id=None, payment_type="None"):
    """
    Stores invoice / amount paid to a post in database, updates user balance
    :param amount: required - amount of sats
    :param invoice_id: required - ln payment invoice
    :param junkbite_app: required - must be one of the following:
    [social,bounty,junkbite-lightning-app] until more are added
    if junkbite_app is random or twitter then we do not need an optional id
    of where the payment went, receiving user is junkbite in this case.

    if junkbite_app is junkbite [social media app] or bounty [q & a app] then
    we need to populate where that went either post id for junkbite or
    [bounty_id / response_id] for bounty app

    This covers the different areas in which a user can be sent sats
    """
    payment = Payment(
        payment_code=shortuuid.uuid(),
        amount=amount,
        ln_invoice=ln_invoice,
        junkbite_app=junkbite_app,
        post_id=post_id,
        bounty_id=bounty_id,
        bounty_response_id=response_id,
        comment_id=comment_id,
        payment_type=payment_type,
    )
    payment.sending_user_id = _get_current_user_id()
    if post_id:
        payment.receiving_user_id = _get_post_user_id(post_id)
    elif comment_id:
        payment.receiving_user_id = _get_comment_user_id(comment_id)
    elif bounty_id:
        payment.receiving_user_id = _get_bounty_user_id(bounty_id)
    elif response_id:
        payment.receiving_user_id = _get_bounty_response_user_id(response_id)
    else:
        payment.receiving_user_id = _get_junkbite_user_id()
    db.session.add(payment)
    _preprocess_transaction(payment)
    db.session.commit()


def _set_bounty_winner_and_close(bounty_id, winner_response_id):
    """
    Update bounty and bounty response on closing of bounty
    """
    bounty = Bounty.query.get(bounty_id)
    bounty.winner_response_id = winner_response_id
    bounty.status = "closed"
    db.session.add(bounty)
    response = BountyResponse.query.get(winner_response_id)
    response.bounty_winner = True
    db.session.add(response)
    db.session.commit()


def _get_bounty_winner_info(bounty_id):
    """
    Returns the bounty winner's user id
    """
    bounty = Bounty.query.get(bounty_id)
    wr = BountyResponse.query.get(bounty.winner_response_id)
    return bounty.total_sats, bounty.initial_ln_invoice, wr.user_id


def _save_bounty_response(bounty_id, user_id, body, processed_body):
    """
    Writes a bounty response to the database
    """
    response = BountyResponse(
        bounty_response_code=shortuuid.uuid(),
        bounty_id=bounty_id,
        user_id=user_id,
        body=body,
        processed_body=processed_body
    )
    db.session.add(response)
    db.session.commit()


def _save_bounty(title, body, processed_body, user_id, upload, filename, sats, invoice, status="open"):
    """
    Writes a bounty to the database
    :return: the inserted bounty's database id
    """
    bounty = Bounty(
        bounty_code=shortuuid.uuid(),
        title=title,
        body=body,
        processed_body=processed_body,
        user_id=user_id,
        upload=upload,
        unique_filename=filename,
        initial_sats=sats,
        initial_ln_invoice=invoice,
        total_sats=sats,
        status=status
    )
    db.session.add(bounty)
    db.session.commit()
    return bounty


def _save_user_profile(user_id, email=None, link=None, bio=None, unique_filename=None, email_opt_out=False):
    """
    Writes profile updates for a specific user to database
    """
    user = User.query.get(user_id)
    if email and email != user.email:  # if not change ignore
        user.email = email
        user.email_verified = False
    user.link = link
    user.bio = bio
    if unique_filename:
        user.unique_filename = unique_filename
    user.email_opt_out = email_opt_out  # will always have True or False val
    db.session.add(user)
    db.session.commit()


def _mark_notifications_as_read(ids):
    """
    Marks notifications in database as read
    """
    notification_query = Notification.query.filter(Notification.id.in_(ids))
    for n in notification_query:
        n.opened = True
        db.session.add(n)
    db.session.commit()


def _mark_unread_notifications_as_read(user_id):
    """
    Marks notifications in database as read
    """
    notification_query = Notification.query.filter_by(
        to_user_id=user_id,
        opened=False
    )
    for n in notification_query:
        n.opened = True
        db.session.add(n)
    db.session.commit()


def _mark_notification_as_read(code):
    """
    Marks notifications in database as read
    """
    notification = Notification.query.filter_by(notification_code=code).first()
    notification.opened = True
    db.session.add(notification)
    db.session.commit()


def _save_notification(to_user_id, from_user_id, content, post_id=None, bounty_id=None, bounty_response_id=None, comment_id=None):
    """
    Writes a 'notification' to the database - tells a user something
    """
    notification = Notification(
        notification_code=shortuuid.uuid(),
        to_user_id=to_user_id,
        from_user_id=from_user_id,
        content=content,
        post_id=post_id,
        bounty_id=bounty_id,
        bounty_response_id=bounty_response_id,
        comment_id=comment_id
    )
    db.session.add(notification)
    db.session.commit()


def _save_withdrawal(user_id, ln_invoice, node_uri, amount):
    """
    Stores a withdrawal
    """
    withdrawal = Withdrawal(
        withdrawal_code=shortuuid.uuid(),
        user_id=user_id,
        ln_invoice=ln_invoice,
        node_uri=node_uri,
        amount=amount,
        status="requested"
    )
    db.session.add(withdrawal)
    db.session.commit()
    return withdrawal


def _update_advertisement(advertisement_id, post_id):
    """
    Updates an ad with the newly created post
    """
    ad = Advertisement.query.get(advertisement_id)
    ad.post_id = post_id
    db.session.add(ad)
    db.session.commit()
