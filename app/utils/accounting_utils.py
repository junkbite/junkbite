from datetime import datetime, timedelta
from app.models import (
    Payment,
    UserBalance,
    UserBalanceEntry,
    Withdrawal
)


def _get_payments_last_x_hours(now=None, hours_ago=1):
    """
    Retreive all payments from previous X number of hours
    """
    if not now:
        now = datetime.now()
    x_hours_ago = now - timedelta(hours=hours_ago)
    return Payment.query.filter(
        Payment.created >= x_hours_ago,
        Payment.status == "completed",
        Payment.ln_invoice!=None
    ).all()


def _get_balance_entries_associated_to_payments(payments):
    """
    Grab all user balance entries that are associated to the lightning invoice
    of the found payments
    """
    ln_invoices = [payment.ln_invoice for payment in payments]
    return UserBalanceEntry.query.filter(
        UserBalanceEntry.ln_invoice.in_(ln_invoices)
    ).all()


def _get_balance_entries_last_x_hours(now, hours_ago=1):
    """
    Retreive all balance entries from previous X number of hours

    IGNORES ACCOUNT CREATION
    """
    if not now:
        now = datetime.now()
    x_hours_ago = now - timedelta(hours=hours_ago)
    return UserBalanceEntry.query.filter(
        UserBalanceEntry.timestamp >= x_hours_ago,
        UserBalanceEntry.action_type != "Account Creation"
    ).all()


def _accounting_attempt():
    now = datetime.now()
    twenty_four_hours = now - timedelta(hours=24)
    payments_last_24 = Payment.query.filter(
        Payment.status=="completed",
        Payment.created > twenty_four_hours
    )
    entries = UserBalanceEntry.query.filter(
        UserBalanceEntry.timestamp > twenty_four_hours,
        UserBalanceEntry.action_type == "DEBIT"
    )
    total_payment_amounts = 0
    balance_entry_amounts = 0
    invoice_data = {}
    for p in payments_last_24:
        invoice_data[p.ln_invoice] = {
            'payment': str(p),
            'balance_entry': None
        }
        total_payment_amounts += p.amount
        print(p)
    for e in entries:
        if e.ln_invoice not in invoice_data:
            invoice_data[e.ln_invoice] = {}
        invoice_data[e.ln_invoice]['balance_entry'] = e
        balance_entry_amounts += e.amount
    assert balance_entry_amounts == total_payment_amounts
    print(balance_entry_amounts == total_payment_amounts)
