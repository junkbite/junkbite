import os
import re
import uuid
from datetime import datetime, timedelta
from decimal import Decimal
from flask import g, current_app
from pytz import timezone
from random import randint
from werkzeug.utils import secure_filename
from werkzeug.exceptions import abort
from config import Config


def _default_limit():
    return current_app.config.get("DEFAULT_LIMIT")


def _default_offset():
    return current_app.config.get("DEFAULT_OFFSET")


def _allowed_file(filename, stream):
    """Check if file is malicious and if extension is allowed."""
    file_ext = filename.rsplit('.', 1)[1].lower()
    if '.' not in filename:
        return False
    if file_ext not in Config.ALLOWED_EXTENSIONS:
        return False
    return True


def _build_lists_of_stale_objects(posts, keep_x_posts=20):
    """
    Build a list of stale posts and a list of stale image filenames.
    :param posts: list of posts - oldest first
    :param keep_x_posts: only delete posts if there is more than this
    :return <list>, <list>: stale_posts, stale_images
    """
    if len(posts) < keep_x_posts:
        print(f'Not deleting posts, there are less than {keep_x_posts}')
        return [], []
    stale_posts = []
    stale_images = []
    for post in posts:
        # keep_x_posts - do not delete more than we want to
        if len(posts) - len(stale_posts) <= keep_x_posts:
            break 
        if _is_stale(post):
            stale_posts.append(post)
            if post['unique_filename'] != "":
                stale_images.append(post['unique_filename'])
    return stale_posts, stale_images


def _create_unique_filename(filename):
    """
    Generate a unique_filename for an uploaded file

    :return <str>: unique_filename
    """
    ignore_me, file_extension = os.path.splitext(filename)
    return str(uuid.uuid4()) + file_extension


def _create_grid(posts, six_rows=False):
    """
    Organize a list of posts into a grid of posts, equally divided into 3
    columns for explore page and 6 columns for images. This makes it easy to
    display using Jinja2 in html.

    grid:
        [
            [x, y, z, a, ...],
            [b, c, d, e, ...],
            [r, q, s, j, ...],
        ]

    :param singles: Place a single item in each row
    :param six_rows: set to true for 'images' endpoint (creates 6 rows)
    :return <list>: grid
    """
    grid = []
    row = []
    if not six_rows:
        if len(posts) > 3:
            n = round(len(posts)/3)
            one = posts[:n]
            two = posts[n:n*2]
            three = posts[n*2:]
            grid = [three, two, one]
        else:
            for post in posts:
                row.append(post)
                grid.append(row)
                row = []
    else:
        if len(posts) > 5:
            n = round(len(posts)/6)
            one = posts[:n]
            two = posts[n:n*2]
            three = posts[n*2:n*3]
            four = posts[n*3:n*4]
            five = posts[n*4:n*5]
            six = posts[n*5:]
            grid = [six, five, four, three, two, one]
        else:
            for post in posts:
                row.append(post)
                grid.append(row)
                row = []
    return grid


def _datetime_to_humantime(dt):
    """
    Convert a datetime to 'how long since it has been created'

    ex: 7 Hours ago, 4 minutes ago, 10 days ago, etc

    :param <datetime: dt - utc timezone
    :return <str>: human readable time since
    """
    difference = datetime.now() - dt
    hours = int(difference.total_seconds()//3600)
    if hours < 1:
        return str(int(difference.total_seconds()//60)) + " minutes ago"
    elif hours < 24:
        return str(hours) + " hours ago"
    elif int(hours/24) == 1:
        return "1 day ago"
    else:
        return str(int(hours//24)) + " days ago"


def _file_upload(file, filename=None):
    """
    Create a unique filename and save it to UPLOAD_FOLDER (static/images)

    :param file: from request.files['file']
    :return: filename <str> and unique_filename <str>
    """
    if filename is None:
        filename = secure_filename(file.filename)
    unique_filename = _create_unique_filename(filename)
    file.save(
        os.path.join(
            current_app.config['UPLOAD_FOLDER'],
            unique_filename
        )
    )
    return filename, unique_filename


def _get_age(dt):
    """
    Calculate the age in hours of a datetime

    Note: these datetimes will be UTC, no need to convert because the
    difference will be the same as long as they are the same timezone

    :param <datetime, tzinfo=US/Eastern>: dt - timezone aware datetime (EST)
    :return <int>: age
    """
    delta = datetime.now(timezone('US/Eastern')) - dt
    return int(delta.total_seconds()//3600)


def _is_stale(post):
    """
    Determine if a post is considerd stale

    stale = any post > 48 hours old

    :return: boolean
    """
    if _get_age(post.created) > 48:
        return True
    return False


def _process_request_file(request):
    """
    Files are uploaded the same way in multiple places, this function handles
    checking, processing, uploading and returning file upload
    """
    if 'file' in request.files and request.files['file'].filename != '':
        file = request.files['file']
        if file.filename == '':
            upload = ''
        if file and _allowed_file(file.filename, file.stream):
            upload, unique_filename = _file_upload(file)
            return True, upload, unique_filename
        else:
            return False, None, None
    return True, None, None


def _api_process_request_file(filename, file):
    """
    Files are uploaded the same way in multiple places, this function handles
    checking, processing, uploading and returning file upload
    """
    if filename == '':
        upload = ''
    if file and _allowed_file(filename, file.stream):
        upload, unique_filename = _file_upload(file, filename)
        return True, upload, unique_filename
    else:
        return False, None, None
    return True, None, None


def _remove_file_from_image_directory(file):
    """
    Remove a specified file from the static images directory

    No longer needed.
    """
    if os.path.isfile(file):
        os.remove(os.path.join(current_app.config['UPLOAD_FOLDER'], file))
        print('removed {}'.format(file))


def _remove_posts_without_image(posts):
    """
    Remove any row where unique_filename is <deleted> or "";

    :param posts: Sqlite3.Row (id, title, unique_filename, username)
    :return <list>: posts containing images
    """
    image_posts = []
    for post in posts:
        if post['unique_filename'] != "<deleted>" and \
           post['unique_filename'] != "":
            image_posts.append(post)
    return image_posts


def _sats_to_bitcoin(sats):
    sat_constant = 0.00000001
    return (Decimal(sats) * Decimal(sat_constant)).quantize(Decimal('1e-8'))


def _bitcoin_to_sats(bitcoin):
    """
    converts amount in bitcoin to amount in sats
    example: 0.00000100 to 100
    :param bitcoin: str - ex: "0.00000100"
    """
    sat_constant = Decimal(0.00000001)
    coin = Decimal(bitcoin)
    return int(round(coin / sat_constant))


def _shuffle(posts):
    """
    Randomize the order of posts using the Fisher-Yates shuffle algorithm.

    :param post: <list> of posts (or anything)
    :return <list>: posts
    """
    n = len(posts)
    for i in range(n - 1, 0, -1):
        j = randint(0, i)
        if i == j:
            continue
        posts[i], posts[j] = posts[j], posts[i]
    return posts


def _sqliterow_to_dict(sqlite_row, comment=False, blog=False, response=False):
    """
    Convert and Sqlite3 row into a dict so the contents can be edited in place.

    Make this more dynamic later (give any row but now its just posts)

    :param sqlite_row: a row of data from a sqlite table
    :return <dict>: row
    """
    if response:
        response = {}
        response['id'] = sqlite_row['id']
        response['user_id'] = sqlite_row['user_id']
        response['username'] = sqlite_row['username']
        response['created'] = sqlite_row['created']
        response['processed_body'] = sqlite_row['processed_body']
        response['unique_filename'] = sqlite_row['unique_filename']
        response['upvotes'] = sqlite_row['upvotes']
        response['bounty_winner'] = sqlite_row['bounty_winner']
        return response

    if comment:
        comment = {}
        comment['id'] = sqlite_row['id']
        comment['author_id'] = sqlite_row['author_id']
        comment['body'] = sqlite_row['body']
        comment['created'] = sqlite_row['created']
        comment['processed_body'] = sqlite_row['processed_body']
        comment['username'] = sqlite_row['username']
        comment['upload'] = sqlite_row['upload']
        comment['unique_filename'] = sqlite_row['unique_filename']
        return comment

    if blog:
        blog = {}
        blog['id'] = sqlite_row['id']
        blog['title'] = sqlite_row['title']
        blog['body'] = sqlite_row['body']
        blog['processed_body'] = sqlite_row['processed_body']
        blog['upload'] = sqlite_row['upload']
        blog['unique_filename'] = sqlite_row['unique_filename']
        blog['created'] = sqlite_row['created']
        blog['author_id'] = sqlite_row['author_id']
        blog['username'] = sqlite_row['username']
        blog['nsfw'] = sqlite_row['nsfw']
        return blog

    post = {}
    post['id'] = sqlite_row['id']
    post['title'] = sqlite_row['title']
    post['body'] = sqlite_row['body']
    post['processed_body'] = sqlite_row['processed_body']
    post['upload'] = sqlite_row['upload']
    post['unique_filename'] = sqlite_row['unique_filename']
    post['created'] = sqlite_row['created']
    post['author_id'] = sqlite_row['author_id']
    post['sticky'] = sqlite_row['sticky']
    post['username'] = sqlite_row['username']
    post['nsfw'] = sqlite_row['nsfw']
    post['meme'] = sqlite_row['meme']
    return post
