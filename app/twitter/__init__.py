from flask import Blueprint

bp = Blueprint('twitter', __name__, url_prefix='/twitter')

from app.twitter import routes
