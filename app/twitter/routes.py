from app.twitter import bp
from flask import (
    render_template,
    redirect,
    url_for,
    request,
    flash,
    current_app
)
from app.utils.twitter_utils import (
    send_tweet,
    send_media_tweet,
    update_twitter_image,
    update_twitter_background_image
)
from app.utils.meta_utils import _process_request_file


@bp.route('/compose', methods=("POST",))
def compose():
    """
    View function to return a form where a user can fill out a tweet
    and a invoice to be paid to send the tweet
    """
    if request.method == 'POST':
        # Create the tweet and send it
        media_tweet = False
        tweet_content = request.form.get("title")
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('twitter.compose'))
        if upload is not None:
            media_tweet = True
        if media_tweet:
            response = send_media_tweet(unique_filename, tweet_content)
        else:
            response = send_tweet(tweet_content)
        if response == "Success":
            flash("Tweet sent!")
        else:
            flash(response)
        return redirect(url_for('bombadillo.lightning'))
    return render_template('twitter/compose.html')


@bp.route('/update_profile_picture', methods=("POST",))
def update_profile_picture():
    """
    View function to return a form where a user can upload a picture to update
    the profile picture of the twitter account
    """
    if request.method == 'POST':
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('bombadillo.lightning'))
        response = update_twitter_image(unique_filename)
        if response is None:
            flash("Profile picture updated!")
        else:
            flash(response)
        return redirect(url_for('bombadillo.lightning'))
    return render_template('twitter/update_profile_image.html')


@bp.route('/update_background_picture', methods=("POST",))
def update_background_picture():
    """
    View function to return a form where a user can upload a picture to update
    the background picture of the twitter account
    """
    if request.method == 'POST':
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('bombadillo.lightning'))
        response = update_twitter_background_image(unique_filename)
        if response is None:
            flash("Banner picture updated!")
        else:
            flash(response)
        return redirect(url_for('bombadillo.lightning'))
    return render_template('twitter/update_background_image.html')
