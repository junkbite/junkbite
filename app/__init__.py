import os
import mistune
import logging
from logging.handlers import SMTPHandler, RotatingFileHandler
from flask import Flask
from flask_bootstrap import Bootstrap
from config import Config
from sendgrid import SendGridAPIClient
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


bootstrap = Bootstrap()
db = SQLAlchemy()
# compare_type will migrate when column changes type (string to integer, etc)
migrate = Migrate(compare_type=True)


def create_app(config_class=Config):
    """
    Application factory function.

    Create and configure the application.
    :return: app instance
    """
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config_class)

    # add resuable instance of mistune to use throughout the app
    app.markdown = mistune.Markdown(
        escape=True,
        hard_wrap=True,
        parse_inline_html=True,
        parse_block_html=True
    )

    # Twilio and SendGrid Configuration
    app.sg = SendGridAPIClient(app.config.get('SENDGRID_SECRET'))

    # ensure the folders exists for database, images, and invoices
    try:
        os.makedirs(app.instance_path)
        os.makedirs(app.config['UPLOAD_FOLDER'])
        os.makedirs(app.config['INVOICE_DIR'])
    except OSError:
        pass

    # Initialize Bootstrap, sqlalchemy
    bootstrap.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)

    # import blueprints
    from . import api
    app.register_blueprint(api.bp)

    from . import advertise
    app.register_blueprint(advertise.bp)

    from . import admin
    app.register_blueprint(admin.bp)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import bombadillo
    app.register_blueprint(bombadillo.bp)

    from . import bounty
    app.register_blueprint(bounty.bp)

    from . import blog
    app.register_blueprint(blog.bp)

    from . import errors
    app.register_blueprint(errors.bp)

    from . import information
    app.register_blueprint(information.bp)

    from . import twitter
    app.register_blueprint(twitter.bp)

    app.add_url_rule('/', endpoint='index')
    bombadillo.cli.init_app(app)

    # Setup Logging
    if not app.debug and not app.testing:
        if app.config['LOG_TO_STDOUT']:
            stream_handler = logging.StreamHandler()
            stream_handler.setLevel(logging.INFO)
            app.logger.addHandler(stream_handler)
        else:
            if not os.path.exists('logs'):
                os.mkdir('logs')
            file_handler = RotatingFileHandler(
                'logs/junkbite.txt',
                maxBytes=10240,
                backupCount=10
            )
            file_handler.setFormatter(
                logging.Formatter(
                    '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
                )
            )
            file_handler.setLevel(logging.INFO)
            app.logger.addHandler(file_handler)
        app.logger.setLevel(logging.INFO)
        app.logger.info('junkbite startup')

    return app
