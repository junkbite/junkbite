import os
import json
import sqlite3
from flask import (
    current_app,
    flash,
    g,
    render_template,
    jsonify,  # DEPRECATED, JUST RETURN DICTIONARIES
    request
)
from werkzeug.security import check_password_hash
from werkzeug.exceptions import abort
from app.auth.utils import login_required
from app.utils.meta_utils import (
    _api_process_request_file
)
from app.api import bp
from app.utils.db_utils import (
    _api_create_post,
    _api_get_comments,
    _api_get_users,
    _api_get_posts,
    _api_get_threads,
    _increase_bounty_total_sats,
    _payment_complete,
    _get_user_by_id,
    _get_user_by_username,
    _get_junkbite_user_id,
    _get_post_user_id,
    _get_comment_user_id,
    _get_bounty_user_id,
    _get_bounty_response_user_id,
    _get_anon_user_id,
    _get_payment_info_from_invoice,
    _update_user_balance,
    _save_notification
)
from app.email import (
    sendgrid_send_error_email_to_admin,
    sendgrid_new_zap_on_post
)


@bp.route('/docs')
def docs():
    """Instructions and examples for bombadillo api."""
    return render_template('api/docs.html')


@bp.route('/users')
def api_users():
    """Return json data of users in database."""
    users = _api_get_users()
    data = []
    for u in users:
        user = {'username': u['username'], 'created': u['created']}
        data.append(user)
    return jsonify(data)


@bp.route('/threads')
def api_threads():
    """Return json data of each post with all of its comments."""
    data = _api_get_threads()
    return jsonify(data)


@bp.route('/posts')
def api_posts():
    """Return json data of posts in database."""
    posts = _api_get_posts()
    data = []
    for post in posts:
        data.append(_sqliterow_to_dict(post))
    return jsonify(data)


@bp.route('/comments')
def api_comments():
    """Return json data of comments in database."""
    comments = _api_get_comments()
    data = []
    for c in comments:
        data.append(_sqliterow_to_dict(c, comment=True))
    return jsonify(data)


def _extract_request_info_image():
    """branch to create post with an image
    ImmutableMultiDict([('file', <FileStorage: '/Users/Joe/0junkbite-images/images/cat_sunglasses.jpg' ('image/jpg')>)])
    ImmutableMultiDict([('filename', 'cat_sunglasses.jpg'), ('data', '{"title": "Test", "body": "test"}')])
    """
    try:
        filename = request.form.get('filename')
        data = json.loads(request.form.get('data')).get('data')
        file = request.files.get('file')
        return data, filename, file
    except Exception:
        return None


def _extract_request_info_text():
    """branch to create post of just text"""
    return request.json.get('data'), None, None


def _api_determine_post_type(request):
    """Given a request, read its values to determinet the type of post"""
    if request.json is not None:
        return 'text'
    if 'file' in request.files and not request.json:
        return 'image'
    return None


@bp.route('/create-post', methods=("POST",))
def api_create_post():
    """Return json data of posts in database."""
    post_type = _api_determine_post_type(request)
    if post_type == 'image':
        data, filename, file = _extract_request_info_image()
    elif post_type == 'text':
        data, filename, file = _extract_request_info_text()
    else:
        return {'message': 'Unreadable request'}, 400
    username = request.authorization.get('username')
    password = request.authorization.get('password')
    user = _get_user_by_username(username)
    error = None
    if user is None:
        error = 'Username not registered'
        current_app.logger.info(
            f"Failed api create post attempt for user: {username}. "
            "Reason: username not registered"
        )
    elif not user.check_password(password):
        error = 'Incorrect password'
        current_app.logger.info(
            f"Failed login attempt for username: {username}. "
            "Reason: Incorrect password attempt."
        )
    if error:
        return {'message': error}, 401
    if not data or not 'title' in data:
        error = 'Data missing. title is required'
        return {'message': error}, 400
    if file:
        success, upload, unique_filename = _api_process_request_file(filename, file)
        if not success:
            error = "That filetype is unsupported"
            return {'message': error}, 400
    else:
        upload = None
        unique_filename = None
    title = data.get('title', "")
    if (title.isspace() or title.strip() == "") and unique_filename is None:
        error = "Your post is empty"
        return {'message': error}, 400
    post = {
        'user_id': user.id,
        'title': title,
        'body': "",
        'processed_body': "",
        'upload': upload,
        'unique_filename': unique_filename
    }
    _api_create_post(post)
    return {'post': post}, 201


@bp.route('/invoice-hook', methods=["POST"])
def invoice_hook():
    """
    Trusted API hook, LND watcher client hits this immediately when an invoice 
    is paid
    """
    # TODO: Add a secret key here and give it to the trusted client,
    # No one else can call this request
    if current_app.config["SECRET_KEY"] != request.json.get('secret'):
        return {'message': 'unauthorized'}
    ln_invoice = request.json.get('payment-request')
    amount = request.json.get('amount')
    # update the invoice noting that it was paid
    _payment_complete(ln_invoice)
    # Get Payment Receiving User ID
    payment = _get_payment_info_from_invoice(ln_invoice)
    if payment is None:
        error_msg = f"Payment not found for lightning invoice: {ln_invoice}"
        sendgrid_send_error_email_to_admin(error_msg)
        return {'message': 'payment failed'}
    # subtract fee from amount to update user balance
    assert payment.amount == amount
    amount = amount - payment.fee.amount
    author_id = None
    if payment.post_id:
        author_id = _get_post_user_id(payment.post_id)
    if payment.comment_id:
        author_id = _get_comment_user_id(payment.comment_id)
    if payment.bounty_id:
        author_id = _get_bounty_user_id(payment.bounty_id)
    if payment.bounty_response_id:
        author_id = _get_bounty_response_user_id(payment.bounty_response_id)
    if payment.sending_user_id is not None:
        username_of_sender = _get_user_by_id(payment.sending_user_id).username
    else:
        username_of_sender = 'anonymous'
    # If this was paid to a bounty -> DO NOT UPDATE USER BALANCE
    # This adds the the BOUNTY REWARD

    # IF AUTHOR ID is the same as the RECEIVING USER ID
    # This user tipped themselves, DO NOT UPDATE BALANCE -> this goes to junkbite

    # IF TIP IS TO THE ANON ACCOUNT -> do not update balance,
    # This also goe to junkbite

    # IF its a response to a bounty, update the user balance for this
    # this user contributed a positive comment and has been rewarded
    if (payment.post_id or payment.bounty_response_id or payment.comment_id) and author_id is not None:
        if payment.receiving_user_id != payment.sending_user_id and payment.receiving_user_id != _get_anon_user_id():
            was_balance_updated = _update_user_balance(amount, ln_invoice, payment.receiving_user_id)
            if was_balance_updated:
                content = f"{username_of_sender} zapped you {amount} sats"
                if payment.post_id:
                    sendgrid_new_zap_on_post(payment)
                    _save_notification(
                        to_user_id=author_id,
                        from_user_id=payment.sending_user_id,
                        content=content,
                        post_id=payment.post_id,
                    )
                if payment.bounty_response_id:
                    _save_notification(
                        to_user_id=author_id,
                        from_user_id=payment.sending_user_id,
                        content=content,
                        bounty_response_id=payment.bounty_response_id,
                    )
    else:
        if payment.bounty_id:
            content = f"{username_of_sender} boosted your bounty by {amount} sats"
            _save_notification(
                to_user_id=author_id,
                from_user_id=payment.sending_user_id,
                content=content,
                bounty_id=payment.bounty_id,
            )
            # need extra check here
            _increase_bounty_total_sats(payment.bounty_id, payment.amount)
        else:
            content = f"{username_of_sender} zapped junkbite {amount} sats"
            _save_notification(
                to_user_id=_get_junkbite_user_id(),
                from_user_id=payment.sending_user_id,
                content=content
            )
    # TODO: update junkbite balance here if no other is the reciever
    # Server sends client message here so client can update
    return {'message': ln_invoice}
