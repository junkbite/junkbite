import bleach
from datetime import datetime
from flask import (
    flash,
    g,
    redirect,
    render_template,
    request,
    session,
    url_for,
    current_app,
)
from app import db
from app.auth.utils import login_required
from app.utils.tenor_utils import search_tenor_gifs
from app.bombadillo import bp
from app.utils.meta_utils import (
    _process_request_file,
    _default_limit,
    _default_offset
)
from app.utils.db_utils import (
    _delete_bookmark,
    _delete_comment_like,
    _delete_comment,
    _delete_follow,
    _delete_post,
    _delete_post_like,
    _get_bookmark,
    _get_comment,
    _get_follow,
    _get_images,
    _get_post_author,
    _get_post_user_id,
    _get_post_likes,
    _get_post_zaps,
    _get_junkbite_user_id,
    _get_user,
    _get_user_posts,
    _get_user_drafts,
    _get_followed_users,
    _get_advertisement_post_ids,
    _get_advertisement_posts,
    _get_anon_user_id,
    _get_anon_user,
    _get_max_withdrawal_amount,
    _get_user_by_username,
    _get_current_user_id,
    _get_comment_like,
    _get_user_followers,
    _get_user_following,
    _get_comment_by_id,
    _get_bookmarked_posts,
    _get_zapped_posts,
    _get_liked_posts,
    _get_explore_posts,
    _get_random_post,
    _get_notifications,
    _get_post_like,
    _get_post_comments,
    _get_comment_comments,
    _get_post_id_from_post_code,
    _get_comment_id_from_comment_code,
    _get_post,
    _get_settings,
    _get_threads,
    _get_users,
    _get_sent_user_payment_history,
    _get_received_user_payment_history,
    _get_lightning_app_price,
    _get_user_by_id,
    _get_user_balance,
    _get_user_profile_picture,
    _has_invoice_been_paid,
    _get_withdrawals,
    _mark_notification_as_read,
    _mark_unread_notifications_as_read,
    _save_bookmark,
    _save_comment_like,
    _save_comment,
    _save_follow,
    _save_post,
    _save_article,
    _save_notification,
    _save_node_uri,
    _save_post_like,
    _save_payment,
    _save_user_profile,
    _save_withdrawal,
    _search_users,
    _search_posts,
    _search_bounties,
    _update_user_balance,
    _update_user_password
)
from app.email import send_email_new_reported_post
from app.email import (
    sendgrid_password_updated,
    sendgrid_new_like_on_post,
    sendgrid_new_comment_on_post,
    sendgrid_new_comment_on_comment,
    sendgrid_new_follower,
    sendgrid_notify_admin_new_post,
    sendgrid_notify_admin_new_withdrawal
)
from lndgrpc import LNDClient
from app.models import ReportedContent, Comment  # TODO move this to db_utils

BOMBADILLO_POST = 'bombadillo.post'


@bp.before_request
def update_last_active():
    if g.user:
        g.user.last_active = datetime.utcnow()
        db.session.add(g.user)
        db.session.commit()


@bp.route('/api')
def api():
    return redirect(url_for('api.docs'))


@bp.route('/save-draft', methods=("POST",))
def save_draft():
    """
    Saves a post as a draft
    """
    title = request.form.get('title')
    title = bleach.clean(title)
    success, upload, unique_filename = _process_request_file(request)
    if not success:
        flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
        return redirect(url_for('bombadillo.create'))
    if (title.isspace() or title.strip() == "") and unique_filename is None:
        flash("Not saving an empty draft")
        return redirect(url_for('bombadillo.create'))
    body = ""
    processed_body = ""
    user_id = _get_current_user_id()
    post = _save_post(
        title, body, processed_body, user_id, upload, unique_filename, draft=True
    )
    return redirect(url_for('bombadillo.edit_draft', post_code=post.post_code))


@bp.route('/edit-draft/<post_code>', methods=("GET", "POST"))
def edit_draft(post_code):
    """
    Edit a draft and publish
    """
    post = _get_post(post_code)
    if request.method == "POST":
        title = request.form.get('title')
        title = bleach.clean(title)
        success, upload, unique_filename = _process_request_file(request)
        if not success:
            flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
            return redirect(url_for('bombadillo.create'))
        if (title.isspace() or title.strip() == "") and unique_filename is None:
            flash("Not saving an empty draft")
            return redirect(url_for('bombadillo.create'))
        body = ""
        processed_body = ""
        post.title = title
        post.body = body
        post.processed_body = processed_body
        post.upload = upload
        post.unique_filename = unique_filename
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('bombadillo.edit_draft', post_code=post.post_code))
    user = post.user
    return render_template("bombadillo/edit_draft.html", post=post, user=user)


@bp.route('/create-from-draft/<post_code>', methods=("POST",))
def create_from_draft(post_code):
    """
    Creates a thread but from a pre-existing draft post -- from edit page
    """
    post = _get_post(post_code)
    if not g.user or g.user.id != post.user.id:
        flash("Unauthorized")
        return redirect(url_for("bombadillo.threads"))
    title = request.form.get('title')
    title = bleach.clean(title)
    success, upload, unique_filename = _process_request_file(request)
    if not success:
        flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
        return redirect(url_for('bombadillo.create'))
    if (title.isspace() or title.strip() == "") and unique_filename is None:
        flash("Your post cannot be empty")
        return redirect(url_for('bombadillo.create'))
    post.title = title
    post.draft = False
    if unique_filename:
        post.upload = upload
        post.unique_filename = unique_filename
    db.session.add(post)
    db.session.commit()
    return redirect(url_for(BOMBADILLO_POST, post_code=post.post_code))


@bp.route('/gif-search', methods=("POST",))
def gif_search():
    search_term = request.form.get("searchTerm")
    post_code = request.form.get("postCode")
    search_term = bleach.clean(search_term)
    gifs = search_tenor_gifs(search_term, limit=_default_limit())
    html = render_template(
        'bombadillo/tenor_gif_search.html',
        gifs=gifs,
        post_code=post_code
    )
    return {'html': html}


@bp.route('/create', methods=('GET', 'POST'))
def create():
    """
    Creates a thread, a thread is an image, title and textbox
    """
    if request.method == 'POST':
        title = request.form.get('title')
        title = bleach.clean(title)
        gif_search = request.form.get("gifSearch")
        gif = request.form.get("gif", False)
        tenor_gif = True if gif else False
        if not gif:
            success, upload, unique_filename = _process_request_file(request)
            if not success:
                flash(current_app.config['FILE_TYPE_UNSUPPORTED'])
                return redirect(url_for('bombadillo.create'))
        else:
            unique_filename = gif
            upload = gif_search
        if (title.isspace() or title.strip() == "") and unique_filename is None:
            flash("Your post cannot be empty")
            return redirect(url_for('bombadillo.create'))
        body = ""
        processed_body = ""
        user_id = _get_current_user_id()
        post = _save_post(
            title, body, processed_body, user_id, upload, unique_filename, tenor_gif=tenor_gif, draft=False
        )
        sendgrid_notify_admin_new_post(post)
        return redirect(url_for(BOMBADILLO_POST, post_code=post.post_code))
    if g.user:
        user = g.user
    else:
        user = _get_anon_user()
    return render_template('bombadillo/create.html', user=user)


@bp.route('/save-article-draft', methods=("POST",))
def save_article_draft():
    """
    Saves a post as a draft
    """
    title = request.form.get("title")
    title = bleach.clean(title)
    body = request.form.get("editorText", "")
    body = bleach.clean(body)
    if body == "<p><br></p>":
        body = ""
    # remove all empty p tags
    body = body.replace("<p><br></p>", "")
    processed_body = body
    user_id = _get_current_user_id()
    post = _save_article(title, body, processed_body, user_id, draft=True)
    return redirect(url_for('bombadillo.edit_article_draft', post_code=post.post_code))


@bp.route('/edit-article-draft/<post_code>', methods=("GET", "POST"))
def edit_article_draft(post_code):
    """
    Edit a draft and publish
    """
    post = _get_post(post_code)
    if post.draft == False:
        flash("This post is already published")
        return redirect(url_for("bombadillo.post", post_code=post.post_code))
    user = post.user
    return render_template("bombadillo/edit_article.html", post=post, user=user)


@bp.route('/publish-article-draft/<post_code>', methods=("POST",))
def publish_article_draft(post_code):
    """
    Finalizes a Post to not be a draft any longer, visible to users
    """
    post = _get_post(post_code)
    if not g.user or g.user.id != post.user.id:
        flash("Unauthorized")
        return redirect(url_for("bombadillo.threads"))
    title = request.form.get("title")
    title = bleach.clean(title)
    body = request.form.get("editorText", "")
    body = bleach.clean(body)
    if body == "<p><br></p>":
        body = ""
    post.title = title
    post.body = body
    post.processed_body = body
    post.draft = False
    db.session.add(post)
    db.session.commit()
    return redirect(url_for(BOMBADILLO_POST, post_code=post.post_code))


@bp.route('/create-article', methods=('GET', 'POST'))
def create_article():
    """
    Creates a thread, a thread is an image, title and textbox
    """
    if request.method == 'POST':
        title = request.form.get("title")
        title = bleach.clean(title)
        body = request.form.get("editorText", "")
        body = bleach.clean(body)
        if body == "<p><br></p>":
            body = ""
        # using wysiwyg editor now so no markdown processing
        processed_body = body
        user_id = _get_current_user_id()
        post = _save_article(title, body, processed_body, user_id, draft=False)
        return redirect(url_for(BOMBADILLO_POST, post_code=post.post_code))
    return render_template('bombadillo/create_article.html', user=g.user)


@bp.route('/delete/<int:post_id>', methods=('POST',))
@login_required
def delete(post_id):
    _delete_post(post_id)
    return redirect(url_for('bombadillo.threads'))


@bp.route('/delete_comment/<int:comment_id>', methods=('POST',))
@login_required
def delete_comment(comment_id):
    comment = _get_comment_by_id(comment_id)
    _delete_comment(comment_id)
    return redirect(url_for(BOMBADILLO_POST, post_id=comment['post_id']))


@bp.route('/explore', methods=("GET", "POST"))
def explore():
    """
    Displays posts as 3 columns of posts in the form of image + title.
    """
    max_withdrawal_amount = _get_max_withdrawal_amount()
    if request.method == "POST":
        filter_by = request.form.get("filter")
        scope = request.form.get("scope")
        return redirect(url_for('bombadillo.explore', f=filter_by, s=scope))
    filter_by = request.args.get("f", "sats")
    scope = request.args.get("s", "week")
    posts = _get_explore_posts(filter_by=filter_by, scope=scope, limit=_default_limit())
    print(len(posts), posts)
    user_id = _get_current_user_id()
    followee_ids = _get_followed_users(_get_current_user_id())
    selector = ["sats", "likes", "comments", "newest"]
    selector.insert(0, selector.pop(selector.index(filter_by)))
    time_options = ["today", "week", "month", "all"]
    time_options.insert(0, time_options.pop(time_options.index(scope)))
    return render_template(
        "bombadillo/explore.html",
        posts=posts,
        user_id=user_id, 
        user_following=followee_ids,
        selector=selector,
        time_options=time_options,
        max_zap=max_withdrawal_amount
    )


@bp.route('/save-bookmark', methods=["POST"])
def save_bookmark():
    """
    Saves a bookmark record to a post or bounty
    """
    post_code = request.form.get('postCode')
    bounty_id = request.form.get('bountyId')
    if not g.user:
        return {'message': 'login to bookmark'}
    # check if post has already been bookmarked
    post_id = _get_post_id_from_post_code(post_code)
    if not post:
        return {'message': 'save bookmark failed, try again later'}
    bookmark_id = _get_bookmark(g.user.id, post_id=post_id, bounty_id=bounty_id)
    if bookmark_id:
        _delete_bookmark(bookmark_id)
        return {'message': 'removed'}
    _save_bookmark(g.user.id, post_id=post_id, bounty_id=bounty_id)
    return {'message': 'bookmarked'}


@bp.route('/post-like', methods=["POST"])
def post_like():
    """
    View function for an AJAX request. sends json of a like and post id
    """
    post_code = request.form.get('postCode')
    if not g.user:
        # possibly add a way to add way for anons to like posts
        return {'message': 'login to like posts'}
    # check if post has already been liked
    post_id = _get_post_id_from_post_code(post_code)
    if not post_id:
        return {'message': 'failed, try again later'}
    like_id = _get_post_like(post_id, g.user.id)
    if like_id:
        _delete_post_like(like_id)
        return {'message': 'unliked'}
    _save_post_like(post_id, g.user.id)
    post_author_id = _get_post_author(post_id)
    sendgrid_new_like_on_post(g.user, post_id)
    content = f"{g.user.username} liked your post"
    _save_notification(post_author_id, g.user.id, content, post_id=post_id)
    return {'message': 'liked'}


@bp.route('/comment-like', methods=["POST"])
def comment_like():
    """
    View function for an AJAX request. sends json of a like and comment id
    """
    comment_code = request.form.get('commentCode')
    comment_id = _get_comment_id_from_comment_code(comment_code)
    print(comment_id)
    print(g.user.username)
    if not g.user:
        # special case, still count likes for anon users
        return {'message': 'login to like comments'}
    # check if post has already been liked
    comment_like_id = _get_comment_like(comment_id, g.user.id)
    if comment_like_id:
        _delete_comment_like(comment_like_id)
        return {'message': 'unliked'}
    _save_comment_like(comment_id, g.user.id)
    return {'message': 'liked'}


@bp.route('/follow', methods=["POST"])
def follow():
    """
    Receives AJAX request to follow a user
    -- creates a follow relationship between two users
    """
    try:
        follower_id = g.user.id
        follower_username = g.user.username
    except Exception:
        return {'message': 'login to follow'}
    followee_username = request.form.get('username')
    followee = _get_user_by_username(followee_username)
    if not followee:
        return {'message': 'User does not exist'}
    relationship_exists = _get_follow(follower_id, followee.id)
    if relationship_exists:
        _delete_follow(follower_id, followee.id)
        return {'message': 'Unfollowed'}
    else:
        _save_follow(follower_id, followee.id)
        content = f"{follower_username} followed you"
        sendgrid_new_follower(followee, g.user)
        _save_notification(followee.id, follower_id, content)
        return {'message': 'Followed'}


@bp.route('/images')
def images():
    """
    display all images currently in posts or comments.
    """
    posts = _get_images(limit=_default_limit(), offset=0)
    return render_template('bombadillo/images.html', posts=posts)


@bp.route('/more-images', methods=("POST",))
def more_images():
    """
    AJAX query here which we respond with X more images from OFFSET
    """
    offset = int(request.form.get('offset'), _default_offset())
    images = _get_images(limit=_default_limit(), offset=offset)
    # TODO IMAGES Injectable HTML
    html = render_template(
        'bombadillo/injectable_images.html',
        posts=images,
    )
    return {'offset': _default_offset() + 10, 'html': html}


@bp.route('/check-payment', methods=["POST"])
def check_payment():
    """
    AJAX request to check if payment has completed
    """
    ln_invoice = request.form.get("lnInvoice")
    paid = _has_invoice_been_paid(ln_invoice)
    if paid:
        return {'message': True}
    return {'message': False}


@bp.route('/get-invoice', methods=["POST"])
def get_invoice():
    """
    Returns a fresh payment request
    """
    post_code = request.form.get("postCode")
    comment_code = request.form.get("commentCode")
    payment_amount = int(request.form.get("amount"))
    max_withdrawal_amount = _get_max_withdrawal_amount()
    if payment_amount > max_withdrawal_amount:
        return {'message': f'Max zap is {max_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(payment_amount)
    ln_invoice = response.payment_request
    if post_code is not None:
        post_id = _get_post_id_from_post_code(post_code)
        _save_payment(
            amount=payment_amount,
            ln_invoice=ln_invoice,
            post_id=post_id,
            junkbite_app="social",
            payment_type="sat boost on post"
        )
    elif comment_code is not None:
        comment_id = _get_comment_id_from_comment_code(comment_code)
        _save_payment(
            amount=payment_amount,
            ln_invoice=ln_invoice,
            comment_id=comment_id,
            junkbite_app="social",
            payment_type="sat boost on comment"
        )
    else:
        return {'message': 'Failed to get invoice'}
    return {'message': ln_invoice}


@bp.route('/get-twitter-invoice', methods=["POST"])
def get_twitter_invoice():
    func = request.form.get("func")
    if func is None:
        flash("Request had an error")
        return redirect(url_for("bombadillo.lighting"))
    #
    amount = _get_lightning_app_price(func)
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if amount > maximum_withdrawal_amount:
        return {'message': f'Max zap is {maximum_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(amount)
    ln_invoice = response.payment_request
    _save_payment(
        amount=amount,
        ln_invoice=ln_invoice,
        junkbite_app="junkbite-lightning-app",
        payment_type="used a junkbite lightning app"
    )
    return {'message': ln_invoice}


@bp.route('/get-general-invoice', methods=["POST"])
def get_general_invoice():
    # Default to 250 if no amount is found
    amount = int(request.form.get("amount", 250))
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if amount > maximum_withdrawal_amount:
        return {'message': f'Max zap is {maximum_withdrawal_amount} sats'}
    lnd = LNDClient(
        ip_address=current_app.config["LND_RPC_IP"],
        macaroon_filepath=current_app.config['LND_MACAROON'],
        cert_filepath=current_app.config['LND_TLS_CERT'],
        admin=True
    )
    response = lnd.add_invoice(amount)
    ln_invoice = response.payment_request
    _save_payment(
        amount=amount,
        ln_invoice=ln_invoice,
        junkbite_app="junkbite-lightning-app",
        payment_type="used a junkbite lightning app"
    )
    return {'message': ln_invoice}


@bp.route('/new-comment', methods=("POST",))
def new_comment():
    """
    AJAX new comment view function - POST only
    """
    post_code = request.form.get("postCode")
    comment_code = request.form.get("commentCode")
    comment = request.form.get("comment")
    gif_search = request.form.get("gifSearch")
    gif = request.form.get("gif", False)
    tenor_gif = True if gif else False
    unique_filename = gif
    upload = gif_search
    # check user logged in
    commenter_username = g.user.username
    if g.user.id != _get_current_user_id():
        return {'message': 'Failed. Login again'}
    if (comment.isspace() or comment.strip() == "") and unique_filename is None:
        return {'message': 'Failed. Comment Empty'}
    user_id = _get_current_user_id()
    post_id = _get_post_id_from_post_code(post_code)
    comment_id = _get_comment_id_from_comment_code(comment_code)
    if not post_id:
        return {'message': 'post not found'}
    comment = bleach.clean(comment)
    comment = _save_comment(
        user_id,
        post_id,
        comment_id,
        comment,
        upload,
        unique_filename,
        tenor_gif=tenor_gif
    )
    post_author_id = _get_post_author(post_id)
    if comment_id:
        parent_comment = Comment.query.get(comment.comment_id)
        if parent_comment.user_id != comment.user_id:
            sendgrid_new_comment_on_comment(g.user, parent_comment)
            content = f"{commenter_username} replied to your comment"
            _save_notification(
                parent_comment.user_id,
                g.user.id,
                content,
                comment_id=parent_comment.id
            )
    if post_author_id != g.user.id:
        sendgrid_new_comment_on_post(g.user, post_id)
        content = f"{commenter_username} commented on your post"
        _save_notification(post_author_id, g.user.id, content, post_id=post_id)
    return {'message': 'success'}


@bp.route('/post/<post_code>', methods=('GET', 'POST'))
def post(post_code):
    """
    Generate the view post page.

    GET: This is retrieving a specific post and related comments

    :param comment_code: id of specific post
    :return render_template or redirect to bombadillo.post:
    """
    post = _get_post(post_code)
    max_withdrawal_amount = _get_max_withdrawal_amount()
    if not post:
        flash("Not Found")
        return redirect(url_for('bombadillo.threads'))
    if not g.user and post.draft:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    if g.user and post.draft and post.author_id != g.user.id:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    return render_template(
        'bombadillo/post.html',
        post=post,
        user_id=post.user.id if post.user else _get_anon_user_id(),
        comments=[c for c in post.comments if c.comment_id == None],
        max_zap=max_withdrawal_amount
    )


@bp.route('/c/<comment_code>', methods=('GET', 'POST'))
def comment(comment_code):
    """
    Generate the view comment page.

    GET: This is retrieving a specific comment and related comments

    :param comment_code: id of specific comment
    :return render_template or redirect to bombadillo.comment:
    """
    comment = _get_comment(comment_code)
    max_withdrawal_amount = _get_max_withdrawal_amount()
    if not comment:
        flash("Not Found")
        return redirect(url_for('bombadillo.threads'))
    if not g.user and comment.draft:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    if g.user and comment.draft and comment.author_id != g.user.id:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    return render_template(
        'bombadillo/post.html',
        post=comment,
        user_id=comment.user.id,
        comments=comment.comments,
        max_zap=max_withdrawal_amount
    )


@bp.route('/post/<post_code>/likes', methods=('GET',))
def post_likes(post_code):
    """
    Returns a list of users that have liked a post
    """
    post, users = _get_post_likes(post_code)
    if not post:
        flash("Not Found")
        return redirect(url_for('bombadillo.threads'))
    if not g.user and post.draft:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    if g.user and post.draft and post.author_id != g.user.id:
            flash("This post is unavailable, if you are the creator please login")
            return redirect(url_for('bombadillo.threads'))
    return render_template(
        'bombadillo/post_likes.html',
        post=post,
        users=users,
    )


@bp.route('/post/<post_code>/zaps', methods=('GET',))
def post_zaps(post_code):
    """
    Returns a list of users that have liked a post
    """
    post, users = _get_post_zaps(post_code)
    if not post:
        flash("Not Found")
        return redirect(url_for('bombadillo.threads'))
    if not g.user and post.draft:
        flash("This post is unavailable, if you are the creator please login")
        return redirect(url_for('bombadillo.threads'))
    if g.user and post.draft and post.author_id != g.user.id:
            flash("This post is unavailable, if you are the creator please login")
            return redirect(url_for('bombadillo.threads'))
    return render_template(
        'bombadillo/post_zaps.html',
        post=post,
        users=users,
    )


@bp.route('/more-comments', methods=("POST",))
def more_comments():
    post_code = request.form.get('postCode')
    offset = int(request.form.get('offset'), _default_offset())
    post_id = _get_post_id_from_post_code(post_code)
    if not post_id:
        return {'message': 'failed to load comments, try again later'}
    comments = _get_post_comments(post_id, limit=_default_limit(), offset=offset)
    html = render_template(
        'bombadillo/injectable_comments.html',
        comments=comments
    )
    return {'offset': _default_offset() + 10, 'html': html}


@bp.route('/get-nested-comments', methods=("POST",))
def get_nested_comments():
    comment_code = request.form.get('commentCode')
    comment_id = _get_comment_id_from_comment_code(comment_code)
    if not comment_id:
        return {'message': 'failed to load comments, try again later'}
    comments = _get_comment_comments(comment_id, limit=_default_limit())
    html = render_template(
        'bombadillo/injectable_comments.html',
        comments=comments
    )
    return {'html': html}


@bp.route('/lightning', methods=("GET",))
def lightning():
    """
    Landing Page for junkbite lightning apps
    """
    return render_template('bombadillo/lightning.html')


@bp.route('/random', methods=("GET", "POST"))
def random_post():
    """
    Redirect to random post on lightning invoice payment
    """
    if request.method == "POST":
        post_code = _get_random_post()
        return redirect(url_for(BOMBADILLO_POST, post_code=post_code))
    return render_template('bombadillo/random.html')


@bp.route('/report/comment/<int:comment_id>', methods=('POST',))
def report_comment(comment_id):
    """
    Report a comment that is against the rules to administration.
    adds the comment to the reported_content table
    """
    comment = _get_comment(comment_id)
    reason = request.form['reason']
    found = ReportedContent.query.filter_by(comment_id=comment_id).first()
    if found:
        if found.reporting_user_id != g.user.id:
            found.count += 1
            found.reason = reason
            db.session.add(found)
        else:
            flash('you already reported this comment.')
            return redirect(url_for(BOMBADILLO_POST, post_id=comment.post_id))
    else:
        reported = ReportedContent(
            post_id=comment.post_id,
            commnet_id=comment_id,
            reported_id=comment.author_id,
            reportor_id=g.user.id,
            reason=reason,
        )
        db.session.add(reported)
    db.session.commit()
    flash(f'Comment: {comment_id} from Post: {comment.post_id} has been reported.')
    return redirect(url_for(BOMBADILLO_POST, post_id=comment.post_id))


@bp.route('/report/post/<post_code>', methods=('POST',))
def report_post(post_code):
    """
    Report a post that is against the rules to administration.
    adds the post to the reported_content table
    """
    post = _get_post(post_code)
    reason = request.form.get('reason')
    found = ReportedContent.query.filter(
        ReportedContent.post_id==post.id, ReportedContent.comment_id==None
    ).first()
    if found:
        if found.reporting_user_id != g.user.id:
            found.count += 1
            found.reason += f", {reason}"
            db.session.add(found)
        else:
            flash('you already reported this post.')
            return redirect(url_for(BOMBADILLO_POST, post_code=post_code))
    else:
        reported = ReportedContent(
            post_id=post.id,
            comment_id=None,
            reported_user_id=post.author_id,
            reporting_user_id=g.user.id,
            reason=reason,
        )
        db.session.add(reported)
    db.session.commit()
    send_email_new_reported_post(post)
    flash(f'Post: {post_code} has been reported.')
    return redirect(url_for(BOMBADILLO_POST, post_code=post_code))


@bp.route('/landing', methods=("GET",))
def landing():
    return redirect(url_for('auth.login'))


@bp.route('/', methods=("GET",))
@bp.route('/home', methods=("GET", "POST"))
def threads():
    posts, user_following = _get_threads(10, 0)
    user_id = _get_current_user_id()
    user, balance = _get_user(user_id)
    max_withdrawal_amount = _get_max_withdrawal_amount()
    if not g.user:
        return redirect(url_for('auth.login'))
    if len(posts) == 0:  # user not following anyone
        users = _get_users(15, 0)
        return render_template(
            'bombadillo/follow_some_users.html',
            user_id=user_id,
            users=users
        )
    return render_template(
        'bombadillo/home.html',
        posts=posts,
        user=user,
        user_following=user_following,
        max_zap=max_withdrawal_amount
    )


@bp.route('/u/<username>/likes', methods=("GET",))
@login_required
def user_likes(username):
    """
    Returns posts that a given user has liked
    """
    user_id = _get_current_user_id()
    if g.user.id != user_id or g.user.username != username:
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    posts = _get_liked_posts(limit=_default_limit())
    followee_ids = _get_followed_users(_get_current_user_id())
    return render_template(
        "bombadillo/explore.html",
        posts=posts,
        user_id=user_id, 
        user_following=followee_ids
    )


@bp.route('/u/<username>/bookmarks', methods=("GET",))
@login_required
def user_bookmarks(username):
    """
    Returns posts that a user has bookmarked
    """
    user_id = _get_current_user_id()
    if g.user.id != user_id or g.user.username != username:
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    posts = _get_bookmarked_posts(limit=_default_limit())
    followee_ids = _get_followed_users(_get_current_user_id())
    return render_template(
        "bombadillo/explore.html",
        posts=posts,
        user_id=user_id, 
        user_following=followee_ids
    )


@bp.route('/u/<username>/zaps', methods=("GET",))
@login_required
def user_zaps(username):
    """
    Returns posts that a user has zapped
    """
    user_id = _get_current_user_id()
    if g.user.id != user_id or g.user.username != username:
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    posts = _get_zapped_posts(limit=_default_limit())
    return render_template(
        "bombadillo/explore.html",
        posts=posts,
        user=g.user,
    )


@bp.route('/u/<username>/followers', methods=("GET",))
@login_required
def user_followers(username):
    """
    Returns Users that follow user (username)
    """
    user, followers = _get_user_followers(username)
    return render_template(
        "bombadillo/follow_list.html",
        user=user,
        users=followers
    )



@bp.route('/u/<username>/following', methods=("GET",))
@login_required
def user_following(username):
    """
    Returns Users that user (username) is following
    """
    user, following = _get_user_following(username)
    return render_template(
        "bombadillo/follow_list.html",
        user=user,
        users=following
    )


@bp.route('/u/<username>/notifications', methods=("GET", "POST"))
@login_required
def notifications(username):
    """
    Returns notifications for a user
    """
    user_id = _get_current_user_id()
    if g.user.id != user_id or g.user.username != username:
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    if request.method == "POST":
        mark_all_read = request.form.get('markAllRead', False)
        if mark_all_read:
            _mark_unread_notifications_as_read(g.user.id)
    notifications = _get_notifications(user_id, limit=_default_limit())
    return render_template(
        "bombadillo/notifications.html",
        notifications=notifications,
    )


@bp.route('/go-to-notification', methods=("GET",))
def go_to_notification():
    """
    Marks a notification as complete
    """
    if not g.user:
        flash('Login to view notifications')
        return redirect(url_for('bombadillo.login'))
    comment_code = request.args.get('comment_code')
    post_code = request.args.get('post_code')
    bounty_code = request.args.get('bounty_code')
    bounty_response_code = request.args.get('bounty_response_code')
    username = request.args.get('username')
    notification_code = request.args.get('notification_code')
    if comment_code:
        _mark_notification_as_read(notification_code)
        return redirect(url_for('bombadillo.comment', comment_code=comment_code))
    if post_code:
        _mark_notification_as_read(notification_code)
        return redirect(url_for('bombadillo.post', post_code=post_code))
    if bounty_code:
        _mark_notification_as_read(notification_code)
        return redirect(url_for('bounty.view_bounty', bounty_code=bounty_code))
    if bounty_response_code:
        _mark_notification_as_read(notification_code)
        return redirect(
            url_for(
                'bombadillo.view_bounty_response',
                bounty_response_code=bounty_response_code
            )
        )
    if username:
        _mark_notification_as_read(notification_code)
        return redirect(url_for('bombadillo.view_user', username=username))
    return redirect(url_for('bombadillo.notifications', username=g.user.username))


@bp.route('/more-threads', methods=("POST",))
def more_threads():
    offset = int(request.form.get('offset'), _default_offset())
    limit = _default_limit()
    posts, followee_ids = _get_threads(limit=limit, offset=offset)
    html = render_template('bombadillo/injectable_threads.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/more-explore', methods=("POST",))
def more_explore():
    offset = int(request.form.get('offset'), _default_offset())
    limit = _default_limit()
    filter_by = request.form.get('filter')
    scope = request.form.get('scope')
    posts = _get_explore_posts(
        filter_by=filter_by, scope=scope, limit=limit, offset=offset
    )
    html = render_template('bombadillo/injectable_explore.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/more-likes', methods=("POST",))
def more_user_likes():
    offset = int(request.form.get('offset'), _default_offset())
    limit = _default_limit()
    posts = _get_liked_posts(limit=limit, offset=offset)
    html = render_template('bombadillo/injectable_explore.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/more-bookmarks', methods=("POST",))
def more_user_bookmarks():
    offset = int(request.form.get('offset'), _default_offset())
    limit = _default_limit()
    posts = _get_bookmarked_posts(limit=limit, offset=offset)
    html = render_template('bombadillo/injectable_explore.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/more-zaps', methods=("POST",))
def more_user_zaps():
    offset = int(request.form.get('offset'), _default_offset())
    limit = _default_limit()
    posts = _get_zapped_posts(limit=limit, offset=offset)
    html = render_template('bombadillo/injectable_explore.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/more-notifications', methods=("POST",))
def more_notifications():
    user_id = _get_current_user_id()
    if g.user.id != user_id:
        return {'offset': 10000, 'html': '', 'message': 'Unauthorized'}
    offset = int(request.form.get('offset'), _default_offset())
    notifications = _get_notifications(user_id, limit=offset, offset=offset)
    html = render_template(
        'bombadillo/injectable_notifications.html',
        notifications=notifications
    )
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/u/<username>/edit-profile', methods=("GET", "POST"))
@login_required
def edit_profile(username):
    """
    Returns a template for updating user information
    """
    if not g.user or g.user.username != username:
        flash("Unauthorized")
        return redirect(url_for('bombadillo.threads'))
    user = _get_user_by_username(username)
    if request.method == "POST":
        email = request.form.get('email')
        if email == '':
            email = None
        link = request.form.get('link')
        if link == '':
            link = None
        bio = request.form.get('bio')
        if bio == '':
            bio = None
        email_opt_out = request.form.get('optOut', False)
        if email_opt_out:
            email_opt_out = True
        if link:
            link = bleach.clean(link)
        if bio:
            bio = bleach.clean(bio)
        success, upload, unique_filename = _process_request_file(request)
        # save user profile updates
        try:
            _save_user_profile(user.id, email, link, bio, unique_filename, email_opt_out)
        except Exception as e:
            print(e)
            flash("That email has already been taken")
        return redirect(url_for('bombadillo.view_user', username=user.username))
    return render_template('bombadillo/edit_profile.html', user=user)


@bp.route('/u/<username>/update-password', methods=("GET", "POST"))
@login_required
def update_password(username):
    """
    Account settings for a user
    """
    user, balance = _get_user(username)
    if request.method == "POST":
        current_password = request.form.get("currentPassword")
        new_password = request.form.get("newPassword")
        confirm_new_password = request.form.get("confirmNewPassword")
        error = None
        # confirm current password is correct and new password matches
        if not user.check_password(current_password):
            error = "Incorrect Current Password"
        if new_password != confirm_new_password:
            error = "New password check did not match, try again"
        if error is None:
            # update password
            user.set_password(new_password)
            db.session.commit()
            if user.email:
                sendgrid_password_updated(user)
            flash('Password updated')
        else:
            flash(error)
        return redirect(url_for('bombadillo.view_user', username=user.username))
    return render_template('bombadillo/update_password.html', user=user)


@bp.route('/search', methods=("GET", "POST",))
def search():
    if request.method == "POST":
        search_str = request.form.get("search")
        return redirect(url_for('bombadillo.search', s=search_str))
    search_str = request.args.get('search') or request.args.get('s')
    active = request.args.get("active", "user")  # default to user view active
    user_results = _search_users(search_str)
    post_results, followee_ids = _search_posts(search_str)
    bounty_results = _search_bounties(search_str)
    return render_template(
        "bombadillo/search_results.html",
        active=active,
        user_results=user_results,
        post_results=post_results,
        bounty_results=bounty_results
    )


@bp.route('/images/<filename>')
def view_image(filename):
    """Display image."""
    return render_template("bombadillo/image.html", image=filename)


@bp.route('/u/<username>')
def view_user(username):
    user, balance = _get_user(username)
    max_withdrawal_amount = _get_max_withdrawal_amount()
    posts = _get_user_posts(user.id, limit=_default_limit(), offset=0)
    return render_template(
        'bombadillo/user.html',
        user=user,
        balance=balance,
        posts=posts,
        max_zap=max_withdrawal_amount
    )


@bp.route('/u/<username>/user-confirm-email')
def user_confirm_email(username):
    user, balance = _get_user(username)
    return render_template(
        'bombadillo/user_confirm_email.html',
        user=user,
        balance=balance
    )


@bp.route('/u/<username>/drafts')
def view_user_drafts(username):
    if not g.user or g.user.username != username:
        flash('Unauthorized')
        return redirect(url_for('bombadillo.threads'))
    user, balance = _get_user(username)
    posts = _get_user_drafts(user.id, limit=_default_limit(), offset=0)
    return render_template(
        'bombadillo/user_drafts.html',
        user=user,
        posts=posts,
        balance=balance,
    )


@bp.route('/more-user-profile-posts', methods=("POST",))
def more_user_profile_posts():
    offset = int(request.form.get('offset'), _default_offset())
    username = request.form.get('username')
    user, balance = _get_user(username)
    posts = _get_user_posts(user.id, limit=offset, offset=offset)
    html = render_template('bombadillo/injectable_explore.html', posts=posts)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/sent-payment-history', methods=("GET",))
@login_required
def sent_payment_history():
    '''Displays to a logged in user their `sent payments` - sent to other posts'''
    if g.user.id is None or g.user.id != _get_current_user_id():
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    payments = _get_sent_user_payment_history(g.user.id, limit=_default_limit())
    return render_template("bombadillo/sent_payment_history.html", payments=payments)


@bp.route('/more-sent-payments', methods=("POST",))
@login_required
def more_sent_payments():
    offset = int(request.form.get('offset'), _default_offset())
    if g.user.id is None or g.user.id != _get_current_user_id():
        return {'message': 'unauthorized'}
    payments = _get_sent_user_payment_history(
        g.user.id,
        limit=offset,
        offset=offset
    )
    html = render_template('bombadillo/injectable_payments.html', payments=payments)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/received-payment-history', methods=("GET",))
@login_required
def received_payment_history():
    '''Displays to a logged in user their `sent payments` - sent to other posts'''
    if g.user.id is None or g.user.id != _get_current_user_id():
        flash('Unauthorized')
        return redirect(url_for('bombadillo.explore'))
    payments = _get_received_user_payment_history(g.user.id, limit=_default_limit())
    return render_template("bombadillo/received_payment_history.html", payments=payments)


@bp.route('/more-received-payments', methods=("POST",))
@login_required
def more_received_payments():
    offset = int(request.form.get('offset'), _default_offset())
    if g.user.id is None or g.user.id != _get_current_user_id():
        return {'message': 'unauthorized'}
    payments = _get_received_user_payment_history(
        g.user.id,
        limit=_default_limit(),
        offset=offset
    )
    html = render_template('bombadillo/injectable_payments.html', payments=payments)
    return {'offset': offset + _default_offset(), 'html': html}


@bp.route('/withdraw', methods=("GET", "POST"))
@login_required
def withdraw():
    user_id = _get_current_user_id()
    settings = _get_settings()
    maximum_withdrawal_amount = _get_max_withdrawal_amount()
    if request.method == "POST":
        if settings.withdrawals_frozen:
            return redirect(url_for("bombadillo.withdraw", id=user_id))
        try:
            ln_invoice = request.form.get('ln-invoice')
            ln_invoice = ln_invoice.replace('lightning:', '')  # just incase
            lnd = LNDClient(
                ip_address=current_app.config["LND_RPC_IP"],
                macaroon_filepath=current_app.config['LND_MACAROON'],
                cert_filepath=current_app.config['LND_TLS_CERT'],
                admin=True
            )
            info = lnd.decode_payment_request(ln_invoice)
            node_uri = info.destination
            amount = info.num_satoshis
            # check balance here
            balance = _get_user_balance(user_id)
            if amount > maximum_withdrawal_amount:
                flash(f'Max zap is {maximum_withdrawal_amount} sats')
            elif balance - amount >= 0:
                w = _save_withdrawal(user_id, ln_invoice, node_uri, amount)
                sendgrid_notify_admin_new_withdrawal(w)
                flash("Withdrawal Request Submitted")
            else:
                flash("Insufficient funds, invoice an amount less than or equal to your balance")
            return redirect(url_for("bombadillo.withdraw", id=user_id))
        except Exception as e:
            current_app.logger.info("Bolt 11 Invoice - withdraw failure")
            current_app.logger.info(e)
            flash("Invoice Failure. Please double check bolt11 invoice and retry.")
            return redirect(url_for("bombadillo.withdraw", id=user_id))
    withdrawals = _get_withdrawals(user_id)
    user_balance = _get_user_balance(user_id)
    if settings.withdrawals_frozen:
        message = "Withdrawals are frozen due to system maintenance"
        if settings.withdrawals_frozen_message:
            message = settings.withdrawals_frozen_message
        flash(message)
    return render_template(
        "bombadillo/withdraw.html",
        withdrawals=withdrawals,
        user_balance=user_balance,
        settings=settings,
        max_zap=maximum_withdrawal_amount
    )
