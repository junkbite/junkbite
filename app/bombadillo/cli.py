import click
from app.utils.db_utils import _delete_expired_ads
from flask.cli import with_appcontext


@click.command('delete-expired-advertisements')
@with_appcontext
def delete_expired_advertisements_command():
    """
    Removes all advertisements that are expired from the purchased time
    """
    ads = _delete_expired_ads()
    click.echo(f'Deleted {ads} advertisements')


def init_app(app):
    """
    app.cli.add_command() adds a new command that can be called with the flask
    command.
    """
    app.cli.add_command(delete_expired_advertisements_command)
