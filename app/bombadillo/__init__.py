from flask import Blueprint

bp = Blueprint('bombadillo', __name__)

from app.bombadillo import routes, cli
