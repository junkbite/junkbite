"""
a python lnd client which listens for completed invoice payments and notifies
the server

create a click command line tool to properly implement this

TODO: This client needs to be ALWAYS ON and ALWAYS LISTENING.

- Monitoring of this application
- Logs
- Database Entries for invoices found etc -- double match it against web server on periodic
- Auto restart this client on failure
"""
import os
import sys
import json
import click
import requests
from lndgrpc import LNDClient

BASEDIR = os.path.abspath(os.path.dirname(__file__))


try:
    with open('/etc/junkbite.json') as config_file:
        config = json.load(config_file)
except FileNotFoundError as e:
    print('Not in production, using .local_config.json')
    with open('.local_config.json') as config_file:
        config = json.load(config_file)


class Config:
    """
    Confuration can be controlled here for now, move to separate file later
    """
    def __init__(self):
        self.verbose = False
        self.ip_address = config.get("LND_RPC_IP")
        self.macaroon_filepath = config.get("LND_MACAROON")
        self.cert_filepath = config.get("LND_TLS_CERT")
        self.admin = config.get("LND_ADMIN") or True
        self.url = config.get("JUNKBITE_SERVER_URL") or "https://junkbite.com/api/invoice-hook"
        self.secret = config.get("SECRET")
    #
    def __str__(self):
        configuration = {
            'verbose': self.verbose,
            'ip_address': self.ip_address,
            'macaroon_filepath': self.macaroon_filepath,
            'cert_filepath': self.cert_filepath,
            'admin': self.admin,
            'url': self.url
        }
        return json.dumps(configuration, indent=4)


def notify_server(invoice, amount, config):
    """
    Sends invoice to server to notify the payment has been received
    """
    data = {
        'secret': config.secret,
        'payment-request': invoice,
        'amount': amount
    }
    try:
        response = requests.post(config.url, json=data, timeout=10)
        return response
    except requests.exceptions.Timeout:
        print("Timeout occurred")
        return None


pass_config = click.make_pass_decorator(Config, ensure=True)


@click.group()
@click.option('--log-file', '-l', type=click.File('w'), default=sys.stderr)
@click.option('--verbose', '-v', is_flag=True)
@pass_config
def cli(config, log_file, verbose):
    config.verbose = verbose
    config.log_file = log_file
    if config.verbose:
        click.echo("Verbose mode On")


@cli.command()
@pass_config
def start_app(config):
    """Start the application"""
    if config.verbose:
        click.echo("Starting App")
        click.echo(config)

    lnd = LNDClient(
        ip_address=config.ip_address,
        macaroon_filepath=config.macaroon_filepath,
        cert_filepath=config.cert_filepath,
        admin=True
    )
    print(lnd.get_info())
    click.echo('Listening for invoices...')
    for invoice in lnd.subscribe_invoices():
        print(invoice)
        if invoice.settled:
            # hit webhook with payment request
            print(invoice)  # Make this a log
            notify_server(invoice.payment_request, invoice.amt_paid_sat, config)


if __name__ == "__main__":
    # example start: `python lightning_watcher.py -v start-app`
    cli()
