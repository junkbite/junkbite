import unittest
from flask_testing import TestCase
import os
from os.path import expanduser
import json
from app import create_app, db
from app.models import (
    User,
    Blog,
    Payment,
    PaymentProcessingFee,
    Post,
    Settings
)
from app.utils.db_utils import (
    _create_user,
    _save_payment,
    _save_bounty,
    _save_bounty_creation_payment,
    _save_bounty_payout
)
from custom_jinja_filters import (
    strftime,
    remove_leading_char,
    strip_code_type,
    process_post_text
)


BASEDIR = os.path.abspath(os.path.dirname(__file__))
ADMIN_USERNAME = 'admin'
ADMIN_PASSWORD = 'admin-password'
ANON_USERNAME = 'anonymous'
ANON_PASSWORD = 'anon-password'
JUNKBITE_USERNAME = 'junkbite'
JUNKBITE_PASSWORD = 'junkbite-password'


class JunkbiteTest(TestCase):

    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(BASEDIR, "instance/app-test.sqlite")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    TESTING = True

    def create_app(self):
        # pass in test configuration
        app = create_app(self)
        app.secret_key = "test"
        # Add custom jinja filters to environment
        app.jinja_env.filters["strftime"] = strftime
        app.jinja_env.filters["remove_leading_char"] = remove_leading_char
        app.jinja_env.filters["strip_code_type"] = strip_code_type
        app.jinja_env.filters["process_post_text"] = process_post_text
        app.config.update(
            LND_MACAROON="/Users/Joe/.lnd/admin.macaroon",
            LND_TLS_CERT="/Users/Joe/.lnd/tls.cert",
            LND_RPC_IP="192.168.0.51:10009",
            ADMINS=["test@junkbite.com"]
        )
        return app

    def setUp(self):
        # initalize the app - referring to readme .. need to create some stuff
        db.create_all()
        _create_user(ADMIN_USERNAME, ADMIN_PASSWORD, True)
        _create_user(ANON_USERNAME, ANON_PASSWORD, False)
        _create_user(JUNKBITE_USERNAME, JUNKBITE_PASSWORD, False)
        s = Settings(
            payment_percentage=5,
            withdrawals_frozen=False,
            withdrawals_frozen_message="Not Frozen",
            maximum_withdrawal_amount=2000,
            withdrawal_period=24
        )
        db.session.add(s)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def register(self, username, password):
        return self.client.post('/register', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def login(self, username, password):
        return self.client.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.client.get('/logout', follow_redirects=True)

    def test_register_new_user(self):
        # Calling client gets a new session
        rv = self.register('junkbiteUser1', 'testPassword1')
        self.assertIn(b'Successfully registered', rv.data)

    def test_login_logout(self):
        """Make sure login and logout works."""
        rv = self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        self.assertIn(b'admin', rv.data)
        rv = self.logout()
        self.assertIn(b"Login", rv.data)
        rv = self.login('not-a-user', 'not-a-password')
        self.assertIn(b'Username not registered', rv.data)
        rv = self.login(ADMIN_USERNAME, 'not-admin-password')
        self.assertIn(b'Incorrect password', rv.data)

    def test_not_logged_in_redirect_to_login(self):
        """Get empty explore page."""
        rv = self.client.get('/', follow_redirects=True)
        self.assertIn(b'Login', rv.data)

    def test_empty_db(self):
        """Get empty explore page."""
        rv = self.client.get('/explore', follow_redirects=True)
        self.assertIn(b'Theres no posts', rv.data)

    def test_create_empty_post(self):
        """Test create an empty post -- not allowed"""
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post('/create', data=dict(
            title='',
        ), follow_redirects=True)
        # Ensure post was NOT created and error message displays
        self.assertIn(b"Your post cannot be empty", rv.data)

    def test_create_post(self):
        """Test creating a post works."""
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post('/create', data=dict(
            title='Hello, World!',
        ), follow_redirects=True)
        # Check that the post was created and username returned on post view
        self.assertIn(b'Hello, World!', rv.data)
        self.assertIn(b'admin', rv.data)

    def test_explore_page_shows_post(self):
        """The post created in the above function should not be seen in the
        explore page because it doesn't include an image."""
        rv = self.client.get('/')
        self.assertNotIn(b'Hello, World!', rv.data)
        self.assertNotIn(b'admin', rv.data)

    def test_get_admin_manage(self):
        '''GET  /admin/manage'''
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get('/administration/admin')
        self.assertIn(b'Junkbite Administration', rv.data)

    def test_get_register(self):
        '''GET /register'''
        rv = self.client.get('/register')
        self.assertIn(b"Optional. Used for password reset and notifications", rv.data)
        self.assertIn(b"Register", rv.data)
        self.assertIn(b"Login", rv.data)
        self.assertIn(b"Junkbite", rv.data)

    def test_get_login(self):
        '''GET /register'''
        rv = self.client.get('/login')
        self.assertIn(b"Register", rv.data)
        self.assertIn(b"Login", rv.data)
        self.assertIn(b"Junkbite", rv.data)

    def test_get_auth_reset_password(self):
        '''GET, POST  /reset-password/<token>'''
        bad_token = 'not-good-123'
        rv = self.client.get(
            f'reset-password/{bad_token}', follow_redirects=True
        )
        self.assertIn(b'Invalid Reset Token', rv.data)

    def test_get_auth_reset_password_request(self):
        '''GET, POST  /reset-password-request'''
        rv = self.client.get('/reset-password-request')
        self.assertIn(b'Get Reset Password Link', rv.data)
        self.assertIn(b'Email', rv.data)

    def test_get_create_blog(self):
        '''GET, POST  /create_blog'''
        # without logging in
        rv = self.client.get('/create_blog', follow_redirects=True)
        self.assertIn(b'Unauthorized', rv.data)
        # login is basic user
        self.login(ANON_USERNAME, ANON_PASSWORD)
        rv = self.client.get('/create_blog', follow_redirects=True)
        self.assertIn(b'You cannot go there', rv.data)
        self.logout()
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get('/create_blog', follow_redirects=True)
        self.assertIn(b'Create Blog', rv.data)
        self.assertIn(b'Title', rv.data)
        self.assertIn(b'Blog', rv.data)
        self.assertIn(b'Submit', rv.data)

    def test_create_blog(self):
        """Test creating a post works."""
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post('/create_blog', data=dict(
            title='Hello, World!',
            body="Test Blog Creation"
        ), follow_redirects=True)
        # Check that the post was created and username returned on post view
        self.assertIn(b'Hello, World!', rv.data)
        self.assertIn(b'admin', rv.data)
        self.assertIn(b'Test Blog Creation', rv.data)

    def test_create_empty_blog(self):
        """Test creating a blog works."""
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post('/create_blog', data=dict(
            title='',
            body='Hello'
        ), follow_redirects=True)
        # Ensure post was NOT created and error message displays
        self.assertIn(b"Title is required", rv.data)
        rv = self.client.post('/create_blog', data=dict(
            title='hi',
            body='',
        ), follow_redirects=True)
        # Ensure post was NOT created and error message displays
        self.assertIn(b"Body is required", rv.data)

    def test_get_blog(self):
        '''GET, POST  /blogs/<int:blog_id>'''
        # create blog then get blog
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        self.client.post('/create_blog', data=dict(
            title='Hello World',
            body="Test Blog Creation"
        ), follow_redirects=True)
        # Get the blog  that was just created
        slug = "hello-world"
        rv = self.client.get(f'blogs/{slug}')
        self.assertIn(b'Hello World', rv.data)
        self.assertIn(b'Test Blog Creation', rv.data)

    def test_get_blogs(self):
        '''GET /blogs'''
        rv = self.client.get('/blogs')
        self.assertIn(b'blogs', rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get('/blogs')
        self.assertIn(b'create blog', rv.data)

    def test_delete_blog(self):
        '''POST       /blogs/delete-blog/<int:blog_id>'''
        # create blog then get blog
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        self.client.post('/create_blog', data=dict(
            title='Hello World',
            body="Test Blog Creation"
        ), follow_redirects=True)
        # Get the blog post that was just created
        rv = self.client.get('/blogs/hello-world')
        self.assertIn(b'Hello World', rv.data)
        self.assertIn(b'Test Blog Creation', rv.data)
        # Delete the blog that was created and make sure its not there anymore
        blog = Blog.query.filter_by(slug="hello-world").first()
        rv = self.client.post(f'/blogs/delete-blog/{blog.id}', follow_redirects=True)
        self.assertNotIn(b'Hello World', rv.data)
        rv = self.client.get('/blogs/hello-world', follow_redirects=True)
        self.assertIn(b'Blog not found', rv.data)

    def test_get_api_information(self):
        '''GET /api'''
        rv = self.client.get('api', follow_redirects=True)
        self.assertIn(b"api information", rv.data)
        self.assertIn(b"GET", rv.data)

    def test_get_bombadillo_edit_profile(self):
        '''GET, POST  /edit-profile/<int:user_id>'''
        rv = self.client.get('/u/admin/edit-profile', follow_redirects=True)
        self.assertIn(b'Login for this function', rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get('/u/admin/edit-profile')
        self.assertIn(b"Avatar", rv.data)
        self.assertIn(b"Email", rv.data)
        self.assertIn(b"web link", rv.data)
        self.assertIn(b"bio", rv.data)
        self.assertIn(b"Update Profile", rv.data)

    def test_get_lightning_random(self):
        '''GET, POST  /random'''
        rv = self.client.get('/random')
        self.assertIn(b"Pay a lightning invoice", rv.data)
        self.assertIn(b"Invoice Me!", rv.data)

    def test_get_lightning(self):
        '''GET, POST  /lightning'''
        rv = self.client.get('/lightning')
        self.assertIn(b"Pay a lightning invoice", rv.data)
        self.assertIn(b"Invoice Me!", rv.data)
        self.assertIn(b"Control Junkbite's Twitter", rv.data)
        self.assertIn(b"Send a Tweet", rv.data)
        self.assertIn(b"Update Banner Image", rv.data)
        self.assertIn(b"Update Profile Image", rv.data)

    def test_get_bombadillo_received_payment_history(self):
        '''GET        /received-payment-history'''
        rv = self.client.get("/received-payment-history", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/received-payment-history")
        self.assertIn(b"Activity - Received", rv.data)

    def test_get_bombadillo_sent_payment_history(self):
        '''GET        /sent-payment-history'''
        rv = self.client.get("/sent-payment-history", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/sent-payment-history")
        self.assertIn(b"Activity - Sent", rv.data)

    def test_get_bombadillo_update_password(self):
        '''GET, POST  /u/<username>/update-password'''
        rv = self.client.get("/u/admin/update-password", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/u/admin/update-password")
        self.assertIn(b"Current Password", rv.data)
        self.assertIn(b"New Password", rv.data)
        self.assertIn(b"Confirm New Password", rv.data)

    def test_get_bombadillo_user_bookmarks(self):
        '''GET        /u/<username>/bookmarks'''
        rv = self.client.get("/u/admin/bookmarks", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/u/admin/bookmarks")
        self.assertIn(b"Theres no posts", rv.data)

    def test_get_bombadillo_user_likes(self):
        '''GET        /user/likes'''
        rv = self.client.get("/u/admin/likes", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/u/admin/likes")
        self.assertIn(b"Theres no posts", rv.data)

    def test_get_bombadillo_withdraw(self):
        '''GET, POST  /withdraw'''
        rv = self.client.get("/withdraw", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/withdraw")
        self.assertIn(b"Create an invoice with your lightning wallet and paste it below", rv.data)
        self.assertIn(b"Withdrawals", rv.data)
        self.assertIn(b"Submit Withdrawal Request", rv.data)

    def test_get_bounty_all_bounties(self):
        '''GET        /all-bounties'''
        rv = self.client.get("/all-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Create", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/all-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Your Bounties", rv.data)
        self.assertIn(b"Your Responses", rv.data)
        self.assertIn(b"Create", rv.data)

    def test_get_bounty_closed_bounties(self):
        '''GET        /closed-bounties'''
        rv = self.client.get("/closed-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Create", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/closed-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Your Bounties", rv.data)
        self.assertIn(b"Your Responses", rv.data)
        self.assertIn(b"Create", rv.data)

    def test_get_bounty_open_bounties(self):
        '''GET        /open-bounties'''
        rv = self.client.get("/open-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Create", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/open-bounties")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Your Bounties", rv.data)
        self.assertIn(b"Your Responses", rv.data)
        self.assertIn(b"Create", rv.data)

    def test_get_bounty_create_bounty(self):
        '''GET, POST  /create-bounty'''
        rv = self.client.get("/create-bounty", follow_redirects=True)
        self.assertIn(b"Login for this function", rv.data)
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get("/create-bounty")
        self.assertIn(b"All", rv.data)
        self.assertIn(b"Open", rv.data)
        self.assertIn(b"Closed", rv.data)
        self.assertIn(b"Your Bounties", rv.data)
        self.assertIn(b"Your Responses", rv.data)
        self.assertIn(b"Create", rv.data)
        self.assertIn(b"Create Bounty", rv.data)
        self.assertIn(b"Bounty Information", rv.data)
        self.assertIn(b"Generate Bounty Invoice", rv.data)

    def test_get_information_info(self):
        '''GET        /information'''
        rv = self.client.get("/information")
        self.assertIn(b"Information Links", rv.data)
        self.assertIn(b"About", rv.data)
        self.assertIn(b"FAQs", rv.data)
        self.assertIn(b"Lightning Node", rv.data)
        self.assertIn(b"Help", rv.data)
        self.assertIn(b"Rules", rv.data)
        self.assertIn(b"Support", rv.data)
        self.assertIn(b"Feedback", rv.data)
        self.assertIn(b"Privacy Policy", rv.data)
        self.assertIn(b"Terms of Service", rv.data)
        # test backend route
        rv = self.client.get("/info")
        self.assertIn(b"Information Links", rv.data)
        self.assertIn(b"About", rv.data)
        self.assertIn(b"FAQs", rv.data)
        self.assertIn(b"Lightning Node", rv.data)
        self.assertIn(b"Help", rv.data)
        self.assertIn(b"Rules", rv.data)
        self.assertIn(b"Support", rv.data)
        self.assertIn(b"Feedback", rv.data)
        self.assertIn(b"Privacy Policy", rv.data)
        self.assertIn(b"Terms of Service", rv.data)

    def test_get_information_lightning_info(self):
        '''GET        /node-information'''
        rv = self.client.get("node-information")
        self.assertIn(b"Junkbite Lightning Node Information", rv.data)
        self.assertIn(b"Alias", rv.data)
        self.assertIn(b"Node URI", rv.data)
        self.assertIn(b"How to connect with LND", rv.data)
        self.assertIn(b"How to connect with c-lightning", rv.data)

    def test_get_information_rules(self):
        '''GET        /rules'''
        rv = self.client.get("rules")
        self.assertIn(b"Rules", rv.data)
        self.assertIn(b"No Hacking", rv.data)
        self.assertIn(b"Do not hack junkbite", rv.data)

    def test_payment_and_fee(self):
        """
        Test saving a payment and check its saved and a fee is saved
        """
        # first create a post to tip
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post('/create', data=dict(
            title='Hello, World!',
        ), follow_redirects=True)
        # Check that the post was created and username returned on post view
        self.assertIn(b'Hello, World!', rv.data)
        self.assertIn(b'admin', rv.data)
        post = Post.query.filter_by(title="Hello, World!").first()
        amount = 250
        fee = 0.05  # 5% -> pull this from settings
        invoice = "Test-Bolt11"
        post_id = post.id
        _save_payment(
            amount=amount,
            ln_invoice=invoice,
            post_id=post_id,
            junkbite_app="social",
            payment_type="sat boost on post"
        )
        payment = (
            Payment.query.filter_by(post_id=post_id, amount=amount)
            .first()
        )
        self.assertEqual(payment.amount, amount)
        self.assertEqual(payment.ln_invoice, "Test-Bolt11")
        self.assertIsNone(payment.bounty_id)
        self.assertIsNone(payment.bounty_response_id)
        self.assertIsNotNone(payment.fee)
        self.assertEqual(payment.fee.payment_id, payment.id)
        self.assertEqual(payment.fee.amount, int(amount * fee))

    def test_bounty_flow(self):
        # Login and GET create bounty page
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.get('/create-bounty', follow_redirects=True)
        # self.assertEqual(we are on the Create Bounty page
        self.assertIn(b'Create Bounty', rv.data)
        # We can do a post method here because testing can't talk to LND
        # so we will work around it
        title = "My First Bounty"
        body = "What is the Fed's inflation target?"
        invoice = "Test-Bolt11"
        processed_body = body
        fee = 0.05
        amount = 2500
        upload = None
        filename = None
        user_id = User.query.filter_by(username=ADMIN_USERNAME).first().id
        _save_payment(
            amount=amount,
            ln_invoice=invoice,
            junkbite_app="bounty",
            payment_type="bounty creation"
        )
        bounty = _save_bounty(
            title,
            body,
            processed_body,
            user_id, 
            upload,
            filename,
            amount,
            invoice
        )
        _save_bounty_creation_payment(
            amount=amount,
            ln_invoice=invoice,
            bounty_id=bounty.id,
            junkbite_app="bounty",
            payment_type="bounty creation",
        )
        # Now check Payment and Fees
        payment = (
            Payment.query.filter_by(bounty_id=bounty.id, amount=amount)
            .first()
        )
        self.assertEqual(payment.amount, amount)
        self.assertEqual(payment.ln_invoice, "Test-Bolt11")
        self.assertIsNone(payment.post_id)
        self.assertEqual(payment.bounty_id, bounty.id)
        self.assertIsNone(payment.bounty_response_id)
        self.assertIsNotNone(payment.fee)
        self.assertEqual(payment.fee.payment_id, payment.id)
        self.assertEqual(payment.fee.amount, int(amount * fee))
        # Now create a bounty response and award it
        # Fail to respond to own bounty (correct)
        rv = self.client.post(f'/bounty/{bounty.bounty_code}', data=dict(
            response="I am answering my own bounty!"
        ), follow_redirects=True)
        self.assertIn(b"You cannot respond to your own bounty", rv.data)
        # logout and back in with different user
        self.logout()
        self.login(JUNKBITE_USERNAME, JUNKBITE_PASSWORD)
        response = "The Fed members are all fiat brains"
        rv = self.client.post(f'/bounty/{bounty.bounty_code}', data=dict(
            response=response
        ), follow_redirects=True)
        self.assertIn(response.encode("UTF-8"), rv.data)
        # now logout and log back in with admin to reward the bounty
        # first ensure junkbite cannt reward themself the bounty
        rv = self.client.post(f'/award-bounty/{bounty.bounty_code}', data=dict(
            bountyResponseWinner=1
        ), follow_redirects=True)
        self.assertIn(b'This is not your bounty to award', rv.data)
        self.logout()
        self.login(ADMIN_USERNAME, ADMIN_PASSWORD)
        rv = self.client.post(f'/award-bounty/{bounty.bounty_code}', data=dict(
            bountyResponseWinner=1
        ), follow_redirects=True)
        self.assertIn(b"Bounty Awarded and Closed", rv.data)
        # check payment was sent to JUNKBITE_USERNAME
        payment = (
            Payment.query.filter_by(
                receiving_user_id=3, payment_type="bounty winner"
            )
            .first()
        )
        self.assertEqual(payment.amount, amount - int(amount*fee))


if __name__ == '__main__':
    unittest.main()
