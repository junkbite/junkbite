# junkbite.
Built with Flask
[Flask Offical Docs](http://flask.pocoo.org/docs/1.0/installation/#virtual-environments)

## Setting Up Locally
This is the backend repository for all services and utilities. The following
instructions show how to get the project setup on a local environment.

#### Database Tables
View sql in app/scheme.sql --> sql for sqlite
```
advertisement
advertisement_stats
archived_posts
blog
blog_comment
bounty
bounty_response
bounty_response_upvote
btc_pay_server_client
comment
comment_like
followers
junkbite_node
notification
onion_url
payment
post
post_like
post_node
reported_content
user
user_balance
user_balance_entries
user_node
withdrawal
```

#### Create virtual environment using python 3

```
cd ~/junkbite
python3 -m venv venv
```

#### Activating the virtual environment
```
. venv/bin/activate
```
Always have your virtual environment active when developing otherwise you will have dependency issues.

#### Deactivating virtual environment
```
deactivate
```

#### Install Libraries
```
pip install -r requirements.txt
```

#### Create images and invoices directory inside static folder
```
mkdir app/static/images
mkdir app/static/invoices
```

#### Set Environment Variables
Run each line individually in terminal like this:
```
export FLASK_APP=app
export FLASK_ENV=development
```
Create a hidden file called `.local_config.json` full of fields that config.py will read
from, based on what is that file, some parts of junkbite may not work (for instance twitter bot
will not work unless you have twitter api keys)

```
{
    "SECRET_KEY":"TEST",
    "DATABASE_URL": "",
    "DATABASE_HOST": "",
    "DATABASE_NAME": "",
    "DATABASE_USER": "",
    "DATABASE_PASSWORD": "",
    "DEBUG": false,
    "ADMIN_EMAIL_ADDRESS_LIST": [""],
    "twitter_api_key":"",
    "twitter_api_secret":"",
    "twitter_access_token":"",
    "twitter_access_secret":"",
    "twitter_bearer_token":"",
    "LND_MACAROON":"/path/to/.lnd/admin.macaroon",
    "LND_TLS_CERT":"/path/to/.lnd/tls.cert",
    "LND_RPC_IP":"127.0.0.1:10009",
    "JUNKBITE_SERVER_URL":"http://localhost:5000/api/invoice-hook",
    "FLASK_APP":"start_bombadillo.py",
    "FLASK_ENV":"development",
    "SENDGRID_SECRET":"",
    "LOG_TO_STDOUT": false
}
```
There are more environment variables that need to be sourced, please view: `config.py` to see them all

#### Initialize the database, admin
Run these from top level folder in project.

if you are using mysql as your database you need to create a database called: `junkbite-lightning`
if you are using sqlite it should be created automatically in `instance/app.sqlite`
if there is no migrations folder skit the `flask db init` command
```
flask db init
flask db migrate
flask db upgrade
flask create-admin <username> <password>
flask create-anonymous <password>
flask generate-admin-views
flask default-config-lightning-apps
```
*Examples*
```
flask create-admin admin admin-password
flask create-anonymous password-for-anon
```

#### Start the development server
Run these from top level folder in project.
```
flask run
```
or run over tor like this:
```
python3 start_bombadillo.py
```


#### Installing on production
1. get linux server
2. git clone repo
3. install python3 and python3-pip and anything else
4. activate virtual environment : . venv/bin/activate
5. pip install -r requirements.txt
6. all trafic on port 80 : sudo ufx allow http/80
7. can setup up some other stuff like ssh (probably already setup) - allow some other stuff too
8. install nginx - sudo apt install nginx
9. create junkbite nginx configuration file in /etc/nginx/sites-enabled/ using example in this repo
10. go back to code project (make sure still in venv) and pip install gunicorn
11. start gunicorn

12. now that its working configure supervisor and systemd to always be running this stuff
13. setup up supervisor and configure it

good tutorial here: https://www.youtube.com/watch?v=goToXTC96Co&list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH&index=13
starts at 26:00

14. once nginx is running and supervisord is running gunicorn and its all good its time to setup up ssl for https
15. good walkthrough here: https://www.youtube.com/watch?v=Gdys9qPjuKs&list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH&index=15
16. essentially we install certbot and run the commands and it updates nginx configuration accordingly
17. restart nginx and test its working. then setup cronjob to renex certificate every month `30 4 1 * * sudo certbot renew --quiet`
18. Set up cronjobs -> check in periodic tasks folder
19. increase upload size allowed in nginx
```
# set client body size to 2M #
client_max_body_size 5M;
```
5 megabytes