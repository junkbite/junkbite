"""empty message

Revision ID: 7b7e5860e177
Revises: c869e89d8c56
Create Date: 2021-06-05 10:18:27.292184

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '7b7e5860e177'
down_revision = 'c869e89d8c56'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('faq_entry', 'answer',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=4294000000),
               existing_nullable=True)
    op.alter_column('post', 'body',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=4294000000),
               existing_nullable=True)
    op.alter_column('post', 'processed_body',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=4294000000),
               existing_nullable=True)
    op.add_column('user', sa.Column('email_verified', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'email_verified')
    op.alter_column('post', 'processed_body',
               existing_type=sa.Text(length=4294000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    op.alter_column('post', 'body',
               existing_type=sa.Text(length=4294000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    op.alter_column('faq_entry', 'answer',
               existing_type=sa.Text(length=4294000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    # ### end Alembic commands ###
