"""empty message

Revision ID: a5f804e98a9b
Revises: ba05616c1fa5
Create Date: 2021-05-12 17:34:06.788896

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = 'a5f804e98a9b'
down_revision = 'ba05616c1fa5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('feedback', 'feedback_code',
               existing_type=mysql.VARCHAR(length=20),
               type_=sa.String(length=30),
               existing_nullable=True)
    op.alter_column('support_ticket', 'ticket_code',
               existing_type=mysql.VARCHAR(length=20),
               type_=sa.String(length=30),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('support_ticket', 'ticket_code',
               existing_type=sa.String(length=30),
               type_=mysql.VARCHAR(length=20),
               existing_nullable=True)
    op.alter_column('feedback', 'feedback_code',
               existing_type=sa.String(length=30),
               type_=mysql.VARCHAR(length=20),
               existing_nullable=True)
    # ### end Alembic commands ###
