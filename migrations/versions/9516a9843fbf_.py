"""empty message

Revision ID: 9516a9843fbf
Revises: 9259e7d9614a
Create Date: 2021-05-15 20:21:26.084527

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '9516a9843fbf'
down_revision = '9259e7d9614a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'body',
               existing_type=mysql.TEXT(),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    op.alter_column('post', 'processed_body',
               existing_type=mysql.TEXT(),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'processed_body',
               existing_type=mysql.LONGTEXT(),
               type_=mysql.TEXT(),
               existing_nullable=True)
    op.alter_column('post', 'body',
               existing_type=mysql.LONGTEXT(),
               type_=mysql.TEXT(),
               existing_nullable=True)
    # ### end Alembic commands ###
