"""empty message

Revision ID: 8976a66c5a23
Revises: c292efece825
Create Date: 2021-05-27 22:30:20.591032

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '8976a66c5a23'
down_revision = 'c292efece825'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'body',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=4294000000),
               existing_nullable=True)
    op.alter_column('post', 'processed_body',
               existing_type=mysql.LONGTEXT(),
               type_=sa.Text(length=4294000000),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'processed_body',
               existing_type=sa.Text(length=4294000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    op.alter_column('post', 'body',
               existing_type=sa.Text(length=4294000000),
               type_=mysql.LONGTEXT(),
               existing_nullable=True)
    # ### end Alembic commands ###
