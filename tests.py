import os
import tempfile
import pytest
from werkzeug.security import generate_password_hash
from app import create_app


@pytest.fixture
def client():
    """
    Configures the app for testing and initializes the database
    https://flask.palletsprojects.com/en/1.1.x/testing/
    1. create the 'test' application
    2. create the database
    3. setup up the application as it would be if it were fresh...
        This means creating necessary acounts, and running setup functions etc
        see: db.py
    """
    app = create_app()
    db_fd, app.config['DATABASE_URI'] = tempfile.mkstemp()
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            init_db(testing=True, testing_temp_file_loc=app.config['DATABASE_URI'])
            create_admin('admin', 'admin-test-password')
            create_anonymous('anon-test-password')
        yield client

    os.close(db_fd)
    os.unlink(app.config['DATABASE_URI'])


def create_admin(username, password):
    """
    Create a test admin user in temporary database for testing
    """
    db = get_db()
    cursor = db.execute(
        'INSERT INTO user (username, password, admin)'
        ' VALUES (?, ?, ?)',
        (username, generate_password_hash(password), 1)
    )
    db.commit()
    db.execute(
        "INSERT INTO user_balance (user_id, balance) VALUES (?, ?)",
        (cursor.lastrowid, 0)
    )
    db.commit()


def create_anonymous(password):
    """
    Create a test anonymous user in temporary database for testing
    """
    db = get_db()
    cursor = db.execute(
        'INSERT INTO user (username, password, admin)'
        ' VALUES (?, ?, ?)',
        ('anonymous', generate_password_hash(password), 0)
    )
    db.commit()
    db.execute(
        "INSERT INTO user_balance (user_id, balance) VALUES (?, ?)",
        (cursor.lastrowid, 0)
    )
    db.commit()


def register(client, username, password):
    return client.post('auth/register', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)


def login(client, username, password):
    return client.post('auth/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)


def logout(client):
    return client.get('auth/logout', follow_redirects=True)


def test_login_logout(client):
    """Make sure login and logout works."""
    rv = login(client, 'admin', 'admin-test-password')
    assert b'Login successful' in rv.data
    rv = login(client, 'not-a-user', 'not-a-password')
    assert b'Username not registered' in rv.data
    rv = login(client, 'admin', 'not-admin-password')
    assert b'Incorrect password' in rv.data


def test_register_new_user(client):
    """Test a new user can register."""
    # Successfully register new user
    rv = register(client, 'junkbite', 'junkbite')
    assert b'Successfully registered' in rv.data
    # Fail to create user again
    rv = register(client, 'junkbite', 'junkbite')
    assert b'User junkbite is already registered' in rv.data


def test_empty_db(client):
    """Get empty explore page."""
    rv = client.get('/')
    assert b'Theres no posts' in rv.data


def test_create_post(client):
    """Test creating a post works."""
    login(client, 'admin', 'admin-test-password')
    rv = client.post('/create', data=dict(
        title='Hello, World!',
    ), follow_redirects=True)
    # Check that the post was created and username returned on post view
    assert b'Hello, World!' in rv.data
    assert b'admin' in rv.data


def test_create_empty_post(client):
    """Test creating a post works."""
    login(client, 'admin', 'admin-test-password')
    rv = client.post('/create', data=dict(
        title='',
    ), follow_redirects=True)
    # Ensure post was NOT created and error message displays
    assert b"Your post cannot be empty" in rv.data


def test_explore_page_doesnt_include_new_post(client):
    """The post created in the above function should not be seen in the
    explore page because it doesn't include an image."""
    rv = client.get('/')
    assert b'Hello, World!' not in rv.data
    assert b'admin' not in rv.data


def test_post_admin_judgement(client):
    '''POST       /admin/review/judgement/<int:id>'''
    pass


def test_get_admin_manage(client):
    '''GET  /admin/manage'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('admin/manage')
    assert b'Administration Home' in rv.data


def test_get_admin_manage_comments(client):
    '''GET  /admin/manage-comments'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('admin/manage-comments')
    assert b'Comments' in rv.data
    assert b'Comment ID' in rv.data
    assert b'User ID' in rv.data
    assert b'Post ID' in rv.data
    assert b'Created' in rv.data
    assert b'Body' in rv.data
    assert b'Upload' in rv.data
    assert b'Unique Filename' in rv.data


def test_get_admin_manage_posts(client):
    '''GET  /admin/manage-posts'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('admin/manage-posts')
    assert b'Posts' in rv.data
    assert b'Post ID' in rv.data
    assert b'User ID' in rv.data
    assert b'Created' in rv.data
    assert b'Title' in rv.data
    assert b'Upload' in rv.data
    assert b'Unique Filename' in rv.data
    assert b'NSFW' in rv.data
    assert b'Meme' in rv.data
    assert b'Body' in rv.data


def test_get_admin_manage_reported_content(client):
    '''GET, POST  /admin/manage-reported-content'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('admin/manage-reported-content')
    assert b'Reported Content' in rv.data
    assert b'Take Action' in rv.data
    assert b'ID' in rv.data
    assert b'Post ID' in rv.data
    assert b'Comment ID' in rv.data
    assert b'Post or Comment' in rv.data
    assert b'Reporter User ID' in rv.data
    assert b'Reported User ID' in rv.data
    assert b'Reason' in rv.data
    assert b'First Reported' in rv.data
    assert b'Reported Count' in rv.data
    assert b'Action Taken' in rv.data


def test_get_admin_manage_users(client):
    '''GET, POST  /admin/manage-users'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('admin/manage-users')
    assert b'Users' in rv.data
    assert b'User ID' in rv.data
    assert b'Username' in rv.data
    assert b'Created' in rv.data
    assert b'IP Address' in rv.data
    assert b'Last Login' in rv.data
    assert b'Punishment Level' in rv.data


def test_get_admin_review(client):
    '''GET        /admin/review/<int:id>'''
    login(client, 'admin', 'admin-test-password')
    # need to create a reported post, then do a get
    pass


def test_get_advertise_buy_sticky_post(client):
    '''GET, POST  /advertise/buy_sticky_post'''
    pass


def test_get_advertise_create_sticky_post(client):
    '''GET, POST  /advertise/create_sticky_post/<int:ad_id>'''
    pass


def test_get_api_api_comments(client):
    '''GET        /api/comments'''
    pass


def test_get_api_api_create_post(client):
    '''POST       /api/create-post'''
    pass


def test_get_api_api_posts(client):
    '''GET        /api/posts'''
    pass


def test_get_api_api_threads(client):
    '''GET        /api/threads'''
    pass


def test_get_api_api_users(client):
    '''GET        /api/users'''
    pass


def test_get_api_docs(client):
    '''GET        /api/docs'''
    pass


def test_get_api_invoice_hook(client):
    '''POST       /api/invoice-hook'''
    pass


def test_get_auth_login(client):
    '''GET, POST  /auth/login'''
    rv = client.get('auth/login')
    assert b"Login" in rv.data
    assert b"Register" in rv.data
    assert b"Username" in rv.data
    assert b"Password" in rv.data
    assert b"Forgot Password?" in rv.data


def test_get_auth_register(client):
    '''GET, POST  /auth/register'''
    rv = client.get('auth/login')
    assert b"Login" in rv.data
    assert b"Register" in rv.data
    assert b"Username" in rv.data
    assert b"Password" in rv.data
    assert b"Forgot Password?" in rv.data


def test_get_auth_logout(client):
    '''GET  /auth/logout'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get('auth/logout', follow_redirects=True)
    assert b"Logout successful" in rv.data


def test_get_auth_reset_password(client):
    '''GET, POST  /auth/reset-password/<token>'''
    bad_token = 'not-good-123'
    rv = client.get(f'auth/reset-password/{bad_token}', follow_redirects=True)
    assert b'Invalid Reset Token' in rv.data


def test_get_auth_reset_password_request(client):
    '''GET, POST  /auth/reset-password-request'''
    rv = client.get('auth/reset-password-request')
    assert b'Get Reset Password Link' in rv.data
    assert b'Email' in rv.data


def test_get_create_blog(client):
    '''GET, POST  /blog/create_blog'''
    # without logging in
    rv = client.get('blog/create_blog', follow_redirects=True)
    assert b'Login as admin to go there' in rv.data
    # login is basic user
    login(client, 'anonymous', 'anon-test-password')
    rv = client.get('blog/create_blog', follow_redirects=True)
    assert b'You cannot go there' in rv.data
    logout(client)
    login(client, 'admin', 'admin-test-password')
    rv = client.get('blog/create_blog', follow_redirects=True)
    assert b'create a blog post' in rv.data
    assert b'Title' in rv.data
    assert b'Blog' in rv.data
    assert b'Submit' in rv.data


def test_create_blog(client):
    """Test creating a post works."""
    login(client, 'admin', 'admin-test-password')
    rv = client.post('blog/create_blog', data=dict(
        title='Hello, World!',
        body="Test Blog Creation"
    ), follow_redirects=True)
    # Check that the post was created and username returned on post view
    assert b'Hello, World!' in rv.data
    assert b'admin' in rv.data
    assert b'Test Blog Creation' in rv.data


def test_create_empty_post(client):
    """Test creating a post works."""
    login(client, 'admin', 'admin-test-password')
    rv = client.post('blog/create_blog', data=dict(
        title='',
        body='Hello'
    ), follow_redirects=True)
    # Ensure post was NOT created and error message displays
    assert b"Title is required" in rv.data
    rv = client.post('blog/create_blog', data=dict(
        title='hi',
        body='',
    ), follow_redirects=True)
    # Ensure post was NOT created and error message displays
    assert b"Body is required" in rv.data


def test_get_blog(client):
    '''GET, POST  /blog/blog/<int:blog_id>'''
    # create blog then get blog
    login(client, 'admin', 'admin-test-password')
    client.post('blog/create_blog', data=dict(
        title='Hello, World!',
        body="Test Blog Creation"
    ), follow_redirects=True)
    # Get the blog post that was just created
    rv = client.get('blog/blog/1')
    assert b'Hello, World!' in rv.data
    assert b'Test Blog Creation' in rv.data


def test_get_blogs(client):
    '''GET        /blog/blogs'''
    rv = client.get('blog/blogs')
    assert b'blogs' in rv.data
    login(client, 'admin', 'admin-test-password')
    rv = client.get('blog/blogs')
    assert b'create blog' in rv.data


def test_get_blog_delete_blog(client):
    '''POST       /blog/delete_blog/<int:blog_id>'''
    # create blog then get blog
    login(client, 'admin', 'admin-test-password')
    client.post('blog/create_blog', data=dict(
        title='Hello, World!',
        body="Test Blog Creation"
    ), follow_redirects=True)
    # Get the blog post that was just created
    rv = client.get('blog/blog/1')
    assert b'Hello, World!' in rv.data
    assert b'Test Blog Creation' in rv.data
    # Delete the blog that was created and make sure its not there anymore
    rv = client.post('blog/delete_blog/1', follow_redirects=True)
    assert b'Hello, World!' not in rv.data
    rv = client.get('blog/blog/1')
    assert b'Not Found' in rv.data


def test_get_api_information(client):
    '''GET        /api'''
    rv = client.get('api', follow_redirects=True)
    assert b"api information" in rv.data
    assert b"GET" in rv.data


def test_get_bombadillo_check_payment(client):
    '''POST       /check-payment'''
    pass


def test_get_bombadillo_comment_like(client):
    '''POST       /comment-like'''
    pass


def test_get_bombadillo_create(client):
    '''GET, POST  /create'''
    pass


def test_get_bombadillo_delete(client):
    '''POST       /delete/<int:post_id>'''
    pass


def test_get_bombadillo_delete_comment(client):
    '''POST       /delete_comment/<int:comment_id>'''
    pass


def test_get_bombadillo_edit_profile(client):
    '''GET, POST  /edit-profile/<int:user_id>'''
    rv = client.get('edit-profile/1')
    assert b"Avatar" in rv.data
    assert b"Email" in rv.data
    assert b"web link" in rv.data
    assert b"bio" in rv.data
    assert b"Update Profile" in rv.data


def test_get_bombadillo_explore(client):
    '''GET        /explore'''
    pass


def test_get_bombadillo_explore(client):
    '''GET        /'''
    pass


def test_get_bombadillo_follow(client):
    '''POST       /follow'''
    pass


def test_get_bombadillo_get_general_invoice(client):
    '''POST       /get-general-invoice'''
    pass


def test_get_bombadillo_get_invoice(client):
    '''POST       /get-invoice'''
    pass


def test_get_bombadillo_images(client):
    '''GET        /images'''
    pass


def test_get_bombadillo_lightning(client):
    '''GET        /lightning'''
    pass


def test_get_bombadillo_more_comments(client):
    '''POST       /more-comments'''
    pass


def test_get_bombadillo_more_explore(client):
    '''POST       /more-explore'''
    pass


def test_get_bombadillo_more_images(client):
    '''POST       /more-images'''
    pass


def test_get_bombadillo_more_received_payments(client):
    '''POST       /more-received-payments'''
    pass


def test_get_bombadillo_more_sent_payments(client):
    '''POST       /more-sent-payments'''
    pass


def test_get_bombadillo_more_threads(client):
    '''POST       /more-threads'''
    pass


def test_get_bombadillo_more_user_bookmarks(client):
    '''POST       /more-bookmarks'''
    pass


def test_get_bombadillo_more_user_likes(client):
    '''POST       /more-likes'''
    pass


def test_get_bombadillo_more_user_profile_posts(client):
    '''POST       /more-user-profile-posts'''
    pass


def test_get_bombadillo_new_comment(client):
    '''POST       /new-comment'''
    pass


def test_get_bombadillo_post(client):
    '''GET, POST  /post/<int:post_id>'''
    pass


def test_get_bombadillo_post_like(client):
    '''POST       /post-like'''
    pass


def test_get_bombadillo_random_post(client):
    '''GET, POST  /random'''
    rv = client.get('random')
    assert b"Random" in rv.data
    assert b"Go!" in rv.data


def test_get_bombadillo_received_payment_history(client):
    '''GET        /received-payment-history'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("received-payment-history")
    assert b"Activity - Received" in rv.data


def test_get_bombadillo_report_comment(client):
    '''POST       /report/comment/<int:id>'''
    pass


def test_get_bombadillo_report_post(client):
    '''POST       /report/post/<int:post_id>'''
    pass


def test_get_bombadillo_save_bookmark(client):
    '''POST       /save-bookmark'''
    pass


def test_get_bombadillo_search_users(client):
    '''GET, POST  /search-users'''
    rv = client.get('search-users')
    assert b"Users" in rv.data


def test_get_bombadillo_sent_payment_history(client):
    '''GET        /sent-payment-history'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("sent-payment-history")
    assert b"Activity - Sent" in rv.data


def test_get_bombadillo_threads(client):
    '''GET, POST  /threads'''
    pass


def test_get_bombadillo_update_password(client):
    '''GET, POST  /update-password/<int:user_id>'''
    rv = client.get("update-password")
    assert b"Not Found" in rv.data
    login(client, 'admin', 'admin-test-password')
    rv = client.get("update-password/1")
    assert b"Current Password" in rv.data
    assert b"New Password" in rv.data
    assert b"Confirm New Password" in rv.data


def test_get_bombadillo_user_bookmarks(client):
    '''GET        /user/bookmarks'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/user/bookmarks")
    assert b"Theres no posts" in rv.data


def test_get_bombadillo_user_likes(client):
    '''GET        /user/likes'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/user/likes")
    assert b"Theres no posts" in rv.data


def test_get_bombadillo_users(client):
    '''GET        /users'''
    rv = client.get("/users")
    assert b"admin" in rv.data


def test_get_bombadillo_view_image(client):
    '''GET        /images/<filename>'''
    pass


def test_get_bombadillo_view_user(client):
    '''GET        /users/<int:user_id>'''
    pass


def test_get_bombadillo_withdraw(client):
    '''GET, POST  /withdraw'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/withdraw")
    assert b"Lightning Invoice" in rv.data
    assert b"Withdrawals" in rv.data
    assert b"Submit Withdrawal Request" in rv.data


def test_get_bootstrap_static(client):
    '''GET        /bootstrap/static/<path:filename>'''
    pass


def test_get_bounty_all_bounties(client):
    '''GET        /all-bounties'''
    rv = client.get("/all-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Create" in rv.data
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/all-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Yours" in rv.data
    assert b"Your Answers" in rv.data
    assert b"Create" in rv.data


def test_get_bounty_award_bounty(client):
    '''POST       /award-bounty/<int:bounty_id>'''
    pass


def test_get_bounty_closed_bounties(client):
    '''GET        /closed-bounties'''
    rv = client.get("/closed-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Create" in rv.data
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/closed-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Yours" in rv.data
    assert b"Your Answers" in rv.data
    assert b"Create" in rv.data

def test_get_bounty_create_bounty(client):
    '''GET, POST  /create-bounty'''
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/create-bounty")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Yours" in rv.data
    assert b"Your Answers" in rv.data
    assert b"Create" in rv.data


def test_get_bounty_get_bounty_invoice(client):
    '''POST       /get-bounty-invoice'''
    pass


def test_get_bounty_get_bounty_response_invoice(client):
    '''POST       /get-bounty-response-invoice'''
    pass


def test_get_bounty_more_all_bounties(client):
    '''POST       /more-all-bounties'''
    pass


def test_get_bounty_more_closed_bounties(client):
    '''POST       /more-closed-bounties'''
    pass


def test_get_bounty_more_open_bounties(client):
    '''POST       /more-open-bounties'''
    pass


def test_get_bounty_more_user_bounties(client):
    '''POST       /more-user-bounties'''
    pass


def test_get_bounty_more_user_responded_bounties(client):
    '''POST       /more-user-responded-bounties'''
    pass


def test_get_bounty_open_bounties(client):
    '''GET        /open-bounties'''
    rv = client.get("/open-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Create" in rv.data
    login(client, 'admin', 'admin-test-password')
    rv = client.get("/open-bounties")
    assert b"All" in rv.data
    assert b"Open" in rv.data
    assert b"Closed" in rv.data
    assert b"Yours" in rv.data
    assert b"Your Answers" in rv.data
    assert b"Create" in rv.data


def test_get_bounty_response_upvote(client):
    '''POST       /response-upvote'''
    pass


def test_get_bounty_user_bounties(client):
    '''GET        /user/user-bounties/<int:user_id>'''
    pass


def test_get_bounty_user_responded_bounties(client):
    '''GET        /user/user-bounty-responses/<int:user_id>'''
    pass


def test_get_bounty_view_bounty(client):
    '''GET, POST  /bounty/<int:bounty_id>'''
    pass


def test_get_bounty_view_bounty_response(client):
    '''GET        /bounty_response/<int:response_id>'''
    pass


def test_get_index(client):
    '''GET        /'''
    pass


def test_get_information_about(client):
    '''GET        /about'''
    pass


def test_get_information_bitcoin_pdf(client):
    '''GET        /bitcoin_pdf'''
    pass


def test_get_information_help(client):
    '''GET        /help'''
    rv = client.get("/help")
    assert b"Junkbite Functionality" in rv.data


def test_get_information_info(client):
    '''GET        /information'''
    rv = client.get("/information")
    assert b"Information Links" in rv.data
    assert b"rules" in rv.data
    assert b"about" in rv.data
    assert b"help" in rv.data
    assert b"lightning node" in rv.data


def test_get_information_info(client):
    '''GET        /info'''
    rv = client.get("/info")
    assert b"Information Links" in rv.data
    assert b"rules" in rv.data
    assert b"about" in rv.data
    assert b"help" in rv.data
    assert b"lightning node" in rv.data


def test_get_information_lightning_info(client):
    '''GET        /node-information'''
    rv = client.get("node-information")
    assert b"Lightning Node" in rv.data
    assert b"Public Key" in rv.data


def test_get_information_rules(client):
    '''GET        /rules'''
    rv = client.get("rules")
    assert b"rules" in rv.data
    assert b"No Hacking" in rv.data
    assert b"Do not hack junkbite" in rv.data


def test_get_static(client):
    '''GET        /static/<path:filename>'''
    pass

