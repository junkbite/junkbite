import os
import sqlite3
import jinja2
from app import create_app, db, commands
from datetime import datetime
from config import Config
from app.models import (
    User,
    Post,
    Comment,
    Payment,
    UserBalance,
    UserBalanceEntry,
    FollowRequest,
    Notification,
    Bounty,
    BountyResponse,
    Withdrawal,
    ReportedContent,
    Followers,
    UserNode,
    PostNode,
    Payment,
    PostLike,
    BountyResponseLike,
    Bookmark,
    CommentLike,
    Blog,
    OnionUrl,
    Advertisement,
    AdvertisementStats,
    JunkbiteNode,
    Settings,
    PaymentProcessingFee,
    FAQEntry
)
from custom_jinja_filters import (
    strftime,
    remove_leading_char,
    strip_code_type,
    process_post_text,
)


app = create_app()
commands.init_app(app)

# Add custom jinja filters to environment
app.jinja_env.filters["strftime"] = strftime
app.jinja_env.filters["remove_leading_char"] = remove_leading_char
app.jinja_env.filters["strip_code_type"] = strip_code_type
app.jinja_env.filters["process_post_text"] = process_post_text


@app.shell_context_processor
def make_shell_context():
    '''Preloads the flask shell with db and models'''
    return {
        'db': db,
        'User': User,
        'Post': Post,
        'Comment': Comment,
        'UserBalance': UserBalance,
        'UserBalanceEntry': UserBalanceEntry,
        'FollowRequest': FollowRequest,
        'Notification': Notification,
        'Bounty': Bounty,
        'BountyResponse': BountyResponse,
        'Withdrawal': Withdrawal,
        'ReportedContent': ReportedContent,
        'Followers': Followers,
        'UserNode': UserNode,
        'PostNode': PostNode,
        'Payment': Payment,
        'PostLike': PostLike,
        'BountyResponseLike': BountyResponseLike,
        'Bookmark': Bookmark,
        'CommentLike': CommentLike,
        'Blog': Blog,
        'OnionUrl': OnionUrl,
        'Advertisement': Advertisement,
        'AdvertisementStats': AdvertisementStats,
        'JunkbiteNode': JunkbiteNode,
        'Settings': Settings,
        'PaymentProcessingFee': PaymentProcessingFee,
        'FAQEntry': FAQEntry
    }
