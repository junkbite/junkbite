# Ensure we navigate to the application root so the custom commands are visible
* * * * * cd /root/junkbite; /root/junkbite/venv/bin/flask pay-pending-withdrawals
* * * * * sleep 15; cd /root/junkbite; /root/junkbite/venv/bin/flask pay-pending-withdrawals
* * * * * sleep 30; cd /root/junkbite; /root/junkbite/venv/bin/flask pay-pending-withdrawals
* * * * * sleep 45; cd /root/junkbite; /root/junkbite/venv/bin/flask pay-pending-withdrawals
