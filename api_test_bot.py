import os
import json
import requests
from requests.auth import HTTPBasicAuth

# -----------------------------------------------------------------------------
# Help
# -----------------------------------------------------------------------------
# A python client which can hit all the GET endpoints and shows an example
# of how to create a post via the API
#
# API Endpoints:
# GET        /api/comments
# POST       /api/create-post
# GET        /api/posts
# GET        /api/threads
# GET        /api/users
# GET        /api/docs
# -----------------------------------------------------------------------------


#BASE_URL = "https://junkbite.com/api/"
BASE_URL = "http://localhost:5000/api/"
USERS = "users"
COMMENTS = "comments"
CREATE = "create-post"
POSTS = "posts"
THREADS = "threads"


def _get(endpoint):
    """
    Make a get request to a junkbite url
    :param: endpoint
    """
    return requests.get(endpoint)


def create_post(username, password, title, image=None):
    """
    Send a POST request to create post api endpoint
    Note: text post just sends json
    image post must send data as a form
    """
    url = BASE_URL + CREATE
    auth = HTTPBasicAuth(username, password)
    payload = {'data': {"title": title}}
    if image is None:
        return requests.post(url, json=payload, auth=auth)
    data = {'filename': os.path.basename(image), 'data': json.dumps(payload)}
    files = {'file': (image, open(image, 'rb'), 'image/jpg', {'Expires': '0'})}
    return requests.post(url, files=files, auth=auth, data=data)


def get_users():
    endpoint = BASE_URL + USERS
    return _get(endpoint)


def get_comments():
    endpoint = BASE_URL + COMMENTS
    return _get(endpoint)


def get_posts():
    endpoint = BASE_URL + POSTS
    return _get(endpoint)


def get_threads():
    endpoint = BASE_URL + THREADS
    return _get(endpoint)


def post_create():
    endpoint = BASE_URL + CREATE
    return _get(endpoint)


def test_post_with_image():
    title = "Test"
    username = "psqnt"
    password = "psqnt"
    image = "~/cat_sunglasses.jpg"
    x = create_post(username=username, password=password, title=title, image=image)
    print(x)
    print(x.json())


def test_post_no_image():
    title = "Test"
    username = "psqnt"
    password = "psqnt"
    x = create_post(username=username, password=password, title=title)
    print(x)
    print(x.json())


if __name__ == "__main__":
    username = 'ahole'
    password = 'ahole'
    title_base = "test{}"
    for i in range(12, 100):
        title = title_base.format(i)
        print(title)
        create_post(username, password, title, image=None)
